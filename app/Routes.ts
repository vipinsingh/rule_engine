import ApiRouter from "./shared/ApiRouter/ApiRouter";
import {Router} from "express";
import UserRouter from './modules/User/routes/UserRouter';
import ProjectRouter from './modules/Project/routes/ProjectRouter';
import DecisionTableRouter from './modules/DecisionTable/routes/DecisionTableRouter';
import DecisionRouter from './modules/Decision/routes/DecisionRouter';
import DecisionRuleRouter from './modules/DecisionRule/routes/DecisionRuleRouter';
import RuleSetRouter from './modules/RuleSet/routes/RuleSetRouter';

class Routes extends ApiRouter{
    routes : Router

    constructor() {
        super();
        this.routes = this.instance;
        this.setRoutes();
    }

    setRoutes() {
        this.routes.use('/user', UserRouter);
        this.routes.use('/projects', ProjectRouter);
        this.routes.use('/tables', DecisionTableRouter);
        this.routes.use('/decisions', DecisionRouter);
        this.routes.use('/rules', DecisionRuleRouter);
        this.routes.use('/rule_set', RuleSetRouter);
    }
}

export default new Routes().routes;