import { LogRequest } from './shared/middlewares/LogRequest';
import {Express, Request, Response} from "express";
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import Routes from './Routes';
import cors from 'cors';

export class Server {

    private app: Express;

    constructor(app: Express) {
        this.app = app;

        this.app.use(express.static(path.resolve("./") + "/frontend/build"));

        this.app.use(cors());

        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));

        this.app.get("/api", (req: Request, res: Response): void => {
            res.send("You have reached the API!");
        });

        this.app.use("/api/v1", LogRequest.initiateLog, Routes);

        this.app.get("*", (req: Request, res: Response): void => {
            res.sendFile(path.resolve("./") + "/frontend/build/index.html");
        });
    }

    public start(port: string): void {
        this.app.listen(parseInt(port), () => console.log(`Server listening on port ${port}!`));
    }

}