import { ERRORS } from './../../../lang/en/error_messages';
import { FIELDS } from './../../../config/fields';
import { Request, Response } from "express";
import { CommonFunctions } from "../../../shared/utils/CommonFunctions";
import DecisionTableService from '../services/DecisionTableService';

class DecisionTableController {

    createTable(req : Request, res : Response) {
        try {
            const tableData : any = CommonFunctions.filterObj(req.body, FIELDS.create_table);

            CommonFunctions.checkProjectExists(tableData.user_id, tableData.project_id)
                .then(project => {
                    if(!project) {
                        return CommonFunctions.createErrorResponse(req, res, ERRORS['resource_not_found'], 404);
                    }
                    return DecisionTableService.createTable(tableData)
                })
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data, 201);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
            
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    listTables(req : Request, res : Response) {
        try {
            const params = CommonFunctions.filterObj(req.query, FIELDS.list_tables);

            DecisionTableService.listTables(params)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
            
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    updateTable(req : Request, res : Response) {
        try {
            const tableData : any = CommonFunctions.filterObj(req.body, FIELDS.create_table);

            const id = req.params.id;

            CommonFunctions.checkProjectExists(tableData.user_id, tableData.project_id)
                .then(project => {
                    if(!project) {
                        return CommonFunctions.createErrorResponse(req, res, ERRORS['resource_not_found'], 404);
                    }
                    return DecisionTableService.updateTable(id, tableData)
                })
                .then(data => {
                    if(!data) {
                        return CommonFunctions.createSuccessResponse(req, res, [], 200);
                    }
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
            
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    getTable(req : Request, res : Response) {
        try {
            const params = CommonFunctions.filterObj(req.query, FIELDS.list_tables);

            params['_id'] = req.params.id;

            DecisionTableService.getTable(params)
                .then(data => {
                    if(!data) {
                        return CommonFunctions.createSuccessResponse(req, res, [], 200);
                    }
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
            
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }
}

export default new DecisionTableController()