export const CREATE_TABLE = [
    {
        name : 'id',
        paramIn : 'param',
        validations : [
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/,
                optional : true
            }
        ]
    },
    {
        name : 'user_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'name',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '3,100',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'description',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '3,200',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'default_title',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '3,200',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'default_description',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '3,200',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'default_decision',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'decision_type',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '0,30',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'table_type',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '0,30',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'fields',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'isArray',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'fields.*.id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'integer',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'fields.*.title',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '3,50',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'fields.*.api_key',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '2,50',
                message : 'invalid',
                bail : true
            },
            {
                name : 'pattern',
                pattern : /^[a-zA-Z_]+$/,
                message : 'invalid'
            },
        ]
    },
    {
        name : 'fields.*.fieldType',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '0,30',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rules',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'isArray',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rules.*.id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'integer',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rules.*.title',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '0,200',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rules.*.description',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '0,200',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rules.*.decision',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rules.*.conditions',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'isArray',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rules.*.conditions.*.field_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'integer',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rules.*.conditions.*.rule_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'integer',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rules.*.conditions.*.field_name',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'rules.*.conditions.*.condition',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'rules.*.conditions.*.value',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            }
        ]
    },
];

export const LIST_TABLES = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'page_no',
        paramIn : 'query',
        validations : [
            {
                name : 'integer',
                message : 'invalid',
                optional  : true
            },
            {
                name : 'custom',
                message : 'invalid',
                optional  : true,
                callback : (value) => {
                    return value > 0;
                }
            }
        ]
    },
    {
        name : 'per_page',
        paramIn : 'query',
        validations : [
            {
                name : 'integer',
                message : 'invalid',
                optional  : true
            },
            {
                name : 'custom',
                message : 'invalid',
                optional  : true,
                callback : (value) => {
                    return value > 0;
                }
            }
        ]
    }
];

export const GET_TABLE = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'id',
        paramIn : 'param',
        validations : [
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
];