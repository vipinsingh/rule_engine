import { CREATE_TABLE, LIST_TABLES, GET_TABLE } from './../validators/decisionTableValidationConfig';
import { RequestValidator } from './../../../shared/middlewares/RequestValidator';
import { Router } from 'express';
import { ValidationErrorHandler } from './../../../shared/middlewares/ValidationErrorHandler';
import DecisionTableController from '../controllers/DecisionTableController';
import ApiRouter from '../../../shared/ApiRouter/ApiRouter';

class DecisionTableRouter extends ApiRouter {
    router : Router;

    constructor() {
        super();
        this.router = this.instance;
        this.setDecisionTableRoutes();
    }

    setDecisionTableRoutes() {
        this.router.post('/', RequestValidator.validateReq(CREATE_TABLE), ValidationErrorHandler.checkError, DecisionTableController.createTable)
        this.router.put('/:id', RequestValidator.validateReq(CREATE_TABLE), ValidationErrorHandler.checkError, DecisionTableController.updateTable)
        this.router.get('/', RequestValidator.validateReq(LIST_TABLES), ValidationErrorHandler.checkError, DecisionTableController.listTables)
        this.router.get('/:id', RequestValidator.validateReq(GET_TABLE), ValidationErrorHandler.checkError, DecisionTableController.getTable)
    }
}

export default new DecisionTableRouter().router;