import { SchemaBuilder } from './../../../db/SchemaBuilder';

class DecisionTable extends SchemaBuilder {

    constructor() {
        super({
            user_id: {
                type: String
            },
            project_id : {
                type: String
            },
            name: {
                type: String
            },
            description: {
                type: String
            },
            table_type : {
                type: String
            },
            decision_type : {
                type: String
            },
            default_title : {
                type: String
            },
            default_description : {
                type: String
            },
            default_decision : {
                type: String
            },
            fields : {
                type: [
                    {
                        id : Number,
                        title : String,
                        api_key : String,
                        fieldType : String
                    }
                ]
            },
            rules : {
                type: [
                    {
                        id : Number,
                        title : String,
                        description : String,
                        decision : String,
                        conditions : [
                            {
                                rule_id : Number,
                                field_id : Number,
                                field_name : String,
                                condition : String,
                                value : String
                            }
                        ]
                    }
                ]
            }
        }, 'tables');
    }
}

export default new DecisionTable().Model;