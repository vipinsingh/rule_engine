import { FIELDS } from './../../../config/fields';
import { QueryBuilderService } from './../../../shared/services/QueryBuilderService';

class DecisionTableService {
    private queryBuilder : QueryBuilderService;

    constructor(queryBuilder : QueryBuilderService) {
        this.queryBuilder = queryBuilder;
    }

    createTable(data) {
        return this.queryBuilder.Create('DecisionTable', data)
    }

    updateTable(id, data) {
        return this.queryBuilder.Update('DecisionTable', { _id : id }, data, { new :  true })
    }

    listTables(params) {
        const pageNo = params[FIELDS.page_no] ? parseInt(params[FIELDS.page_no]) : 1;
        const perPage = params[FIELDS.per_page] ? parseInt(params[FIELDS.per_page]) : 15;
        
        const skipRecords = (pageNo - 1) * perPage;

        const findParams = {
            user_id : params[FIELDS.user_id],
            project_id : params[FIELDS.project_id]
        }

        let count = null;

        return this.queryBuilder.Count('DecisionTable', findParams)
            .then(dataCount => {
                if(dataCount === 0) {
                    return {
                        tables : [],
                        count : 0
                    };
                }
                count = dataCount;
                return this.queryBuilder.Find('DecisionTable', findParams, 'name description created_at updated_at', {skip : skipRecords, limit : perPage})
            })
            .then(data => {
                if(data.count === 0) {
                    return Promise.resolve(data);
                }
                return Promise.resolve({
                    tables : data,
                    count : count
                });
            })
    }

    getTable(params) {
        return this.queryBuilder.FindOne('DecisionTable', params)
    }
}

export default new DecisionTableService(new QueryBuilderService())