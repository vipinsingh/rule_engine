import { CREATE_PROJECT, GET_PROJECTS } from './../validators/projectValidationConfig';
import { RequestValidator } from './../../../shared/middlewares/RequestValidator';
import { Router } from 'express';
import { ValidationErrorHandler } from './../../../shared/middlewares/ValidationErrorHandler';
import ProjectController from './../controllers/ProjectController';
import ApiRouter from '../../../shared/ApiRouter/ApiRouter';

class ProjectRouter extends ApiRouter {
    router : Router;

    constructor() {
        super();
        this.router = this.instance;
        this.setProjectRoutes();
    }

    setProjectRoutes() {
        this.router.post('/', RequestValidator.validateReq(CREATE_PROJECT), ValidationErrorHandler.checkError, ProjectController.createProject)
        this.router.get('/', RequestValidator.validateReq(GET_PROJECTS), ValidationErrorHandler.checkError, ProjectController.getProjects)
    }
}

export default new ProjectRouter().router;