import { ERRORS } from './../../../lang/en/error_messages';
import { Request, Response } from "express";
import { CommonFunctions } from "../../../shared/utils/CommonFunctions";
import { FIELDS } from "../../../config/fields";
import ProjectService from "../services/ProjectService";

class ProjectController {

    createProject(req : Request, res: Response) {
        try {
            const projectData : any = CommonFunctions.filterObj(req.body, FIELDS.create_project);

            ProjectService.checkUserExists(projectData.user_id)
                .then(user => {
                    if(!user) {
                        return CommonFunctions.createErrorResponse(req, res, ERRORS['resource_not_found'], 404);
                    }
                    return ProjectService.createProject(projectData)
                })
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data, 201);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
            
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    getProjects(req : Request, res: Response) {
        try {
            const params = CommonFunctions.filterObj(req.query, FIELDS.get_projects);

            ProjectService.getProjects(params)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })

        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }
}

export default new ProjectController();