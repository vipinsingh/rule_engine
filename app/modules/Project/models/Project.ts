import { SchemaBuilder } from '../../../db/SchemaBuilder';

class Project extends SchemaBuilder {

    constructor() {
        super({
            user_id: {
                type: String
            },
            name: {
                type: String
            },
            description: {
                type: String
            }
        }, 'projects');
    }
}

export default new Project().Model

