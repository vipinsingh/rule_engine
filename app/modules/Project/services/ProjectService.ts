import { FIELDS } from './../../../config/fields';
import { QueryBuilderService } from './../../../shared/services/QueryBuilderService';

class ProjectService {
    private queryBuilder : QueryBuilderService;

    constructor(queryBuilder : QueryBuilderService) {
        this.queryBuilder = queryBuilder;
    }

    checkUserExists(user_id) {
        return this.queryBuilder.FindOne('User', { _id : user_id })
    }

    createProject(data) {
        return this.queryBuilder.Create('Project', data)
    }

    getProjects(params) {
        const pageNo = params[FIELDS.page_no] ? parseInt(params[FIELDS.page_no]) : 1;
        const perPage = params[FIELDS.per_page] ? parseInt(params[FIELDS.per_page]) : 15;
        
        const skipRecords = (pageNo - 1) * perPage;

        const findParams = {
            user_id : params[FIELDS.user_id],
        }

        let count = null;
    
        return this.queryBuilder.Count('Project', findParams)
            .then(dataCount => {
                if(dataCount === 0) {
                    return {
                        projects : [],
                        count : 0
                    };
                }
                count = dataCount;
                return this.queryBuilder.Find('Project', findParams, null, {skip : skipRecords, limit : perPage})
            })
            .then(data => {
                if(data.count === 0) {
                    return Promise.resolve(data);
                }
                return Promise.resolve({
                    projects : data,
                    count : count
                });
            })
    }
}

export default new ProjectService(new QueryBuilderService());