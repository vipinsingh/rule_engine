export const CREATE_PROJECT = [
    {
        name : 'user_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'name',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '2,100',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'description',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '2,200',
                message : 'invalid'
            },
        ]
    }
];

export const GET_PROJECTS = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'page_no',
        paramIn : 'query',
        validations : [
            {
                name : 'integer',
                message : 'invalid',
                optional  : true
            },
            {
                name : 'custom',
                message : 'invalid',
                optional  : true,
                callback : (value) => {
                    return value > 0;
                }
            }
        ]
    },
    {
        name : 'per_page',
        paramIn : 'query',
        validations : [
            {
                name : 'integer',
                message : 'invalid',
                optional  : true
            },
            {
                name : 'custom',
                message : 'invalid',
                optional  : true,
                callback : (value) => {
                    return value > 0;
                }
            }
        ]
    }
];