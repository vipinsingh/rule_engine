import DecisionTable from './../../DecisionTable/models/DecisionTable';

export const RUN_DECISION = [
    {
        name : 'table_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail :  true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/,
                bail : true
            },
            {
                name : 'custom',
                message : 'invalid',
                callback : (value) => {
                    return DecisionTable.findById(value)
                        .then(table => {
                            if(!table) {
                                return Promise.reject('invalid')
                            }
                            return true;
                        })
                }
            }
        ]
    }
];

export const LIST_DECISIONS = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'page_no',
        paramIn : 'query',
        validations : [
            {
                name : 'integer',
                message : 'invalid',
                optional  : true
            },
            {
                name : 'custom',
                message : 'invalid',
                optional  : true,
                callback : (value) => {
                    return value > 0;
                }
            }
        ]
    },
    {
        name : 'per_page',
        paramIn : 'query',
        validations : [
            {
                name : 'integer',
                message : 'invalid',
                optional  : true
            },
            {
                name : 'custom',
                message : 'invalid',
                optional  : true,
                callback : (value) => {
                    return value > 0;
                }
            }
        ]
    }
];

export const GET_DECISION = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'table_id',
        paramIn : 'query',
        validations : [
            {
                name : 'notEmpty',
                message : 'required',
                optional :  true
            }
        ]
    }
];