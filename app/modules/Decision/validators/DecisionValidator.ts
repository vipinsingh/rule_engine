import DecisionTable from './../../DecisionTable/models/DecisionTable';
import { Request, Response, NextFunction } from 'express';
import { ERRORS } from '../../../lang/en/error_messages';
import { CommonFunctions } from '../../../shared/utils/CommonFunctions';

export class DecisionValidator {

    validateDecisionReq = async (req :  Request, res : Response, next : NextFunction) => {

        try {
            const DecTable = await DecisionTable.findById(req.body.table_id);

            let errors = [];

            const fieldsArr = [];
            
            let fieldTypeObj = {};

            DecTable.fields.forEach(field => {
                if(req.body[field.api_key] === undefined) {
                    const {type , message} = ERRORS['required'];
                    errors.push({
                        type,
                        param : field.api_key,
                        message
                    });
                } else {
                    errors = CommonFunctions.validateAccordingToType(errors, field.fieldType, req.body[field.api_key], field.api_key);
                }
                fieldsArr.push(field.api_key);
                fieldTypeObj[field.api_key] = field.fieldType;
            });

            if(errors.length > 0) {
                return CommonFunctions.createErrorResponse(req, res, errors, 400);
            }

            req.body.table = DecTable;
            req.body.fields_arr = fieldsArr;
            req.body.fieldTypeObj = fieldTypeObj;

            next();
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }
}