import { FIELDS } from './../../../config/fields';
import { Request, Response } from "express";
import { CommonFunctions } from "../../../shared/utils/CommonFunctions";
import DecisionService from '../services/DecisionService';

class DecisionController {

    runDecision(req : Request, res : Response) {
        try {
            const DecTable = JSON.parse(JSON.stringify(req.body.table));

            const inputFields = CommonFunctions.filterObj(req.body, req.body.fields_arr);

            DecisionService.runDecision(DecTable, inputFields, req.body.fieldTypeObj)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data, 201);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    listDecisions(req : Request, res : Response) {
        try {
            const params = CommonFunctions.filterObj(req.query, FIELDS.list_decisions);

            DecisionService.listDecisions(params)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    getDecisionDetails(req : Request, res : Response) {
        try {
            const params = CommonFunctions.filterObj(req.query, FIELDS.get_decision);

            params['_id'] = req.params.decId;

            DecisionService.getDecisionDetails(params)
                .then(data => {
                    if(!data) {
                        return CommonFunctions.createSuccessResponse(req, res, [], 200);
                    }
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }
}

export default new DecisionController();