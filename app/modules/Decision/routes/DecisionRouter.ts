import { RUN_DECISION, LIST_DECISIONS, GET_DECISION } from './../validators/decisionValidationConfig';
import { RequestValidator } from './../../../shared/middlewares/RequestValidator';
import { Router } from 'express';
import { DecisionValidator } from './../validators/DecisionValidator';
import { ValidationErrorHandler } from './../../../shared/middlewares/ValidationErrorHandler';
import DecisionController from '../controllers/DecisionController';
import ApiRouter from '../../../shared/ApiRouter/ApiRouter';

class DecisionRouter extends ApiRouter {
    router : Router;

    decisionValidator = new DecisionValidator();

    constructor() {
        super();
        this.router = this.instance;
        this.setDecisionRoutes();
    }

    setDecisionRoutes() {
        this.router.post('/',
            RequestValidator.validateReq(RUN_DECISION),
            ValidationErrorHandler.checkError,
            this.decisionValidator.validateDecisionReq,
            DecisionController.runDecision
        );
        this.router.get('/',
            RequestValidator.validateReq(LIST_DECISIONS),
            ValidationErrorHandler.checkError,
            DecisionController.listDecisions
        );
        this.router.get('/:decId',
            RequestValidator.validateReq(GET_DECISION),
            ValidationErrorHandler.checkError,
            DecisionController.getDecisionDetails
        );
    }
}

export default new DecisionRouter().router;