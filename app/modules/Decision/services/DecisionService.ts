import { CONDITION_ACTIONS } from './../../../shared/utils/ConditionActions';
import { CommonFunctions } from './../../../shared/utils/CommonFunctions';
import { FIELDS } from './../../../config/fields';
import { QueryBuilderService } from './../../../shared/services/QueryBuilderService';
class DecisionService {
    private queryBuilder : QueryBuilderService;

    constructor(queryBuilder : QueryBuilderService) {
        this.queryBuilder = queryBuilder;
    }

    runDecision(DecTable, inputFields, fieldTypeObj) {
        let firstRuleToMatch = null;

        const allMatchingRules = [];

        DecTable.rules.forEach(rule => {
            rule['matched'] = true;

            rule.conditions.forEach(condition => {
                condition['matched'] = this.isConditionMatched(
                    inputFields[condition.field_name],
                    fieldTypeObj[condition.field_name],
                    condition.condition,
                    condition.value
                );
                rule['matched'] = rule['matched'] && condition['matched'];
            });

            if(rule['matched']) {
                if(!firstRuleToMatch) {
                    firstRuleToMatch = rule;
                }
                allMatchingRules.push(rule);
            }
        });

        const allMatchingDecisions = allMatchingRules.map(rule => {
            return {
                title : rule[FIELDS.title],
                description : rule[FIELDS.description],
                decision : rule[FIELDS.decision]
            }
        });

        const dataToSave = {
            table_id : DecTable[FIELDS.id],
            user_id : DecTable[FIELDS.user_id],
            project_id : DecTable[FIELDS.project_id],
            table_info : {
                name : DecTable[FIELDS.name],
                description : DecTable[FIELDS.description],
                table_type : DecTable[FIELDS.table_type],
                decision_type : DecTable[FIELDS.decision_type],
                default_decision : DecTable[FIELDS.default_decision]
            },
            rule_title : firstRuleToMatch ? firstRuleToMatch[FIELDS.title] : DecTable[FIELDS.default_title],
            rule_description : firstRuleToMatch ? firstRuleToMatch[FIELDS.description] : DecTable[FIELDS.default_description],
            final_decision : firstRuleToMatch ? firstRuleToMatch.decision : DecTable[FIELDS.default_decision],
            rules : DecTable[FIELDS.rules],
            fields : DecTable[FIELDS.fields],
            request : inputFields,
            all_matching_decisions : allMatchingDecisions
        }

        return this.queryBuilder.Create('Decision', dataToSave)
            .then(data => {
                if(data) {
                    const dataToReturn = CommonFunctions.filterObj(data, FIELDS.return_table_decision);

                    return Promise.resolve(dataToReturn)
                }
            })
    }

    isConditionMatched(fieldValue, fieldType, condition, conditionValue) {
        switch(condition) {

            case '$eq' :
                return CONDITION_ACTIONS[condition](fieldType, fieldValue, conditionValue);

            case '$not_eq' :
                return CONDITION_ACTIONS[condition](fieldType, fieldValue, conditionValue);

            case '$in' :
                return CONDITION_ACTIONS[condition](fieldType, fieldValue, conditionValue);

            case '$not_in' :
                return CONDITION_ACTIONS[condition](fieldType, fieldValue, conditionValue);
            
            case '$is_set' :
                return CONDITION_ACTIONS[condition](fieldValue);

            case '$is_null' :
                return CONDITION_ACTIONS[condition](fieldValue);

            case '$gt' :
                return CONDITION_ACTIONS[condition](fieldValue, conditionValue);

            case '$gte' :
                return CONDITION_ACTIONS[condition](fieldValue, conditionValue);

            case '$lt' :
                return CONDITION_ACTIONS[condition](fieldValue, conditionValue);

            case '$lte' :
                return CONDITION_ACTIONS[condition](fieldValue, conditionValue);

            case '$between' :
                return CONDITION_ACTIONS[condition](fieldValue, conditionValue);

            default : return false;
        }
    }

    listDecisions(params) {
        const pageNo = params[FIELDS.page_no] ? parseInt(params[FIELDS.page_no]) : 1;
        const perPage = params[FIELDS.per_page] ? parseInt(params[FIELDS.per_page]) : 15;
        
        const skipRecords = (pageNo - 1) * perPage;

        const findParams = {
            user_id : params[FIELDS.user_id],
            project_id : params[FIELDS.project_id]
        }

        if(params[FIELDS.table_id]) {
            findParams['table_id'] = params[FIELDS.table_id]
        }

        let count = null;

        return this.queryBuilder.Count('Decision', findParams)
            .then(dataCount => {
                if(dataCount === 0) {
                    return {
                        decisions : [],
                        count : 0
                    };
                }
                count = dataCount;
                return this.queryBuilder.Find('Decision', findParams, '-rules -fields -all_matching_decisions -request -updated_at', {skip : skipRecords, limit : perPage, sort: {'_id': -1}})
            })
            .then(data => {
                if(data.count === 0) {
                    return Promise.resolve(data);
                }
                return Promise.resolve({
                    decisions : data,
                    count : count
                });
            })
    }

    getDecisionDetails(params) {
        return this.queryBuilder.FindOne('Decision', params)
    }
}

export default new DecisionService(new QueryBuilderService());