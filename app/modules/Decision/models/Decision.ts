import { SchemaBuilder } from './../../../db/SchemaBuilder';

class Decision extends SchemaBuilder {

    constructor() {
        super({
            user_id: {
                type: String
            },
            project_id : {
                type: String
            },
            table_id : {
                type: String
            },
            table_info : {
                type : Object
            },
            rule_title : {
                type : String
            },
            rule_description : {
                type : String
            },
            final_decision : {
                type : String
            },
            rules : {
                type : Array
            },
            fields : {
                type : Array
            },
            all_matching_decisions : {
                type : Array
            },
            request : {
                type : Object
            }
        }, 'decisions');
    }
}

export default new Decision().Model;