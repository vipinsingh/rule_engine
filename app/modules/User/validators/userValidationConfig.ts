import Session from '../models/Session';
import User from '../models/User';

export const SIGNUP =  [
    {
        name : 'name',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '2,50',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'password',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '5,40',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'email',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'isEmail',
                message : 'invalid',
                bail  : true
            },
            {
                name : 'custom',
                callback : (value)  => {
                    return User.findUserByEmail(value)
                            .then(user => {
                                if(user) {
                                    return Promise.reject('duplicate');  
                                }
                                return true;
                            });
                },
                message : 'duplicate'
            },
        ]
    }
];

export const SIGNIN = [
    {
        name : 'email',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'isEmail',
                message : 'invalid'
            }
        ]
    },
    {
        name : 'password',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '5,40',
                message : 'invalid'
            },
        ]
    }
];

export const LOGOUT = [
    {
        name : 'user_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'custom',
                callback : (value)  => {
                    return Session.findOne({ user_id : value})
                        .then(user => {
                            if(!user) {
                                return Promise.reject('invalid');
                            }
                            return true;
                        })
                },
                message : 'invalid'
            },
        ]
    },
    {
        name : 'token',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    }
];

export const CHECK_AUTH_STATUS = [
    {
        name : 'user_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'token',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    }
];