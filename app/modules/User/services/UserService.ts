import { CommonFunctions } from './../../../shared/utils/CommonFunctions';
import { FIELDS } from './../../../config/fields';
import { QueryBuilderService } from './../../../shared/services/QueryBuilderService';
import { ERRORS } from './../../../lang/en/error_messages';
import User from './../models/User';

class UserService {
    private queryBuilder : QueryBuilderService;

    constructor(queryBuilder : QueryBuilderService) {
        this.queryBuilder = queryBuilder;
    }

    signup(userData) {
        const hash = CommonFunctions.encrypt(userData.password)
        userData[FIELDS.password] = hash;

        return this.queryBuilder.Create('User', userData)
            .then(data => {
                const dataToReturn = CommonFunctions.filterObj(data, FIELDS.return_user_signup)
                return Promise.resolve(dataToReturn);
            })
    }

    signin(params) {
        let userData : any;

        return User.findUserByEmail(params[FIELDS.email])
            .then(user => {
                if(!user) {
                    return Promise.reject(ERRORS.authentication_failed);
                }
                userData = user;
                const password = user[FIELDS.password];
                if(CommonFunctions.verifyPassword(params[FIELDS.password], password)) {
                    const token = CommonFunctions.generateSessionToken();
                    return this.queryBuilder.Create('Session', {
                        user_id : user[FIELDS.mongo_id],
                        session_token : token
                    })
                } else {
                    return Promise.reject(ERRORS.authentication_failed);
                }
            })
            .then(session => {
                let data = {
                    ...session['_doc'],
                    user_name : userData[FIELDS.name],
                    email : userData[FIELDS.email]
                };
                
                return Promise.resolve(data);
            })
    }

    logout(params) {
        return this.queryBuilder.Update('Session', {
            user_id : params[FIELDS.user_id],
            session_token : params[FIELDS.token]
        }, {
            is_active : false
        })
    }

    checkAuthStatus(params) {
        return this.queryBuilder.FindOne('Session', {
            user_id : params[FIELDS.user_id],
            session_token : params[FIELDS.token],
            is_active : true
        })
    }
}

export default new UserService(new QueryBuilderService());