import { Request, Response } from "express";
import { CommonFunctions } from "../../../shared/utils/CommonFunctions";
import { FIELDS } from "../../../config/fields";
import UserService from "../services/UserService";

class UserController {

    signup(req : Request, res : Response) {
        try {
            const userData = CommonFunctions.filterObj(req.body, FIELDS.signup);

            UserService.signup(userData)
                .then(user => {
                    return CommonFunctions.createSuccessResponse(req, res, user, 201);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                });
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    signin(req : Request, res : Response) {
        try {
            const params = CommonFunctions.filterObj(req.body, FIELDS.signin);

            UserService.signin(params)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data, 201);
                })
                .catch(error => {
                    if(error.type && error.type === 'AUTHENTICATION_FAILED') {
                        return CommonFunctions.createErrorResponse(req, res, error, 401);
                    }
                    return CommonFunctions.createErrorResponse(req, res, error);
                });
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    logout(req : Request, res : Response) {
        try {
            const params : any = CommonFunctions.filterObj(req.body, FIELDS.logout);

            UserService.logout(params)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, [], 200);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    checkAuthStatus(req : Request, res : Response) {
        try {
            const params : any = CommonFunctions.filterObj(req.body, FIELDS.logout);

            UserService.checkAuthStatus(params)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data, 200);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }
}

export default new UserController();