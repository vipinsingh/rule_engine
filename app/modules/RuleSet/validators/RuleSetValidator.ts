import { DecisionRuleValidator } from './../../DecisionRule/validators/DecisionRuleValidator';
import { ERRORS } from './../../../lang/en/error_messages';
import { Request, Response, NextFunction } from "express";
import { CommonFunctions } from "../../../shared/utils/CommonFunctions";

export class RuleSetValidator {

    decisionRuleValidator = new DecisionRuleValidator();

    validateRuleSet = (req : Request, res : Response, next : NextFunction) => {
        let ruleSet = req.body.rule_set_exp;
        let ruleSetLength = ruleSet.length;

        if(ruleSetLength < 2) {
            return CommonFunctions.createErrorResponse(req, res, [ERRORS.invalid_rule_set], 400);
        }

        if(ruleSet[0].operator === '$if' && ruleSet[ruleSetLength - 1].operator === '$else') {
            for(let i = 1; i < ruleSetLength - 1; i++) {
                if(ruleSet[i].operator !== '$else_if') {
                    return CommonFunctions.createErrorResponse(req, res, [ERRORS.invalid_rule_set], 400);
                }
            }
        } else {
            return CommonFunctions.createErrorResponse(req, res, [ERRORS.invalid_rule_set], 400);
        }

        for(let i = 0; i < ruleSetLength; i++) {

            if(ruleSet[i].decision_type === 'compute') {
                if(!this.decisionRuleValidator.validateExpression(ruleSet[i].decision_exp)) {
                    return CommonFunctions.createErrorResponse(req, res, [ERRORS.invalid_rule_set], 400);
                }
            } else if(!ruleSet[i].decision_type || !ruleSet[i].then_decision) {
                return CommonFunctions.createErrorResponse(req, res, [ERRORS.invalid_rule_set], 400);
            }

            if(ruleSet[i].operator !== '$else') {
                if(!this.decisionRuleValidator.validateExpression(ruleSet[i].rule_expression)) {
                    return CommonFunctions.createErrorResponse(req, res, [ERRORS.invalid_rule_set], 400);
                }
            }
        }

        next();
    }
}