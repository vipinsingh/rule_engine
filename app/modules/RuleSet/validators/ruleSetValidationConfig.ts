export const CREATE_RULE_SET = [
    {
        name : 'id',
        paramIn : 'param',
        validations : [
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/,
                optional : true
            }
        ]
    },
    {
        name : 'user_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'name',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '3,200',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'description',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '3,500',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rule_set_exp',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'isArray',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rule_set_exp.*.id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'integer',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rule_set_exp.*.operator',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rule_set_exp.*.then_decision',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rule_set_exp.*.decision_type',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rule_set_exp.*.displayValue',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rule_set_exp.*.rule_expression',
        paramIn : 'body',
        validations : [
            {
                name : 'isArray',
                message : 'invalid',
                optional : true
            },
        ]
    },
    {
        name : 'rule_set_exp.*.rule_expression.*.id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'integer',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rule_set_exp.*.rule_expression.*.control_type',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rule_set_exp.*.rule_expression.*.value',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rule_set_exp.*.rule_expression.*.displayValue',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rule_set_exp.*.rule_expression.*.attribute_type',
        paramIn : 'body',
        validations : []
    },
    {
        name : 'rule_set_exp.*.rule_expression.*.rule_data',
        paramIn : 'body',
        validations : []
    },
    {
        name : 'rule_set_exp.*.decision_exp',
        paramIn : 'body',
        validations : [
            {
                name : 'isArray',
                message : 'invalid',
                optional : true
            },
        ]
    },
    {
        name : 'rule_set_exp.*.decision_exp.*.id',
        paramIn : 'body',
        validations : [
            {
                name : 'integer',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rule_set_exp.*.decision_exp.*.control_type',
        paramIn : 'body',
        validations : []
    },
    {
        name : 'rule_set_exp.*.decision_exp.*.value',
        paramIn : 'body',
        validations : []
    },
    {
        name : 'rule_set_exp.*.decision_exp.*.displayValue',
        paramIn : 'body',
        validations : []
    },
    {
        name : 'rule_set_exp.*.decision_exp.*.attribute_type',
        paramIn : 'body',
        validations : []
    }
];

export const LIST_RULE_SET = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'page_no',
        paramIn : 'query',
        validations : [
            {
                name : 'integer',
                message : 'invalid',
                optional  : true
            },
            {
                name : 'custom',
                message : 'invalid',
                optional  : true,
                callback : (value) => {
                    return value > 0;
                }
            }
        ]
    },
    {
        name : 'per_page',
        paramIn : 'query',
        validations : [
            {
                name : 'integer',
                message : 'invalid',
                optional  : true
            },
            {
                name : 'custom',
                message : 'invalid',
                optional  : true,
                callback : (value) => {
                    return value > 0;
                }
            }
        ]
    }
];

export const GET_RULE_SET = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'id',
        paramIn : 'param',
        validations : [
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    }
];