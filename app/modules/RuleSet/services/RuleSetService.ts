import { QueryBuilderService } from "../../../shared/services/QueryBuilderService";
import { CommonFunctions } from './../../../shared/utils/CommonFunctions';
import { FIELDS } from './../../../config/fields';

class RuleSetService {
    private queryBuilder : QueryBuilderService;

    constructor(queryBuilder : QueryBuilderService) {
        this.queryBuilder = queryBuilder;
    }

    saveRuleSet(data) {
        return this.queryBuilder.Create('RuleSet', data)
    }

    listRuleSet(params) {
        const pageNo = params[FIELDS.page_no] ? parseInt(params[FIELDS.page_no]) : 1;
        const perPage = params[FIELDS.per_page] ? parseInt(params[FIELDS.per_page]) : 15;
        
        const skipRecords = (pageNo - 1) * perPage;

        const findParams = {
            user_id : params[FIELDS.user_id],
            project_id : params[FIELDS.project_id]
        }

        let count = null;

        return this.queryBuilder.Count('RuleSet', findParams)
            .then(dataCount => {
                if(dataCount === 0) {
                    return {
                        rule_set : [],
                        count : 0
                    };
                }
                count = dataCount;
                return this.queryBuilder.Find('RuleSet', findParams, 'name description created_at updated_at', {skip : skipRecords, limit : perPage})
            })
            .then(data => {
                if(data.count === 0) {
                    return Promise.resolve(data);
                }
                return Promise.resolve({
                    rule_set : data,
                    count : count
                });
            })
    }

    getRuleSet(params) {
        return this.queryBuilder.FindOne('RuleSet', params)
            .then(data => {
                if(!data) {
                    return Promise.resolve([]);
                }
                return Promise.resolve(data);
            })
    }

    updateRuleSet(id, data) {
        return this.queryBuilder.Update('RuleSet', { _id : id }, data, { new :  true })
    }
}

export default new RuleSetService(new QueryBuilderService());