import { CREATE_RULE_SET, GET_RULE_SET, LIST_RULE_SET } from './../validators/ruleSetValidationConfig';
import { RequestValidator } from './../../../shared/middlewares/RequestValidator';
import { Router } from 'express';
import { ValidationErrorHandler } from './../../../shared/middlewares/ValidationErrorHandler';
import ApiRouter from '../../../shared/ApiRouter/ApiRouter';
import { RuleSetValidator } from '../validators/RuleSetValidator';
import RuleSetController from '../controllers/RuleSetController';

class RuleSetRouter extends ApiRouter {
    router : Router;

    ruleSetValidator = new RuleSetValidator();

    constructor() {
        super();
        this.router = this.instance;
        this.setRuleSetRoutes();
    }

    setRuleSetRoutes() {
        this.router.post('/',
            RequestValidator.validateReq(CREATE_RULE_SET),
            ValidationErrorHandler.checkError,
            this.ruleSetValidator.validateRuleSet,
            RuleSetController.saveRuleSet            
        )

        this.router.get('/',
            RequestValidator.validateReq(LIST_RULE_SET),
            ValidationErrorHandler.checkError,
            RuleSetController.listRuleSet
        )

        this.router.get('/:id',
            RequestValidator.validateReq(GET_RULE_SET),
            ValidationErrorHandler.checkError,
            RuleSetController.getRuleSet
        )

        this.router.put('/:id',
            RequestValidator.validateReq(CREATE_RULE_SET),
            ValidationErrorHandler.checkError,
            RuleSetController.updateRuleSet
        )
    }
}

export default new RuleSetRouter().router;