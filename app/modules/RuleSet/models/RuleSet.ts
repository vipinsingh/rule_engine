import { Schema } from 'mongoose';
import { SchemaBuilder } from './../../../db/SchemaBuilder';

class RuleSet extends SchemaBuilder {

    constructor() {
        super({
            user_id: {
                type: String
            },
            project_id : {
                type: String
            },
            name : {
                type : String
            },
            description : {
                type : String
            },
            rule_set_exp : {
                type : [{
                    id : Number,
                    operator : String,
                    displayValue : String,
                    decision_type : String,
                    then_decision : String,
                    rule_expression : {
                        type : [{
                            id : Number,
                            control_type : String,
                            value : String,
                            displayValue : String,
                            attribute_type : String,
                            rule_data : {
                                type : Object
                            }
                        }]
                    },
                    decision_exp : {
                        type : [{
                            id : Number,
                            control_type : String,
                            value : String,
                            displayValue : String,
                            attribute_type : String
                        }]
                    }
                }]
            }
        }, 'rule_set');
    }
}

export default new RuleSet().Model;