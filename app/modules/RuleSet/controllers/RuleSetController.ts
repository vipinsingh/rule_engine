import { ERRORS } from './../../../lang/en/error_messages';
import { FIELDS } from './../../../config/fields';
import { Request, Response } from "express";
import { CommonFunctions } from "../../../shared/utils/CommonFunctions";
import RuleSetService from '../services/RuleSetService';

class RuleSetController {

    saveRuleSet(req : Request, res : Response) {
        try {
            const data : any = CommonFunctions.filterObj(req.body, FIELDS.save_rule_set);

            CommonFunctions.checkProjectExists(data.user_id, data.project_id)
                .then(project => {
                    if(!project) {
                        return CommonFunctions.createErrorResponse(req, res, ERRORS['resource_not_found'], 404);
                    }
                    return RuleSetService.saveRuleSet(data)
                })
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data, 201);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    listRuleSet(req : Request, res : Response) {
        try {

            const params = CommonFunctions.filterObj(req.query, FIELDS.list_rule_set);

            RuleSetService.listRuleSet(params)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    getRuleSet(req : Request, res : Response) {
        try {

            const params = CommonFunctions.filterObj(req.query, FIELDS.list_rule_set);

            params['_id'] = req.params.id;

            RuleSetService.getRuleSet(params)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    updateRuleSet(req : Request, res : Response) {
        try {

            const data : any = CommonFunctions.filterObj(req.body, FIELDS.save_rule_set);

            CommonFunctions.checkProjectExists(data.user_id, data.project_id)
                .then(project => {
                    if(!project) {
                        return CommonFunctions.createErrorResponse(req, res, ERRORS['resource_not_found'], 404);
                    }
                    return RuleSetService.updateRuleSet(req.params.id, data)
                })
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }
}

export default new RuleSetController();