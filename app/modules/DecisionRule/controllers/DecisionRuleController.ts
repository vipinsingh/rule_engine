import { ERRORS } from './../../../lang/en/error_messages';
import { FIELDS } from './../../../config/fields';
import { Request, Response } from "express";
import { CommonFunctions } from "../../../shared/utils/CommonFunctions";
import DecisionRuleService from "../services/DecisionRuleService";

class DecisionRuleController {

    saveExpression(req : Request, res : Response) {
        try {

            const data = CommonFunctions.filterObj(req.body, FIELDS.save_expression);

            DecisionRuleService.saveExpression(data)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data, 201);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    getExpressions(req : Request, res : Response) {
        try {

            const data = CommonFunctions.filterObj(req.query, FIELDS.get_expressions);

            DecisionRuleService.getExpressions(data)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    getRuleSuggestions(req : Request, res : Response) {
        try {

            const data = CommonFunctions.filterObj(req.query, FIELDS.get_rule_suggestions);

            DecisionRuleService.getRuleSuggestions(data)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    saveRule(req : Request, res : Response) {
        try {
            const data : any = CommonFunctions.filterObj(req.body, FIELDS.save_rule);

            CommonFunctions.checkProjectExists(data.user_id, data.project_id)
                .then(project => {
                    if(!project) {
                        return CommonFunctions.createErrorResponse(req, res, ERRORS['resource_not_found'], 404);
                    }
                    return DecisionRuleService.saveRule(data)
                })
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data, 201);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    listRules(req : Request, res : Response) {
        try {

            const params = CommonFunctions.filterObj(req.query, FIELDS.list_rules);

            DecisionRuleService.listRules(params)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    getRule(req : Request, res : Response) {
        try {

            const params = CommonFunctions.filterObj(req.query, FIELDS.list_rules);

            params['_id'] = req.params.id;

            DecisionRuleService.getRule(params)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    updateRule(req : Request, res : Response) {
        try {

            const data : any = CommonFunctions.filterObj(req.body, FIELDS.save_rule);

            CommonFunctions.checkProjectExists(data.user_id, data.project_id)
                .then(project => {
                    if(!project) {
                        return CommonFunctions.createErrorResponse(req, res, ERRORS['resource_not_found'], 404);
                    }
                    return DecisionRuleService.updateRule(req.params.id, data)
                })
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    runRule(req : Request, res : Response) {
        try {

            const DecRule = req.body.rule;

            const inputFields = CommonFunctions.filterObj(req.body, req.body.fields_arr);

            return DecisionRuleService.runRule(DecRule, inputFields)
                .then(data => {
                    return CommonFunctions.createSuccessResponse(req, res, data);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                })
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

}

export default new DecisionRuleController();