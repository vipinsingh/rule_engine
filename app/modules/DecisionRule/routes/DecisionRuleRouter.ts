import { DecisionRuleValidator } from './../validators/DecisionRuleValidator';
import { ValidationErrorHandler } from './../../../shared/middlewares/ValidationErrorHandler';
import { RequestValidator } from './../../../shared/middlewares/RequestValidator';
import { Router } from 'express';
import ApiRouter from '../../../shared/ApiRouter/ApiRouter';
import { SAVE_EXPRESSION, GET_EXPRESSIONS, SAVE_RULE, GET_RULE, LIST_RULES, RUN_RULE_DECISION, GET_RULE_SUGGESTIONS } from '../validators/decisionRuleValidationConfig';
import DecisionRuleController from '../controllers/DecisionRuleController';

class DecisionRuleRouter extends ApiRouter {
    router : Router;

    decisionRuleValidator = new DecisionRuleValidator();

    constructor() {
        super();
        this.router = this.instance;
        this.setDecisionRuleRoutes();
    }

    setDecisionRuleRoutes() {
        this.router.post('/expressions',
            RequestValidator.validateReq(SAVE_EXPRESSION),
            ValidationErrorHandler.checkError,
            this.decisionRuleValidator.validateSelectedExpression,
            DecisionRuleController.saveExpression
        );

        this.router.get('/expressions',
            RequestValidator.validateReq(GET_EXPRESSIONS),
            ValidationErrorHandler.checkError,
            DecisionRuleController.getExpressions
        );

        this.router.get('/rule_suggestions',
            RequestValidator.validateReq(GET_RULE_SUGGESTIONS),
            ValidationErrorHandler.checkError,
            DecisionRuleController.getRuleSuggestions
        );

        this.router.post('/',
            RequestValidator.validateReq(SAVE_RULE),
            ValidationErrorHandler.checkError,
            this.decisionRuleValidator.validateRuleExpression, 
            DecisionRuleController.saveRule
        );

        this.router.get('/',
            RequestValidator.validateReq(LIST_RULES),
            ValidationErrorHandler.checkError,
            DecisionRuleController.listRules
        );

        this.router.get('/:id',
            RequestValidator.validateReq(GET_RULE),
            ValidationErrorHandler.checkError,
            DecisionRuleController.getRule
        );

        this.router.put('/:id',
            RequestValidator.validateReq(SAVE_RULE),
            ValidationErrorHandler.checkError,
            this.decisionRuleValidator.validateRuleExpression,
            DecisionRuleController.updateRule
        );

        this.router.post('/:id/decision',
            RequestValidator.validateReq(RUN_RULE_DECISION),
            ValidationErrorHandler.checkError,
            this.decisionRuleValidator.validationDecisionReq,
            DecisionRuleController.runRule
        );
    }
}

export default new DecisionRuleRouter().router;