import { Schema } from 'mongoose';
import { SchemaBuilder } from './../../../db/SchemaBuilder';

class Expression extends SchemaBuilder {

    constructor() {
        super({
            user_id: {
                type: String
            },
            project_id : {
                type: String
            },
            expression : {
                type : [{
                    id : Number,
                    control_type : String,
                    value : String,
                    displayValue : String,
                    attribute_type : String,
                    expression_id : {
                        type : Schema.Types.ObjectId,
                        ref : 'expressions'
                    }
                }]
            },
            displayValue : {
                type : String
            }
        }, 'expressions');
    }
}

export default new Expression().Model;