import { Schema } from 'mongoose';
import { SchemaBuilder } from './../../../db/SchemaBuilder';

class Rule extends SchemaBuilder {

    constructor() {
        super({
            user_id: {
                type: String
            },
            project_id : {
                type: String
            },
            name : {
                type : String
            },
            description : {
                type : String
            },
            rule_expression : {
                type : [{
                    id : Number,
                    control_type : String,
                    value : String,
                    displayValue : String,
                    attribute_type : String,
                    expression_id : {
                        type : Schema.Types.ObjectId,
                        ref : 'expressions'
                    }
                }]
            },
            decision_type : {
                type : String
            },
            decision : {
                type : Schema.Types.Mixed
            },
            specify_default_decision : {
                type : String
            },
            default_decision_type : {
                type : String
            },
            default_decision : {
                type : Schema.Types.Mixed
            }
        }, 'rules');
    }
}

export default new Rule().Model;