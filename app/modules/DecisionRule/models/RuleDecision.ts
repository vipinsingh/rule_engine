import { SchemaBuilder } from './../../../db/SchemaBuilder';

class RuleDecision extends SchemaBuilder {

    constructor() {
        super({
            user_id: {
                type: String
            },
            project_id : {
                type: String
            },
            rule_id : {
                type: String
            },
            rule_name : {
                type : String
            },
            rule_description : {
                type : String
            },
            final_decision : {
                type : String
            },
            rule_expression : {
                type : Array
            },
            request : {
                type : Object
            }
        }, 'rule_decisions');
    }
}

export default new RuleDecision().Model;