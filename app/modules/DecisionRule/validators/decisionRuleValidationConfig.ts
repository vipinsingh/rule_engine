import Rule from "../models/Rule";

export const RUN_RULE_DECISION = [
    {
        name : 'id',
        paramIn : 'param',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail :  true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/,
                bail : true
            },
            {
                name : 'custom',
                message : 'invalid',
                callback : (value) => {
                    return Rule.findById(value)
                        .then(rule => {
                            if(!rule) {
                                return Promise.reject('invalid')
                            }
                            return true;
                        })
                }
            }
        ]
    }
];

export const SAVE_EXPRESSION = [
    {
        name : 'user_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'expression',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'isArray',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'expression.*.id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'integer',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'expression.*.control_type',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'expression.*.value',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'expression.*.displayValue',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'expression.*.attribute_type',
        paramIn : 'body',
        validations : []
    }
];

export const GET_EXPRESSIONS = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'value',
        paramIn : 'query',
        validations : []
    }
];


export const GET_RULE_SUGGESTIONS = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'rule_name',
        paramIn : 'query',
        validations : []
    }
];

export const SAVE_RULE = [
    {
        name : 'id',
        paramIn : 'param',
        validations : [
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/,
                optional : true
            }
        ]
    },
    {
        name : 'user_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    },
    {
        name : 'name',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '3,200',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'description',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '3,500',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'decision_type',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'lengthRange',
                params : '0,30',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'decision',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required'
            }
        ]
    },
    {
        name : 'specify_default_decision',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required'
            }
        ]
    },
    {
        name : 'default_decision_type',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required'
            }
        ]
    },
    {
        name : 'default_decision',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required'
            }
        ]
    },
    {
        name : 'rule_expression',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'isArray',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rule_expression.*.id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            },
            {
                name : 'integer',
                message : 'invalid'
            },
        ]
    },
    {
        name : 'rule_expression.*.control_type',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rule_expression.*.value',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rule_expression.*.displayValue',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'rule_expression.*.attribute_type',
        paramIn : 'body',
        validations : []
    }
];

export const LIST_RULES = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'page_no',
        paramIn : 'query',
        validations : [
            {
                name : 'integer',
                message : 'invalid',
                optional  : true
            },
            {
                name : 'custom',
                message : 'invalid',
                optional  : true,
                callback : (value) => {
                    return value > 0;
                }
            }
        ]
    },
    {
        name : 'per_page',
        paramIn : 'query',
        validations : [
            {
                name : 'integer',
                message : 'invalid',
                optional  : true
            },
            {
                name : 'custom',
                message : 'invalid',
                optional  : true,
                callback : (value) => {
                    return value > 0;
                }
            }
        ]
    }
];

export const GET_RULE = [
    {
        name : 'user_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'project_id',
        paramIn : 'query',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required'
            }
        ]
    },
    {
        name : 'id',
        paramIn : 'param',
        validations : [
            {
                name : 'pattern',
                message : 'invalid',
                pattern : /^[a-fA-F\d]{24}$/
            }
        ]
    }
];