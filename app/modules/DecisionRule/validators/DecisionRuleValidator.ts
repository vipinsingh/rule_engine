import { ERRORS } from './../../../lang/en/error_messages';
import { Request, Response, NextFunction } from "express";
import { CommonFunctions } from "../../../shared/utils/CommonFunctions";
import DecisionRuleService from '../services/DecisionRuleService';

export class DecisionRuleValidator {
    
    validationDecisionReq = async (req : Request, res : Response, next : NextFunction) => {
        try {
            const DecRule = await DecisionRuleService.getRule({ _id : req.params.id })

            let errors = [];

            const fieldsArr = [];
            
            const fields = [];

            const fieldTypeObj = {};

            DecRule.rule_expression.forEach(expression => {
                if(expression.control_type === 'attribute') {
                    if(!fields.includes(expression.value)) {
                        fieldsArr.push(expression);
                        fields.push(expression.value);
                    }
                }
                if(expression.control_type === 'expression') {
                    expression.value.forEach(exp => {
                        if(exp.control_type === 'attribute') {
                            if(!fields.includes(exp.value)) {
                                fieldsArr.push(exp);
                                fields.push(exp.value);
                            }
                        }
                    });
                }
            });

            if(DecRule.decision_type === 'compute' && Array.isArray(DecRule.decision)) {
                DecRule.decision.forEach(expression => {
                    if(expression.control_type === 'attribute') {
                        if(!fields.includes(expression.value)) {
                            fieldsArr.push(expression);
                            fields.push(expression.value);
                        }
                    }
                });
            }

            if(DecRule.default_decision_type === 'compute' && Array.isArray(DecRule.default_decision)) {
                DecRule.default_decision.forEach(expression => {
                    if(expression.control_type === 'attribute') {
                        if(!fields.includes(expression.value)) {
                            fieldsArr.push(expression);
                            fields.push(expression.value);
                        }
                    }
                });
            }

            fieldsArr.forEach(field => {
                if(req.body[field.value] === undefined) {
                    const {type , message} = ERRORS['required'];
                    errors.push({
                        type,
                        param : field.value,
                        message
                    });
                } else {
                    errors = CommonFunctions.validateAccordingToType(errors, field.attribute_type, req.body[field.value], field.value);
                }
                fieldTypeObj[field.value] = field.attribute_type;
            })

            if(errors.length > 0) {
                return CommonFunctions.createErrorResponse(req, res, errors, 400);
            }

            req.body.rule = DecRule;
            req.body.fields_arr = fields;
            req.body.fieldTypeObj = fieldTypeObj;

            next();
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }
    
    validateSelectedExpression = (req : Request, res : Response, next : NextFunction) => {
        if(!this.validateExpression(req.body.expression)) {
            return CommonFunctions.createErrorResponse(req, res, ERRORS['invalid_sel_expression'], 400);
        } else {
            next();
        }
    }

    validateRuleExpression = (req : Request, res : Response, next : NextFunction) => {
        let errorArr = [];

        if(!this.validateExpression(req.body.rule_expression)) {
            errorArr.push(ERRORS.invalid_rule_expression);
        }

        if(req.body.decision_type === 'compute') {
            if(!this.validateExpression(req.body.decision)) {
                errorArr.push(ERRORS.invalid_decision_expression);
            }
        }

        if(req.body.default_decision_type === 'compute') {
            if(!this.validateExpression(req.body.default_decision)) {
                errorArr.push(ERRORS.invalid_def_dec_expression);
            }
        }

        if(errorArr.length > 0) {
            return CommonFunctions.createErrorResponse(req, res, errorArr, 400);
        } else {
            next();
        }
    }

    validateExpression(expression) {
        try {
            if (!Array.isArray(expression) || expression.length < 2) {
                return false;
            }
            
            const { expString, valueObj } = CommonFunctions.getExpString(expression);
        
            // console.log('expstr:::', expString);
            // console.log('valueObj:::', valueObj);
    
            if(expString) {
                eval(expString);
                return true;
            } else {
                return false;
            }
        } catch (error) {
            console.log('error:::', error);
            return false;
        }
    }
}