import { CommonFunctions } from './../../../shared/utils/CommonFunctions';
import { FIELDS } from './../../../config/fields';
import { QueryBuilderService } from './../../../shared/services/QueryBuilderService';

class DecisionRuleService {
    private queryBuilder : QueryBuilderService;

    constructor(queryBuilder : QueryBuilderService) {
        this.queryBuilder = queryBuilder;
    }

    saveExpression(data) {
        let display_value = '';

        data.expression.forEach((exp, index) => {
            display_value += exp[FIELDS.displayValue];
            
            if(index !== (data.expression.length - 1)) {
                display_value += ' ';
            }
        });

        data.expression = this.deleteExpressionValue(data.expression);

        data[FIELDS.displayValue] = display_value;

        return this.queryBuilder.Create('Expression', data)
    }

    getExpressions(params) {
        const fetchParams = {
            user_id : params[FIELDS.user_id],
            project_id : params[FIELDS.project_id],
            displayValue : params[FIELDS.value] ? { $regex : '.*' + params[FIELDS.value] + '.*' } : ''
        }

        return this.queryBuilder.Find('Expression', fetchParams).populate('expression.expression_id', 'expression')
            .then(data => {
                if(!data || data.length === 0) {
                    return Promise.resolve(data);
                }
                const newData = JSON.parse(JSON.stringify(data));

                const finalData = newData.map(expressionObj => {
                    const newExp = this.getPopulatedNewExpression(expressionObj.expression);
                    expressionObj.expression = newExp;
                    return expressionObj;
                });

                return Promise.resolve(finalData);
            })
    }

    getRuleSuggestions(params) {
        const fetchParams = {
            user_id : params[FIELDS.user_id],
            project_id : params[FIELDS.project_id],
            name : params[FIELDS.rule_name] ? { $regex : '.*' + params[FIELDS.rule_name] + '.*', $options : 'i' } : ''
        }

        return this.queryBuilder.Find('Rule', fetchParams, 'name description decision_type default_decision_type')
    }

    saveRule(data) {
        data.rule_expression = data.rule_expression.map(exp => {
            if(exp[FIELDS.control_type] === 'expression') {
                delete exp[FIELDS.value];
            }
            return exp;
        })
        
        return this.queryBuilder.Create('Rule', data)
    }

    listRules(params) {
        const pageNo = params[FIELDS.page_no] ? parseInt(params[FIELDS.page_no]) : 1;
        const perPage = params[FIELDS.per_page] ? parseInt(params[FIELDS.per_page]) : 15;
        
        const skipRecords = (pageNo - 1) * perPage;

        const findParams = {
            user_id : params[FIELDS.user_id],
            project_id : params[FIELDS.project_id]
        }

        let count = null;

        return this.queryBuilder.Count('Rule', findParams)
            .then(dataCount => {
                if(dataCount === 0) {
                    return {
                        rules : [],
                        count : 0
                    };
                }
                count = dataCount;
                return this.queryBuilder.Find('Rule', findParams, 'name description created_at updated_at', {skip : skipRecords, limit : perPage})
            })
            .then(data => {
                if(data.count === 0) {
                    return Promise.resolve(data);
                }
                return Promise.resolve({
                    rules : data,
                    count : count
                });
            })
    }

    getRule(params) {
        return this.queryBuilder.FindOne('Rule', params).populate('rule_expression.expression_id', 'expression')
            .then(data => {
                if(!data) {
                    return Promise.resolve([]);
                }
                const newData = JSON.parse(JSON.stringify(data));

                const newExp = this.getPopulatedNewExpression(newData.rule_expression);
                newData.rule_expression = newExp;

                return Promise.resolve(newData);
            })
    }

    private deleteExpressionValue (expression) {
        return expression.map(exp => {
            if(exp[FIELDS.control_type] === 'expression') {
                delete exp[FIELDS.value];
            }
            return exp;
        })
    }

    private getPopulatedNewExpression(expression) {
        return expression.map(exp => {
            if(exp[FIELDS.control_type] === 'expression') {
                exp[FIELDS.value] = exp[FIELDS.expression_id].expression;
                exp[FIELDS.expression_id] = exp[FIELDS.expression_id].id;
            }
            return exp;
        })
    }

    updateRule(id, data) {
        data.rule_expression = this.deleteExpressionValue(data.rule_expression);

        return this.queryBuilder.Update('Rule', { _id : id }, data, { new :  true })
    }

    runRule(DecRule, inputFields) {
        let ruleExpString = CommonFunctions.getExpString(DecRule[FIELDS.rule_expression], {}, 'value').expString;

        ruleExpString = ruleExpString.replace(/valueObj/g, 'inputFields');

        const ruleResult = eval(ruleExpString);

        let dataToSave = {
            user_id          : DecRule[FIELDS.user_id],
            project_id       : DecRule[FIELDS.project_id],
            rule_id          : DecRule[FIELDS.id],
            rule_name        : DecRule[FIELDS.name],
            rule_description : DecRule[FIELDS.description],
            rule_expression  : DecRule[FIELDS.rule_expression],
            request          : inputFields
        }

        if(DecRule[FIELDS.decision_type] === 'rule_result') {
            dataToSave['final_decision'] = isNaN(ruleResult) ? ruleResult : Number(ruleResult).toFixed(2);
        } else if(ruleResult) {
            if(DecRule[FIELDS.decision_type] === 'compute') {
                let decExpString = CommonFunctions.getExpString(DecRule[FIELDS.decision], {}, 'value').expString;
                decExpString = decExpString.replace(/valueObj/g, 'inputFields');
                dataToSave['final_decision'] = Number(eval(decExpString)).toFixed(2);
            } else {
                dataToSave['final_decision'] = DecRule[FIELDS.decision];
            }
        } else {
            if(DecRule[FIELDS.specify_default_decision] === 'yes') {
                if(DecRule[FIELDS.default_decision_type] === 'compute') {
                    let decExpString = CommonFunctions.getExpString(DecRule[FIELDS.default_decision], {}, 'value').expString;
                    decExpString = decExpString.replace(/valueObj/g, 'inputFields');
                    dataToSave['final_decision'] = Number(eval(decExpString)).toFixed(2);
                } else {
                    dataToSave['final_decision'] = DecRule[FIELDS.default_decision];
                }
            } else {
                dataToSave['final_decision'] = false;
            }
        }

        return this.queryBuilder.Create('RuleDecision', dataToSave)
            .then(data => {
                if(data) {
                    const dataToReturn = CommonFunctions.filterObj(data, FIELDS.return_rule_decision);
                    return Promise.resolve(dataToReturn);
                }
            })
    }
}

export default new DecisionRuleService(new QueryBuilderService());