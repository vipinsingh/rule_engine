import dotenv from 'dotenv';

dotenv.config();

const MONGO_URI  = process.env.MONGO_URI;
const MASTER_URL = process.env.MASTER_URL;
const NODE_ENV   = process.env.NODE_ENV;

export {
    MONGO_URI,
    MASTER_URL,
    NODE_ENV
};