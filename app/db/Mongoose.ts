import mongoose from 'mongoose';
import { MONGO_URI } from '../config/config';

export default class Mongoose {

    private static instance: Mongoose;

    constructor() {
        this.connect();
    }

    public static getInstance() : Mongoose {
        if (!Mongoose.instance) {
            Mongoose.instance = new Mongoose();
        }

        return Mongoose.instance;
    }

    connect() {
        mongoose.Promise = global.Promise;
        mongoose.connect(MONGO_URI, { useNewUrlParser : true,  useUnifiedTopology : true });
        mongoose.connection
            .once('open', () => console.log('connected to rule_engine_db'))
            .on('error', (error) => console.log(error));
    }
}