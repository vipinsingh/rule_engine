import { options } from '../config/schemaOptions.config';
import { Schema, Model, model } from 'mongoose';

export class SchemaBuilder {
    private static schema : Schema
    private model : Model;
    private collectionName : String;

    constructor(schemaObj, collectionName = '') {
        SchemaBuilder.schema = new Schema({
            ...schemaObj,
            created_at: { type: Schema.Types.Mixed },
            updated_at: { type: Schema.Types.Mixed }
        }, options);

        this.collectionName = collectionName ? collectionName : this.constructor['name'];
    }

    get schema() {
        return SchemaBuilder.schema
    }

    get Model() {
        this.model = model(this.collectionName, this.schema);
        return this.model;
    }
}