import Decision from './../modules/Decision/models/Decision';
import DecisionTable from './../modules/DecisionTable/models/DecisionTable';
import Project from './../modules/Project/models/Project';
import Session from './../modules/User/models/Session';
import User from './../modules/User/models/User';
import Expression from './../modules/DecisionRule/models/Expression';
import Rule from './../modules/DecisionRule/models/Rule';
import RuleSet from './../modules/RuleSet/models/RuleSet';
import RuleDecision from './../modules/DecisionRule/models/RuleDecision';
import Mongoose from './Mongoose';

export class DatabaseHandler {
    private static instance : DatabaseHandler

    private _models;

    constructor() {
        Mongoose.getInstance();

        this._models = {
            User          : User,
            Session       : Session,
            Project       : Project,
            DecisionTable : DecisionTable,
            Decision      : Decision,
            Expression    : Expression,
            Rule          : Rule,
            RuleDecision  : RuleDecision,
            RuleSet       : RuleSet,
        }
    }

    public static get Models()  {
        if (!DatabaseHandler.instance) {
            DatabaseHandler.instance = new DatabaseHandler();
        }
        return DatabaseHandler.instance._models;

    }
}