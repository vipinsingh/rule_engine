export const CONDITION_ACTIONS = {
    '$eq' : (fieldType, fieldValue, conditionValue) => {
        if(fieldType === 'boolean') {
            conditionValue = conditionValue === 'true' ? true : false;
            if(["true", "false"].includes(fieldValue)) {
                fieldValue = fieldValue === 'true' ? true : false;
            }
            return fieldValue === conditionValue;
        }
        return fieldValue == conditionValue;
    },
    '$not_eq' : (fieldType, fieldValue, conditionValue) => {
        if(fieldType === 'boolean') {
            conditionValue = conditionValue === 'true' ? true : false;
            if(["true", "false"].includes(fieldValue)) {
                fieldValue = fieldValue === 'true' ? true : false;
            }
            return fieldValue !== conditionValue;
        }
        return fieldValue != conditionValue;
    },
    '$in' : (fieldType, fieldValue, conditionValue) => {
        let condValues = conditionValue.split(',');
        if(fieldType === 'number') {
            condValues = condValues.map(value => {
                return Number(value);
            })
            return condValues.includes(Number(fieldValue));
        }
        return condValues.includes(fieldValue);
    },
    '$not_in' : (fieldType, fieldValue, conditionValue) => {
        let cond_values = conditionValue.split(',');
        if(fieldType === 'number') {
            cond_values = cond_values.map(value => {
                return Number(value);
            })
            return !cond_values.includes(Number(fieldValue));
        }
        return !cond_values.includes(fieldValue);
    },
    '$is_set' : (fieldValue) => {
        if(fieldValue === null || fieldValue === 'null') {
            return false;
        }
        return true;
    },
    '$is_null' : (fieldValue) => {
        if(fieldValue === null || fieldValue === 'null') {
            return true;
        }
        return false;
    },
    '$gt' : (fieldValue, conditionValue) => {
        conditionValue = Number(conditionValue);
        return fieldValue > conditionValue;
    },
    '$gte' : (fieldValue, conditionValue) => {
        conditionValue = Number(conditionValue);
        return fieldValue >= conditionValue;
    },
    '$lt' : (fieldValue, conditionValue) => {
        conditionValue = Number(conditionValue);
        return fieldValue < conditionValue;
    },
    '$lte' : (fieldValue, conditionValue) => {
        conditionValue = Number(conditionValue);
        return fieldValue <= conditionValue;
    },
    '$between' : (fieldValue, conditionValue) => {
        let condit_values = conditionValue.split(',');
        condit_values = condit_values.map(value => {
            return Number(value);
        })
        return (fieldValue  >= condit_values[0] && fieldValue <= condit_values[1]);
    }
}