import { ERRORS } from './../../lang/en/error_messages';
import ReqResLog from './../models/RequestResponseLog';
import { NODE_ENV } from './../../config/config';
import bcrypt from 'bcrypt';
import { Request, Response } from 'express';
import Project from '../../modules/Project/models/Project';

export class CommonFunctions {
    
    static filterObj(obj, filterArr) {
        const newObj = {}
        filterArr.forEach(el => {
            if(obj[el] !== undefined) {
                newObj[el] = obj[el];
            }
        });

        return newObj;
    }

    static checkProjectExists(user_id, project_id) {
        return Project.findOne({
            _id : project_id,
            user_id : user_id
        })
    }

    static encrypt(str : string) {
        const encryptedStr = bcrypt.hashSync(str, 10);

        return encryptedStr;
    }

    static verifyPassword(password : string, hash : string) {
        return bcrypt.compareSync(password, hash);
    }

    static generateSessionToken() {
        let str = '';
        for(let i = 0; i< 3; i++) {
            str+=(Math.random().toString(36).substring(2, 36) + Math.random().toString(36).substring(2, 15));
        }
        return str;
    }

    static createSuccessResponse(req : Request, res : Response, data : object | null = null, code : number = 200) {
        let response = {
            status : 'success',
            code :  code,
            data
        };

        this.logRequestResponse(req, response);

        return res.status(code).json(response);
    }

    private static async logRequestResponse(req : Request, response) {
        
        return await ReqResLog.findOneAndUpdate({ _id : req.body.log_id}, {
            response      : response,
            status_code   : response.code
        }, { useFindAndModify: false });
    }

    static createErrorResponse(req : Request, res: Response, errors : object | any | null = null, code = 500) {
        let response : any = {
            status : 'failed',
            code : code
        }

        if(code === 500) {
            if(NODE_ENV === 'dev') {
                response.errors = String(errors.stack ? errors.stack : errors);
            } else {
                response.errors = ERRORS['action_failed'];
            }
        } else {
            response.errors = errors;
        }

        this.logRequestResponse(req, response);

        return res.status(code).json(response);
    }

    static validateAccordingToType (errors, fieldType, fieldValue, apiKey) {

        let error = null;
        
        switch(fieldType) {
            case 'string' :
                if(!this.validateStringField(fieldValue)) {
                    error = {...ERRORS['invalid_string']};
                }
                break;
            case 'number' :
                if(!this.validateNumberField(fieldValue)) {
                    error = {...ERRORS['invalid_number']};
                }
                break;
            case 'boolean' :
                if(!this.validateBooleanField(fieldValue)) {
                    error = {...ERRORS['invalid_boolean']};
                }
                break;
            default : break;
        }

        if(error) {
            const {type , message} = error;
            errors.push({
                type,
                param : apiKey,
                message
            });
        }

        return errors;
    }

    private static validateStringField(value) {
        if(value && typeof value !== 'string')  {
            return false;
        }
        return true;
    }

    private static validateNumberField (value) {
        if(value && value !== 'null') {
            if(typeof value === 'string') {
                let pattern = /^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$/;
                return pattern.test(value);
            } else if(typeof value !== 'number') {
                return false;
            }
        }   
        return true;
    }

    private static validateBooleanField (value) {
        if(!["true", "false", true, false, '', 'null', null].includes(value)) {
            return false;
        }
        return true;
    }

    static getExpString(expression, valueObj = {}, fieldToConsider = 'displayValue') {
        let expString = '';

        const valueForFieldType = {
            'number': 10,
            'string': 'Teststring',
            'boolean': true
        }

        expression.forEach((exp, index) => {
            let str = null;

            switch (exp.control_type) {
                case 'attribute':
                    valueObj[exp[fieldToConsider]] = valueForFieldType[exp.attribute_type];
                    if(expression[index + 1] && expression[index + 1].control_type === 'mathOperators' && ['in', 'not in', 'between'].includes(expression[index + 1].displayValue)) {
                        break;
                    }
                    expString = expString + `valueObj["${exp[fieldToConsider]}"] `;
                    break;

                case 'rule' :
                    valueObj[exp.displayValue] = 'dummyValue';
                    if(expression[index + 1] && expression[index + 1].control_type === 'mathOperators' && ['in', 'not in', 'between'].includes(expression[index + 1].displayValue)) {
                        break;
                    }
                    expString = expString + `valueObj["${exp.displayValue}"] `;
                    break;

                case 'expression':
                    expString = expString + this.getExpString(exp.value, {}, fieldToConsider).expString
                    valueObj = {
                        ...valueObj,
                        ...this.getExpString(exp.value).valueObj
                    }
                    break;

                case 'mathOperators':
                    
                    switch(exp.displayValue) {
                        case '^' :
                            str = '** ';
                            break;

                        case '=' :
                            str = '=== ';
                            break;

                        case '!=' :
                            str = '!== ';
                            break;
                        
                        case 'in' :
                            str = this.getExpStringForInNotInBtw(expression, index, exp.displayValue, fieldToConsider);
                            break;

                        case 'not in' :
                            str = this.getExpStringForInNotInBtw(expression, index, exp.displayValue, fieldToConsider);
                            break;

                        case 'is set' :
                            str = '!== undefined ';
                            break;

                        case 'is null' :
                            str = '=== null ';
                            break;

                        case 'between' :
                            str = this.getExpStringForInNotInBtw(expression, index, exp.displayValue, fieldToConsider);
                            break;
                            
                        case '= true' :
                            str = '=== true ';
                            break;

                        case '= false' :
                            str = '=== false ';
                            break;

                        default :
                            str = exp.displayValue + ' ';
                            break;
                    }

                    expString = expString + str;
                    break;

                case 'constant' :
                    if(expression[index - 1] && expression[index - 1].control_type === 'mathOperators' && ['in', 'not in', 'between'].includes(expression[index - 1].displayValue)) {
                        break;
                    }
                    if(this.findTypeOfConstant(exp.value) === 'string') {
                        str = `"${exp.value}" `
                    } else {
                        str = exp.value + ' ';
                    }
                    expString = expString +  str;
                    break;

                case 'operator' :
                    
                    switch(exp.displayValue) {
                        case 'AND' :
                            str = '&& ';
                            break;
                        
                        case 'OR' :
                            str = '|| ';
                            break;

                        case 'NOT' :
                            str = '!';
                            break;

                        default :
                            str = exp.displayValue + ' ';
                            break;
                    }

                    expString = expString + str;
                    break;

                default: break;
            }
        })

        return {
            expString : expString,
            valueObj : valueObj
        };
    }

    private static findTypeOfConstant(constant) {
        if(isNaN(constant)) {
            return 'string';
        } else {
            return 'number';
        }
    }

    private static getExpStringForInNotInBtw(expression, index, operator, fieldToConsider) {
        let expString = null;
        let attributeName = null;
        let constantArr = null;
    
        if(expression[index + 1] && expression[index + 1].control_type === 'constant') {
            constantArr = expression[index + 1].value.split(',');
            constantArr = constantArr.map(constant => {
                constant = `"${constant}"`;
                return constant;
            })
        }

        if(expression[index - 1] && expression[index - 1].control_type === 'attribute') {
            attributeName = `valueObj["${expression[index - 1][fieldToConsider]}"]`;
        }

        if(expression[index - 1] && expression[index - 1].control_type === 'rule') {
            attributeName = `valueObj["${expression[index - 1].displayValue}"]`;
        }
    
        if(constantArr && attributeName) {
            switch(operator) {
                case 'in' :
                    expString = `[${constantArr}].includes(${attributeName}) `;
                    break;
        
                case 'not in' :
                    expString = `![${constantArr}].includes(${attributeName}) `;
                    break;
        
                case 'between' :
                    expString = `${attributeName} >= ${constantArr[0]} && ${attributeName} <= ${constantArr[1] ? constantArr[1] : ''} `;
                    break;
        
                default : break;
            }
        }
    
        return expString ? expString : '';
    }
}