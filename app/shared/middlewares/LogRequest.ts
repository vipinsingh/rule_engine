import { CommonFunctions } from './../utils/CommonFunctions';
import { Request, Response, NextFunction } from "express";
import ReqResLog from '../models/RequestResponseLog';

export class LogRequest {

    public static initiateLog(req : Request, res : Response, next : NextFunction) {
        return new ReqResLog({
            request_url   : req.baseUrl + req.url,
            method        : req.method,
            request_IP    : req.ip,
            request_header: req.headers,
            request_body  : req.body
        }).save()
            .then(data => {
                if(data)  {
                    req.body.log_id = data.id;
                }
                next();
            })
            .catch(err => {
                return CommonFunctions.createErrorResponse(req, res, err);
            })
    }
}