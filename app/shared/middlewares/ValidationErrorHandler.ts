import { ERRORS } from './../../lang/en/error_messages';
import {validationResult} from 'express-validator';
import {Request, Response, NextFunction} from 'express';
import { CommonFunctions } from '../utils/CommonFunctions';

export class ValidationErrorHandler {
    
    static checkError (req : Request, res : Response, next : NextFunction) {
        
        const errors = validationResult(req);

        if(!errors.isEmpty()) {
            const errorArr = errors.array().map(err => {
                const {type , message} = ERRORS[err.msg];
                return {
                    type,
                    parameter : err.param,
                    message
                }
            });
            
            return CommonFunctions.createErrorResponse(req, res, errorArr, 400);
        }

        next();
    }
}