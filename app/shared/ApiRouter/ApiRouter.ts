import {Router} from "express";

export default class ApiRouter {
    private static instance : Router

    constructor() {}

    get instance() {
        // if(!ApiRouter.instance) {
            ApiRouter.instance = Router();
        // }
        return ApiRouter.instance;
    }
}