import { DatabaseHandler } from "../../db/DatabaseHandler";

export class QueryBuilderService {

    Create(model : string, data : any) {
        return new DatabaseHandler.Models[model](data).save()
    }

    Find(model : string, params : any, project : string = null, options : any = {}) {
        return DatabaseHandler.Models[model].find(params, project, options)
    }

    FindOne(model : string, params : any, project : string = null) {
        return DatabaseHandler.Models[model].findOne(params, project)
    }

    Update(model : string, params : any, data : any, options : any = {}) {
        return DatabaseHandler.Models[model].findOneAndUpdate(params, data, { ...options, useFindAndModify: false})
    }

    Count(model : string, params : any = {}) {
        return DatabaseHandler.Models[model].countDocuments(params)
    }
}