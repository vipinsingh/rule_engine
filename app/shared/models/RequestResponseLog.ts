import { SchemaBuilder } from '../../db/SchemaBuilder';

class ReqResLog extends SchemaBuilder {

    constructor() {
        super({
            request_url : {
                type    : String, 
                required: true
            },
            method : {
                type    : String,                                          
                required: true
            },
            request_IP : {
                type    : String
            },
            request_header : {
                type    : Object, 
                required: true
            },
            request_body : {
                type : Object, 
            },
            response : {
                type    : Object
            },
            status_code : {
                type    : Number
            },
        }, 'req_res_log');
    }
}

export default new ReqResLog().Model;