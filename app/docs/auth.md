## User Sign Up

**URL** : `api/v1/user/signup`

**Method** : `POST`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{ 
    "name": "User 1", 
    "email": " user1@b2cdev.com ", 
    "password": "*********" (length 5 to 16)
} 
```

## Success Response

**Code** : `201`

**Response**

```json
{ 
    "status": "success", 
    " code": 201, 
    "data": { 
        "id": "5ef7228b7e50148ba0e5f7b9", 
        "name": "User 1", 
        "email": "user1@b2cdev.com", 
        "created_at": 1593254539, 
        "updated_at": 1593254539 
    } 
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{ 
    "status": "failed", 
    "code": 400, 
    "errors": [ 
        { 
            "type": "FIELD_DUPLICATE", 
            "parameter": "email", 
            "message": "The value of the field is already used for another resource." 
        } 
    ]
}
```

## User Sign In

**URL** : `api/v1/user/signin`

**Method** : `POST`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{ 
    "email": " user1@b2cdev.com ", 
    "password": "*********"  
}  
```

## Success Response

**Code** : `201`

**Response**

```json
{ 
    "status": "success", 
    " code": 201, 
    "data": { 
        "is_active": true, 
        "_id": "5ef724ad7e50148ba0e5f7bd", 
        "user_id": "5ef71fca7e50148ba0e5f7a7", 
        "session_token":"94rthi9sham3gby74xt9wohmz5snegvvato1xpzsw2vqa1ahvgrtcbw5uzajbcgdm7",
        "created_at": 1593255085, 
        "updated_at": 1593255085, 
        "user_name": "User 1", 
        "email": "user1@b2cdev.com" 
    }
} 
```

## Error Response 

**Code** : `401`

**Response**

```json
{ 
    "status": "failed", 
    "code": 401, 
    "errors": [ 
        { 
            "type": " AUTHENTICATION_FAILED ", 
            "message": " Used authentication credentials are invalid." 
        } 
    ]
}
```

## Logout

**URL** : `api/v1/user/logout`

**Method** : `POST`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{ 
    " user_id ": "5e9fcca259d176c0ec13a508", 
    "token": "9rx21wkcqst6n8fnqy3339eewzurehstua15drqbb2mhsja3jcjrej86ni653iwft"  
}     
```

## Success Response

**Code** : `200`

**Response**

```json
{ 
    "status": "success", 
    " code": 200, 
    "data": []
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{ 
    "status": "failed", 
    "code": 400, 
    "errors": [ 
        { 
            "type": "FIELD_INVALID", 
            "parameter": "user_id", 
            "message": "The value of the field is invalid." 
        } 
    ]
}
```

## Check Auth Status when Page refreshes

**URL** : `api/v1/user/auth_status`

**Method** : `POST`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{ 
    " user_id ": "5e9fcca259d176c0ec13a508", 
    "token": "9rx21wkcqst6n8fnqy3339eewzurehstua15drqbb2mhsja3jcjrej86ni653iwft"  
}     
```

## Success Response

**Code** : `200`

**Response**

```json
{ 
    "status": "success", 
    " code": 200, 
    "data": {
        "_id": "5ef724ad7e50148ba0e5f7bd", 
        "is_active": true, 
        "user_id": "5ef71fca7e50148ba0e5f7a7", 
        "session_token":"94rthi9sham3gby74xt9wohmz5snegvvato1xpzsw2vqa1ahvgrtcbw5uzajbcgdm7",
        "created_at": 1593255085,
        "updated_at": 1593255085, 
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{ 
    "status": "failed", 
    "code": 400, 
    "errors": [ 
        { 
            "type": "FIELD_INVALID", 
            "parameter": "user_id", 
            "message": "The value of the field is invalid." 
        } 
    ]
}
```