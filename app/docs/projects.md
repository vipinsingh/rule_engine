## Create Project

**URL** : `api/v1/projects`

**Method** : `POST`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{
	"user_id" : "5e6e2cf5ba6f9436cce68bb8",
	"name" : "Test Project 1",
	"description" : "Test Project 1"
} 
```

## Success Response

**Code** : `201`

**Response**

```json
{
    "status": "success",
    "code": 201,
    "data": {
        "id": "5ef8df7f7e50148ba0e5f871",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "name": "Test Project 1",
        "description": "Test Project 1",
        "created_at": "2020-06-28T18:20:47.000Z",
        "updated_at": "2020-06-28T18:20:47.000Z"
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "description",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## List Projects

**URL** : `api/v1/projects`

**Method** : `GET`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
page_no:1    (<optional><default = 1>)
per_page:2  (<optional><default = 15>)

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "projects": [
            {
                "id": "5e7776ea413d55a10477b61f",
                "user_id": "5e6e2cf5ba6f9436cce68bb8",
                "name": "Test Project 1",
                "description": "Test Description 1",
                "created_at": "2020-03-22T14:32:10.000Z",
                "updated_at": "2020-03-22T14:32:10.000Z"
            },
            {
                "id": "5e778164413d55a10477b654",
                "user_id": "5e6e2cf5ba6f9436cce68bb8",
                "name": "Test Project 2",
                "description": "Test Description 2",
                "created_at": "2020-03-22T15:16:52.000Z",
                "updated_at": "2020-03-22T15:16:52.000Z"
            }
        ],
        "count": 5
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```