## Create Decision Table

**URL** : `api/v1/tables`

**Method** : `POST`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{
    "user_id" : "5e6e2cf5ba6f9436cce68bb8",
    "project_id" : "5e7776ea413d55a10477b61f",
    "name" : "Test Table 1",
    "description" : " Test Description 1",
    "fields" : [
    	{
    		"id" :  1,
    		"title" : "Age",
    		"api_key" : "age",
    		"fieldType" : "number"
    	}
    ],
    "rules" : [
    	{
    		"id" :  1,
    		"title" : "Age above threshold",
    		"description" : "Age > 18",
	    	"conditions" : [
	    		{
	    			"rule_id" : 1,
	    			"field_id" : 1,
	    			"field_name" : "age",
	    			"condition" : "$gt",
	    			"value" : "18"
	    		}
	    	],
	    	"decision" : "approve"
	    }
    ],
    "table_type" : "decision",
    "decision_type" : "string",
    "default_title" : "Default  Title",
    "default_description" : "Default Description",
    "default_decision" : "reject"
} 
```

## Success Response

**Code** : `201`

**Response**

```json
{
    "status": "success",
    "code": 201,
    "data": {
        "id": "5ef8e3597e50148ba0e5f876",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "name": "Test Table 1",
        "description": " Test Description 1",
        "fields": [
            {
                "_id": "5ef8e3597e50148ba0e5f877",
                "id": 1,
                "title": "Age",
                "api_key": "age",
                "fieldType": "number"
            }
        ],
        "rules": [
            {
                "_id": "5ef8e3597e50148ba0e5f878",
                "id": 1,
                "title": "Age above threshold",
                "description": "Age > 18",
                "conditions": [
                    {
                        "_id": "5ef8e3597e50148ba0e5f879",
                        "rule_id": 1,
                        "field_id": 1,
                        "field_name": "age",
                        "condition": "$gt",
                        "value": "18"
                    }
                ],
                "decision": "approve"
            }
        ],
        "table_type": "decision",
        "decision_type": "string",
        "default_title": "Default  Title",
        "default_description": "Default Description",
        "default_decision": "reject",
        "created_at": "2020-06-28T18:37:13.000Z",
        "updated_at": "2020-06-28T18:37:13.000Z"
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "description",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## List Decision Tables

**URL** : `api/v1/tables`

**Method** : `GET`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
project_id:5e7776ea413d55a10477b61f
page_no:1    (<optional><default = 1>)
per_page:2  (<optional><default = 15>)

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "tables": [
            {
                "id": "5e87734d6be2080b95f2f567",
                "name": "Test Table 1",
                "description": " Test Description 1",
                "updated_at": "2020-04-22T10:09:14.000Z",
                "created_at": "2020-04-03T17:33:01.000Z"
            },
            {
                "id": "5e8ddd417054790961e1b1bc",
                "name": "Knockout",
                "description": "Criteria For Knockout",
                "created_at": "2020-04-08T14:18:41.000Z",
                "updated_at": "2020-04-13T18:29:24.000Z"
            }
        ],
        "count": 6
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Update Decision Table

**URL** : `api/v1/tables/5e87734d6be2080b95f2f567`

**Method** : `PUT`

**Path Param** : table_id

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{
    "user_id" : "5e6e2cf5ba6f9436cce68bb8",
    "project_id" : "5e7776ea413d55a10477b61f",
    "name" : "Test Table 1",
    "description" : " Dummy Description 1",
    "fields" : [
    	{
    		"id" :  1,
    		"title" : "Age",
    		"api_key" : "age",
    		"fieldType" : "number"
    	}
    ],
    "rules" : [
    	{
    		"id" :  1,
    		"title" : "Age above threshold",
    		"description" : "Age > 18",
	    	"conditions" : [
	    		{
	    			"rule_id" : 1,
	    			"field_id" : 1,
	    			"field_name" : "age",
	    			"condition" : "$gt",
	    			"value" : "18"
	    		}
	    	],
	    	"decision" : "approve"
	    }
    ],
    "table_type" : "decision",
    "decision_type" : "string",
    "default_title" : "Default  Title",
    "default_description" : "Default Description",
    "default_decision" : "reject"
} 
```

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "id": "5ef8e3597e50148ba0e5f876",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "name": "Test Table 1",
        "description": " Dummy Description 1",
        "fields": [
            {
                "_id": "5ef8e3597e50148ba0e5f877",
                "id": 1,
                "title": "Age",
                "api_key": "age",
                "fieldType": "number"
            }
        ],
        "rules": [
            {
                "_id": "5ef8e3597e50148ba0e5f878",
                "id": 1,
                "title": "Age above threshold",
                "description": "Age > 18",
                "conditions": [
                    {
                        "_id": "5ef8e3597e50148ba0e5f879",
                        "rule_id": 1,
                        "field_id": 1,
                        "field_name": "age",
                        "condition": "$gt",
                        "value": "18"
                    }
                ],
                "decision": "approve"
            }
        ],
        "table_type": "decision",
        "decision_type": "string",
        "default_title": "Default  Title",
        "default_description": "Default Description",
        "default_decision": "reject",
        "created_at": "2020-06-28T18:37:13.000Z",
        "updated_at": "2020-06-28T18:37:13.000Z"
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "description",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Get Decision Table Details

**URL** : `api/v1/tables/5ef8e3597e50148ba0e5f876?user_id=5e6e2cf5ba6f9436cce68bb8&project_id=5e7776ea413d55a10477b61f`

**Method** : `GET`

**Path Param** : table_id

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
project_id:5e7776ea413d55a10477b61f

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "id": "5ef8e3597e50148ba0e5f876",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "name": "Test Table 1",
        "description": " Dummy Description 1",
        "fields": [
            {
                "_id": "5ef8e3597e50148ba0e5f877",
                "id": 1,
                "title": "Age",
                "api_key": "age",
                "fieldType": "number"
            }
        ],
        "rules": [
            {
                "_id": "5ef8e3597e50148ba0e5f878",
                "id": 1,
                "title": "Age above threshold",
                "description": "Age > 18",
                "conditions": [
                    {
                        "_id": "5ef8e3597e50148ba0e5f879",
                        "rule_id": 1,
                        "field_id": 1,
                        "field_name": "age",
                        "condition": "$gt",
                        "value": "18"
                    }
                ],
                "decision": "approve"
            }
        ],
        "table_type": "decision",
        "decision_type": "string",
        "default_title": "Default  Title",
        "default_description": "Default Description",
        "default_decision": "reject",
        "created_at": "2020-06-28T18:37:13.000Z",
        "updated_at": "2020-06-28T18:37:13.000Z"
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Execute Decision Table

**URL** : `api/v1/decisions`

**Method** : `POST`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{
    "table_id": "5e96b689b40f117a00ae533f",
    "months_in_biz": "40",
    "requested_amount": "700000",
    "unsecured_amount": "100000",
    "segmentation": 3,
    "biz_structure": "Company",
    "legal_age_owners_perc": "100",
    "equifax_pr_factor": "1",
    "product_requested" : ""
}
```

## Success Response

**Code** : `201`

**Response**

```json
{
    "status": "success",
    "code": 201,
    "data": {
        "id": "5ef8ea197e50148ba0e5f87c",
        "table_id": "5e96b689b40f117a00ae533f",
        "table_info": {
            "name": "Standard Program",
            "description": "Parameters that shall always be checked irrespective of the product requested by the customer",
            "table_type": "decision",
            "decision_type": "string",
            "default_decision": "Decline"
        },
        "rule_title": "Approve Conditions",
        "rule_description": "Approve Conditions",
        "final_decision": "Approve",
        "all_matching_decisions": [
            {
                "title": "Approve Conditions",
                "description": "Approve Conditions",
                "decision": "Approve"
            }
        ],
        "request": {
            "months_in_biz": "40",
            "requested_amount": "700000",
            "unsecured_amount": "100000",
            "segmentation": 3,
            "biz_structure": "Company",
            "legal_age_owners_perc": "100",
            "equifax_pr_factor": "1",
            "product_requested": ""
        },
        "created_at": 1593371161,
        "updated_at": 1593371161
    }
}
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "param": "product_requested",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## List Decisions (History)

**URL** : `api/v1/decisions?user_id=5e6e2cf5ba6f9436cce68bb8&project_id=5e7776ea413d55a10477b61f&table_id=5e96b689b40f117a00ae533f&page_no=1&per_page=4`

**Method** : `GET`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
project_id:5e7776ea413d55a10477b61f
table_id:5e96b689b40f117a00ae533f
page_no:1    (<optional><default = 1>)
per_page:2  (<optional><default = 15>)

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "decisions": [
            {
                "id": "5ef8ea197e50148ba0e5f87c",
                "table_id": "5e96b689b40f117a00ae533f",
                "user_id": "5e6e2cf5ba6f9436cce68bb8",
                "project_id": "5e7776ea413d55a10477b61f",
                "table_info": {
                    "name": "Standard Program",
                    "description": "Parameters that shall always be checked irrespective of the product requested by the customer",
                    "table_type": "decision",
                    "decision_type": "string",
                    "default_decision": "Decline"
                },
                "rule_title": "Approve Conditions",
                "rule_description": "Approve Conditions",
                "final_decision": "Approve",
                "created_at": "2020-06-28T19:06:01.000Z"
            },
            {
                "id": "5ef799f27e50148ba0e5f805",
                "table_id": "5e96b689b40f117a00ae533f",
                "user_id": "5e6e2cf5ba6f9436cce68bb8",
                "project_id": "5e7776ea413d55a10477b61f",
                "table_info": {
                    "name": "Standard Program",
                    "description": "Parameters that shall always be checked irrespective of the product requested by the customer",
                    "table_type": "decision",
                    "decision_type": "string",
                    "default_decision": "Decline"
                },
                "rule_title": "Approve Conditions",
                "rule_description": "Approve Conditions",
                "final_decision": "Approve",
                "created_at": "2020-06-27T19:11:46.000Z"
            }
        ],
        "count": 17
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Get Decision Details (Detailed History of a Decision)

**URL** : `api/v1/decisions/5ea00034f4948632199efa8f?user_id=5e6e2cf5ba6f9436cce68bb8&project_id=5e7776ea413d55a10477b61f&table_id=5e96b689b40f117a00ae533f`

**Method** : `GET`

**Path Param** : decision_id

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
project_id:5e7776ea413d55a10477b61f
table_id:5e96b689b40f117a00ae533f

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "id": "5ef8e3597e50148ba0e5f876",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "name": "Test Table 1",
        "description": " Dummy Description 1",
        "fields": [
            {
                "_id": "5ef8e3597e50148ba0e5f877",
                "id": 1,
                "title": "Age",
                "api_key": "age",
                "fieldType": "number"
            }
        ],
        "rules": [
            {
                "_id": "5ef8e3597e50148ba0e5f878",
                "id": 1,
                "title": "Age above threshold",
                "description": "Age > 18",
                "conditions": [
                    {
                        "_id": "5ef8e3597e50148ba0e5f879",
                        "rule_id": 1,
                        "field_id": 1,
                        "field_name": "age",
                        "condition": "$gt",
                        "value": "18"
                    }
                ],
                "decision": "approve"
            }
        ],
        "table_type": "decision",
        "decision_type": "string",
        "default_title": "Default  Title",
        "default_description": "Default Description",
        "default_decision": "reject",
        "created_at": "2020-06-28T18:37:13.000Z",
        "updated_at": "2020-06-28T18:37:13.000Z"
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```