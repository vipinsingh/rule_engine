## Save Expression

**URL** : `api/v1/rules/expressions`

**Method** : `POST`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{
    "user_id": "5e6e2cf5ba6f9436cce68bb8",
    "project_id": "5e7776ea413d55a10477b61f",
    "expression": [
        {
            "control_type": "attribute",
            "value": "requested_amount",
            "displayValue": "requested amount",
            "id": 1,
            "attribute_type": "number",
            "is_selected": true,
            "exp_name": "E1"
        },
        {
            "control_type": "mathOperators",
            "value": "$lt",
            "displayValue": "<",
            "id": 2,
            "is_selected": true,
            "exp_name": "E1"
        },
        {
            "control_type": "constant",
            "value": "40000",
            "displayValue": "40000",
            "id": 3,
            "is_selected": true,
            "exp_name": "E1"
        }
    ]
}
```

## Success Response

**Code** : `201`

**Response**

```json
{
    "status": "success",
    "code": 201,
    "data": {
        "id": "5ef8e3597e50148ba0e5f876",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "expression": [
            {
                "control_type": "attribute",
                "value": "requested_amount",
                "displayValue": "requested amount",
                "id": 1,
                "attribute_type": "number",
                "is_selected": true,
                "exp_name": "E1"
            },
            {
                "control_type": "mathOperators",
                "value": "$lt",
                "displayValue": "<",
                "id": 2,
                "is_selected": true,
                "exp_name": "E1"
            },
            {
                "control_type": "constant",
                "value": "40000",
                "displayValue": "40000",
                "id": 3,
                "is_selected": true,
                "exp_name": "E1"
            }
        ],
        "created_at": "2020-06-28T18:37:13.000Z",
        "updated_at": "2020-06-28T18:37:13.000Z"
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "project_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Get Expression Suggestions By Display Value

**URL** : `api/v1/rules/expressions?user_id=5e6e2cf5ba6f9436cce68bb8&project_id=5e7776ea413d55a10477b61f&value=ris`

**Method** : `GET`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
project_id:5e7776ea413d55a10477b61f
value:ris

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": [
        {
            "id": "5eb523f8ddd92ef358b78831",
            "user_id": "5e6e2cf5ba6f9436cce68bb8",
            "project_id": "5e7776ea413d55a10477b61f",
            "expression": [
                {
                    "_id": "5eb523f8ddd92ef358b78832",
                    "control_type": "attribute",
                    "value": "risk_score",
                    "displayValue": "risk score",
                    "id": 1,
                    "attribute_type": "number"
                },
                {
                    "_id": "5eb523f8ddd92ef358b78833",
                    "control_type": "mathOperators",
                    "value": "$lt",
                    "displayValue": "<",
                    "id": 2
                },
                {
                    "_id": "5eb523f8ddd92ef358b78834",
                    "control_type": "constant",
                    "value": "4.5",
                    "displayValue": "4.5",
                    "id": 3
                }
            ],
            "displayValue": "risk score < 4.5",
            "created_at": "2020-05-08T09:18:48.000Z",
            "updated_at": "2020-05-08T09:18:48.000Z"
        },
        {
            "id": "5eb7d10427094ddaf4d2ab5f",
            "user_id": "5e6e2cf5ba6f9436cce68bb8",
            "project_id": "5e7776ea413d55a10477b61f",
            "expression": [
                {
                    "_id": "5eb7d10427094ddaf4d2ab60",
                    "control_type": "attribute",
                    "value": "requested_amount",
                    "displayValue": "requested amount",
                    "id": 1,
                    "attribute_type": "number"
                },
                {
                    "_id": "5eb7d10427094ddaf4d2ab61",
                    "control_type": "mathOperators",
                    "value": "$lt",
                    "displayValue": "<",
                    "id": 2
                },
                {
                    "_id": "5eb7d10427094ddaf4d2ab62",
                    "control_type": "constant",
                    "value": "40000",
                    "displayValue": "40000",
                    "id": 3
                }
            ],
            "displayValue": "requested amount < 40000 AND risk score < 4.5",
            "created_at": "2020-05-10T10:01:40.000Z",
            "updated_at": "2020-05-10T10:01:40.000Z"
        }
    ]
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Save Rule

**URL** : `api/v1/rules`

**Method** : `POST`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{
    "user_id": "5e6e2cf5ba6f9436cce68bb8",
    "project_id": "5e7776ea413d55a10477b61f",
    "name": "Test Rule 3",
    "description": "Test Description 3",
    "rule_expression": [
        {
            "control_type": "attribute",
            "value": "months_in_business",
            "displayValue": "months in business",
            "id": 1,
            "attribute_type": "number"
        },
        {
            "control_type": "mathOperators",
            "value": "$gte",
            "displayValue": ">=",
            "id": 2
        },
        {
            "control_type": "constant",
            "value": "30",
            "displayValue": "30",
            "id": 3
        }
    ],
    "decision_type": "compute",
    "decision": [
        {
            "control_type": "attribute",
            "value": "revenue",
            "displayValue": "revenue",
            "id": 1,
            "attribute_type": "number"
        },
        {
            "control_type": "mathOperators",
            "value": "$div",
            "displayValue": "/",
            "id": 2
        },
        {
            "control_type": "attribute",
            "value": "risk_score",
            "displayValue": "risk score",
            "id": 3,
            "attribute_type": "number"
        }
    ],
    "specify_default_decision": "yes",
    "default_decision_type": "string",
    "default_decision": "Test default decision 3"
}
```

## Success Response

**Code** : `201`

**Response**

```json
{
    "status": "success",
    "code": 201,
    "data": {
        "id": "5ef8e3597e50148ba0e5f876",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "name": "Test Rule 3",
        "description": "Test Description 3",
        "rule_expression": [
            {
                "control_type": "attribute",
                "value": "months_in_business",
                "displayValue": "months in business",
                "id": 1,
                "attribute_type": "number"
            },
            {
                "control_type": "mathOperators",
                "value": "$gte",
                "displayValue": ">=",
                "id": 2
            },
            {
                "control_type": "constant",
                "value": "30",
                "displayValue": "30",
                "id": 3
            }
        ],
        "decision_type": "compute",
        "decision": [
            {
                "control_type": "attribute",
                "value": "revenue",
                "displayValue": "revenue",
                "id": 1,
                "attribute_type": "number"
            },
            {
                "control_type": "mathOperators",
                "value": "$div",
                "displayValue": "/",
                "id": 2
            },
            {
                "control_type": "attribute",
                "value": "risk_score",
                "displayValue": "risk score",
                "id": 3,
                "attribute_type": "number"
            }
        ],
        "specify_default_decision": "yes",
        "default_decision_type": "string",
        "default_decision": "Test default decision 3",
        "created_at": "2020-06-28T18:37:13.000Z",
        "updated_at": "2020-06-28T18:37:13.000Z"
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "project_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## List Rules

**URL** : `api/v1/rules?user_id=5e6e2cf5ba6f9436cce68bb8&project_id=5e7776ea413d55a10477b61f&page_no=1&per_page=2`

**Method** : `GET`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
project_id:5e7776ea413d55a10477b61f
page_no:1    (<optional><default = 1>)
per_page:2  (<optional><default = 15>)

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "rules": [
            {
                "id": "5eb7daeebca070fff81883f5",
                "name": "Test Rule 1",
                "description": "Detailed test description 1 for testing update",
                "created_at": "2020-05-10T10:43:58.000Z",
                "updated_at": "2020-05-12T10:17:59.000Z"
            },
            {
                "id": "5eb90d44a860c5266948f511",
                "name": "Test Rule 2",
                "description": "Test Description 2",
                "created_at": "2020-05-11T08:31:00.000Z",
                "updated_at": "2020-05-11T08:31:00.000Z"
            }
        ],
        "count": 4
    }
}
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Get Rule Details

**URL** : `api/v1/rules/5eb7daeebca070fff81883f5?user_id=5e6e2cf5ba6f9436cce68bb8&project_id=5e7776ea413d55a10477b61f`

**Method** : `GET`

**Path Param** : rule_id

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
project_id:5e7776ea413d55a10477b61f

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "id": "5eb7daeebca070fff81883f5",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "name": "Test Rule 1",
        "description": "Detailed test description 1 for testing update",
        "rule_expression": [
            {
                "_id": "5eb8500e26ab00f8f4ddc939",
                "control_type": "attribute",
                "value": "requested_amount",
                "displayValue": "requested amount",
                "id": 1,
                "attribute_type": "number"
            },
            {
                "_id": "5eb8500e26ab00f8f4ddc93a",
                "control_type": "mathOperators",
                "value": "$between",
                "displayValue": "between",
                "id": 2
            },
            {
                "_id": "5eb8500e26ab00f8f4ddc93b",
                "control_type": "constant",
                "value": "25000,60000",
                "displayValue": "25000,60000",
                "id": 3
            }
        ],
        "decision_type": "string",
        "decision": "Test Decision 1",
        "specify_default_decision": "yes",
        "default_decision_type": "string",
        "default_decision": "Test Default Decision 1",
        "created_at": "2020-05-10T10:43:58.000Z",
        "updated_at": "2020-05-12T10:17:59.000Z"
    }
}
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Update Rule

**URL** : `api/v1/rules/5e87734d6be2080b95f2f567`

**Method** : `PUT`

**Path Param** : rule_id

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{
    "user_id": "5e6e2cf5ba6f9436cce68bb8",
    "project_id": "5e7776ea413d55a10477b61f",
    "name" : "Test Rule 1",
    "description" : "Test Description 1",
    "rule_expression": [
        {
            "control_type": "attribute",
            "value": "requested_amount",
            "displayValue": "requested amount",
            "id": 1,
            "attribute_type": "number",
            "is_selected": true,
            "exp_name": "E1"
        },
        {
            "control_type": "mathOperators",
            "value": "$between",
            "displayValue": "between",
            "id": 2,
            "is_selected": true,
            "exp_name": "E1"
        },
        {
            "control_type": "constant",
            "value": "25000,60000",
            "displayValue": "25000,60000",
            "id": 3,
            "is_selected": true,
            "exp_name": "E1"
        }
    ],
    "decision_type" : "string",
    "decision" : "Test Decision 1",
    "specify_default_decision" : "yes",
    "default_decision_type" : "string",
    "default_decision" : "Test Default Decision 1"
}
```

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "id": "5ef8e3597e50148ba0e5f876",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "name" : "Test Rule 1",
        "description" : "Test Description 1",
        "rule_expression": [
            {
                "control_type": "attribute",
                "value": "requested_amount",
                "displayValue": "requested amount",
                "id": 1,
                "attribute_type": "number",
                "is_selected": true,
                "exp_name": "E1"
            },
            {
                "control_type": "mathOperators",
                "value": "$between",
                "displayValue": "between",
                "id": 2,
                "is_selected": true,
                "exp_name": "E1"
            },
            {
                "control_type": "constant",
                "value": "25000,60000",
                "displayValue": "25000,60000",
                "id": 3,
                "is_selected": true,
                "exp_name": "E1"
            }
        ],
        "decision_type" : "string",
        "decision" : "Test Decision 1",
        "specify_default_decision" : "yes",
        "default_decision_type" : "string",
        "default_decision" : "Test Default Decision 1",
        "created_at": "2020-06-28T18:37:13.000Z",
        "updated_at": "2020-06-28T18:37:13.000Z"
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "description",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Execute Rule

**URL** : `api/v1/rules/5ee8ba765559ac629c70e8a8/decision`

**Method** : `POST`

**Path Param** : rule_id

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{
    "months_in_business": "30",
    "age": "30",
    "revenue": "200000",
    "risk_score": "2"
}
```

## Success Response

**Code** : `201`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "id": "5ef8f3c77e50148ba0e5f884",
        "rule_id": "5ee8ba765559ac629c70e8a8",
        "rule_name": "Profit Indicator",
        "rule_description": "Tells about profit",
        "final_decision": "236.00",
        "request": {
            "months_in_business": "30",
            "age": "30",
            "revenue": "200000",
            "risk_score": "2"
        },
        "created_at": 1593373639,
        "updated_at": 1593373639
    }
}
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "param": "revenue",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Get Rule Suggestions from Rule Name

**URL** : `api/v1/rules/rule_suggestions?user_id=5e6e2cf5ba6f9436cce68bb8&project_id=5e7776ea413d55a10477b61f&rule_name=te`

**Method** : `GET`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
project_id:5e7776ea413d55a10477b61f
rule_name:te

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": [
        {
            "id": "5eb7daeebca070fff81883f5",
            "name": "Test Rule 1",
            "description": "Detailed test description 1 for testing update",
            "decision_type": "string",
            "default_decision_type": "string"
        },
        {
            "id": "5eb90d44a860c5266948f511",
            "name": "Test Rule 2",
            "description": "Test Description 2",
            "decision_type": "string",
            "default_decision_type": ""
        },
        {
            "id": "5eb9960d71c77a3779111998",
            "name": "Test Rule 3",
            "description": "Test Description 3",
            "decision_type": "compute",
            "default_decision_type": "string"
        }
    ]
}
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```