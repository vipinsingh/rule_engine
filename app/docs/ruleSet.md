## Save Rule Set

**URL** : `api/v1/rule_set`

**Method** : `POST`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{
    "user_id": "5e6e2cf5ba6f9436cce68bb8",
    "project_id": "5e7776ea413d55a10477b61f",
    "name": "Rule Set 1",
    "description": "Test Description 1",
    "rule_set_exp": [
        {
            "id": 1,
            "operator": "$if",
            "displayValue": "IF",
            "rule_expression": [
                {
                    "id": 1,
                    "control_type": "rule",
                    "value": "5eb7daeebca070fff81883f5",
                    "displayValue": "Test Rule 1",
                    "rule_data": {
                        "description": "Detailed test description 1 for testing update",
                        "decision_type": "string",
                        "default_decision_type": "string"
                    }
                },
                {
                    "id": 2,
                    "control_type": "mathOperators",
                    "value": "$eq",
                    "displayValue": "="
                },
                {
                    "id": 3,
                    "control_type": "constant",
                    "value": "pass",
                    "displayValue": "pass"
                },
                {
                    "id": 4,
                    "control_type": "operator",
                    "value": "$and",
                    "displayValue": "AND"
                },
                {
                    "id": 5,
                    "control_type": "rule",
                    "value": "5eb90d44a860c5266948f511",
                    "displayValue": "Test Rule 2",
                    "rule_data": {
                        "description": "Test Description 2",
                        "decision_type": "string",
                        "default_decision_type": ""
                    }
                },
                {
                    "id": 6,
                    "control_type": "mathOperators",
                    "value": "$eq",
                    "displayValue": "="
                },
                {
                    "id": 7,
                    "control_type": "constant",
                    "value": "approve",
                    "displayValue": "approve"
                }
            ],
            "then_decision": "Approve",
            "decision_type": "string",
            "decision_exp": []
        },
        {
            "id": 2,
            "operator": "$else",
            "displayValue": "ELSE",
            "then_decision": "Reject",
            "decision_type": "string",
            "decision_exp": []
        }
    ]
}
```

## Success Response

**Code** : `201`

**Response**

```json
{
    "status": "success",
    "code": 201,
    "data": {
        "id": "5ef8e3597e50148ba0e5f876",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "name": "Rule Set 1",
        "description": "Test Description 1",
        "rule_set_exp": [
            {
                "id": 1,
                "operator": "$if",
                "displayValue": "IF",
                "rule_expression": [
                    {
                        "id": 1,
                        "control_type": "rule",
                        "value": "5eb7daeebca070fff81883f5",
                        "displayValue": "Test Rule 1",
                        "rule_data": {
                            "description": "Detailed test description 1 for testing update",
                            "decision_type": "string",
                            "default_decision_type": "string"
                        }
                    },
                    {
                        "id": 2,
                        "control_type": "mathOperators",
                        "value": "$eq",
                        "displayValue": "="
                    },
                    {
                        "id": 3,
                        "control_type": "constant",
                        "value": "pass",
                        "displayValue": "pass"
                    },
                    {
                        "id": 4,
                        "control_type": "operator",
                        "value": "$and",
                        "displayValue": "AND"
                    },
                    {
                        "id": 5,
                        "control_type": "rule",
                        "value": "5eb90d44a860c5266948f511",
                        "displayValue": "Test Rule 2",
                        "rule_data": {
                            "description": "Test Description 2",
                            "decision_type": "string",
                            "default_decision_type": ""
                        }
                    },
                    {
                        "id": 6,
                        "control_type": "mathOperators",
                        "value": "$eq",
                        "displayValue": "="
                    },
                    {
                        "id": 7,
                        "control_type": "constant",
                        "value": "approve",
                        "displayValue": "approve"
                    }
                ],
                "then_decision": "Approve",
                "decision_type": "string",
                "decision_exp": []
            },
            {
                "id": 2,
                "operator": "$else",
                "displayValue": "ELSE",
                "then_decision": "Reject",
                "decision_type": "string",
                "decision_exp": []
            }
        ],
        "created_at": "2020-06-28T18:37:13.000Z",
        "updated_at": "2020-06-28T18:37:13.000Z"
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "project_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## List Rule Sets

**URL** : `api/v1/rule_set?user_id=5e6e2cf5ba6f9436cce68bb8&project_id=5e7776ea413d55a10477b61f&page_no=1&per_page=2`

**Method** : `GET`

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
project_id:5e7776ea413d55a10477b61f
page_no:1    (<optional><default = 1>)
per_page:2  (<optional><default = 15>)

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "rule_set": [
            {
                "id": "5ef65a0d7e50148ba0e5f74c",
                "name": "Rule Set 1",
                "description": "Test Description 1",
                "created_at": "2020-06-26T20:26:53.000Z",
                "updated_at": "2020-06-27T10:36:23.000Z"
            }
        ],
        "count": 1
    }
}
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Get Rule Set Details

**URL** : `api/v1/rule_set/5ef65a0d7e50148ba0e5f74c?user_id=5e6e2cf5ba6f9436cce68bb8&project_id=5e7776ea413d55a10477b61f`

**Method** : `GET`

**Path Param** : rule_set_id

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Query Params

user_id:5e6e2cf5ba6f9436cce68bb8
project_id:5e7776ea413d55a10477b61f

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "id": "5ef65a0d7e50148ba0e5f74c",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "name": "Rule Set 1",
        "description": "Test Description 1",
        "rule_set_exp": [
            {
                "rule_expression": [
                    {
                        "_id": "5ef721277e50148ba0e5f7af",
                        "id": 1,
                        "control_type": "rule",
                        "value": "5eb7daeebca070fff81883f5",
                        "displayValue": "Test Rule 1",
                        "rule_data": {
                            "description": "Detailed test description 1 for testing update",
                            "decision_type": "string",
                            "default_decision_type": "string"
                        }
                    },
                    {
                        "_id": "5ef721277e50148ba0e5f7b0",
                        "id": 2,
                        "control_type": "mathOperators",
                        "value": "$eq",
                        "displayValue": "="
                    },
                    {
                        "_id": "5ef721277e50148ba0e5f7b1",
                        "id": 3,
                        "control_type": "constant",
                        "value": "pass",
                        "displayValue": "pass"
                    },
                    {
                        "_id": "5ef721277e50148ba0e5f7b2",
                        "id": 4,
                        "control_type": "operator",
                        "value": "$and",
                        "displayValue": "AND"
                    },
                    {
                        "_id": "5ef721277e50148ba0e5f7b3",
                        "id": 5,
                        "control_type": "rule",
                        "value": "5eb90d44a860c5266948f511",
                        "displayValue": "Test Rule 2",
                        "rule_data": {
                            "description": "Test Description 2",
                            "decision_type": "string",
                            "default_decision_type": ""
                        }
                    },
                    {
                        "_id": "5ef721277e50148ba0e5f7b4",
                        "id": 6,
                        "control_type": "mathOperators",
                        "value": "$eq",
                        "displayValue": "="
                    },
                    {
                        "_id": "5ef721277e50148ba0e5f7b5",
                        "id": 7,
                        "control_type": "constant",
                        "value": "approve",
                        "displayValue": "approve"
                    }
                ],
                "decision_exp": [],
                "_id": "5ef721277e50148ba0e5f7ae",
                "id": 1,
                "operator": "$if",
                "displayValue": "IF",
                "then_decision": "Approve",
                "decision_type": "string"
            },
            {
                "rule_expression": [],
                "decision_exp": [],
                "_id": "5ef721277e50148ba0e5f7b6",
                "id": 2,
                "operator": "$else",
                "displayValue": "ELSE",
                "then_decision": "Reject",
                "decision_type": "string"
            }
        ],
        "created_at": "2020-06-26T20:26:53.000Z",
        "updated_at": "2020-06-27T10:36:23.000Z"
    }
}
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "user_id",
            "message": "This action requires the field to be specified."
        }
    ]
}
```

## Update Rule Set

**URL** : `api/v1/rule_set/5e87734d6be2080b95f2f567`

**Method** : `PUT`

**Path Param** : rule_set_id

**Header** : None 

**Auth required** : None

**Permissions required** : None

## Request Body

```json
{
    "user_id": "5e6e2cf5ba6f9436cce68bb8",
    "project_id": "5e7776ea413d55a10477b61f",
    "name": "Rule Set 1",
    "description": "Test Description 1",
    "rule_set_exp": [
        {
            "id": 1,
            "operator": "$if",
            "displayValue": "IF",
            "rule_expression": [
                {
                    "id": 1,
                    "control_type": "rule",
                    "value": "5eb7daeebca070fff81883f5",
                    "displayValue": "Test Rule 1",
                    "rule_data": {
                        "description": "Detailed test description 1 for testing update",
                        "decision_type": "string",
                        "default_decision_type": "string"
                    }
                },
                {
                    "id": 2,
                    "control_type": "mathOperators",
                    "value": "$eq",
                    "displayValue": "="
                },
                {
                    "id": 3,
                    "control_type": "constant",
                    "value": "pass",
                    "displayValue": "pass"
                },
                {
                    "id": 4,
                    "control_type": "operator",
                    "value": "$and",
                    "displayValue": "AND"
                },
                {
                    "id": 5,
                    "control_type": "rule",
                    "value": "5eb90d44a860c5266948f511",
                    "displayValue": "Test Rule 2",
                    "rule_data": {
                        "description": "Test Description 2",
                        "decision_type": "string",
                        "default_decision_type": ""
                    }
                },
                {
                    "id": 6,
                    "control_type": "mathOperators",
                    "value": "$eq",
                    "displayValue": "="
                },
                {
                    "id": 7,
                    "control_type": "constant",
                    "value": "approve",
                    "displayValue": "approve"
                }
            ],
            "then_decision": "Approve",
            "decision_type": "string",
            "decision_exp": []
        },
        {
            "id": 2,
            "operator": "$else",
            "displayValue": "ELSE",
            "then_decision": "Reject",
            "decision_type": "string",
            "decision_exp": []
        }
    ]
}
```

## Success Response

**Code** : `200`

**Response**

```json
{
    "status": "success",
    "code": 200,
    "data": {
        "id": "5ef8e3597e50148ba0e5f876",
        "user_id": "5e6e2cf5ba6f9436cce68bb8",
        "project_id": "5e7776ea413d55a10477b61f",
        "name": "Rule Set 1",
        "description": "Test Description 1",
        "rule_set_exp": [
            {
                "id": 1,
                "operator": "$if",
                "displayValue": "IF",
                "rule_expression": [
                    {
                        "id": 1,
                        "control_type": "rule",
                        "value": "5eb7daeebca070fff81883f5",
                        "displayValue": "Test Rule 1",
                        "rule_data": {
                            "description": "Detailed test description 1 for testing update",
                            "decision_type": "string",
                            "default_decision_type": "string"
                        }
                    },
                    {
                        "id": 2,
                        "control_type": "mathOperators",
                        "value": "$eq",
                        "displayValue": "="
                    },
                    {
                        "id": 3,
                        "control_type": "constant",
                        "value": "pass",
                        "displayValue": "pass"
                    },
                    {
                        "id": 4,
                        "control_type": "operator",
                        "value": "$and",
                        "displayValue": "AND"
                    },
                    {
                        "id": 5,
                        "control_type": "rule",
                        "value": "5eb90d44a860c5266948f511",
                        "displayValue": "Test Rule 2",
                        "rule_data": {
                            "description": "Test Description 2",
                            "decision_type": "string",
                            "default_decision_type": ""
                        }
                    },
                    {
                        "id": 6,
                        "control_type": "mathOperators",
                        "value": "$eq",
                        "displayValue": "="
                    },
                    {
                        "id": 7,
                        "control_type": "constant",
                        "value": "approve",
                        "displayValue": "approve"
                    }
                ],
                "then_decision": "Approve",
                "decision_type": "string",
                "decision_exp": []
            },
            {
                "id": 2,
                "operator": "$else",
                "displayValue": "ELSE",
                "then_decision": "Reject",
                "decision_type": "string",
                "decision_exp": []
            }
        ],
        "created_at": "2020-06-28T18:37:13.000Z",
        "updated_at": "2020-06-28T18:37:13.000Z"
    }
} 
```

## Error Response 

**Code** : `400`

**Response**

```json
{
    "status": "failed",
    "code": 400,
    "errors": [
        {
            "type": "FIELD_REQUIRED",
            "parameter": "description",
            "message": "This action requires the field to be specified."
        }
    ]
}
```