export const ERRORS = {
    required : {
        type : 'FIELD_REQUIRED',
        message: 'This action requires the field to be specified.'
    },
    invalid : {
        type : 'FIELD_INVALID',
        message: 'The value of the field is invalid.'
    },
    invalid_string :  {
        type : 'FIELD_INVALID',
        message: 'The value of the field must be a valid string.'
    },
    invalid_number :  {
        type : 'FIELD_INVALID',
        message: 'The value of the field must be a valid number.'
    },
    invalid_boolean :  {
        type : 'FIELD_INVALID',
        message: 'The value of the field must be boolean.'
    },
    duplicate : {
        type : 'FIELD_DUPLICATE',
        message : 'The value of the field is already used for another resource.'
    },
    action_failed : {
        type : 'ACTION_FAILED',
        message :  'The server failed to perform this action for unknown internal reason.'
    },
    authentication_failed : {
        type   : 'AUTHENTICATION_FAILED',
        message: 'Used authentication credentials are invalid.'
    },
    resource_not_found : {
        type   : 'RESOURCE_NOT_FOUND',
        message: 'Resource does not exist or has been removed.'
    },
    invalid_sel_expression : {
        type : 'FIELD_INVALID',
        parameter : 'expression',
        message: 'The value of the field is invalid.'
    },
    invalid_rule_expression : {
        type : 'FIELD_INVALID',
        parameter : 'rule_expression',
        message: 'The value of the field is invalid.'
    },
    invalid_decision_expression : {
        type : 'FIELD_INVALID',
        parameter : 'decision',
        message: 'The value of the field is invalid.'
    },
    invalid_def_dec_expression : {
        type : 'FIELD_INVALID',
        parameter : 'default_decision',
        message: 'The value of the field is invalid.'
    },
    invalid_rule_set : {
        type : 'FIELD_INVALID',
        parameter : 'rule_set_exp',
        message: 'The value of the field is invalid.'
    },
};