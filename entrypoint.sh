#! /bin/bash
cd /usr/src/app/
sed -i "s/db_host/${db_host}/g" /usr/src/app/.env
sed -i "s/db_user/${db_user}/g" /usr/src/app/.env
sed -i "s/db_password/${db_password}/g" /usr/src/app/.env
sed -i "s/db_port/${db_port}/g" /usr/src/app/.env
sed -i "s/db_provider/${db_provider}/g" /usr/src/app/.env
sed -i "s/database/${database}/g" /usr/src/app/.env
sed -i "s/app_env/${app_env}/g" /usr/src/app/.env
sed -i "s/service_port/${service_port}/g" /usr/src/app/.env

pm2 start processes.json --no-daemon