import React, { Component } from "react";
import classes from './RulesList.module.css';
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import Table from '../../../../components/Table/Table';
import * as actions from '../../../../store/actions/index';
import Spinner from "../../../../components/UI/Spinner/Spinner";
import Pagination from "../../../../components/Pagination/Pagination";
import { toast } from "react-toastify";

class RulesList extends Component {

    state = {
        perPage : 15,
        pageNo : 1,
        tillPage : 5,
        maxPageItems : 5
    }

    handlePageSelect = (number) => {
        this.setState({ pageNo : number });
        
        this.props.onListRules(this.getListRulesParams(number));
    }

    getListRulesParams = (pageNo = null) => {
        return {
            user_id : this.props.user_id,
            project_id : this.props.match.params.projectId,
            page_no : pageNo ? pageNo : this.state.pageNo,
            per_page : this.state.perPage
        }
    }

    handlePrevNext = (action) => {
        let tillPage = null;
        if(action === 'prev') {
            tillPage = this.state.tillPage - this.state.maxPageItems;
        } else if(action === 'next') {
            tillPage = this.state.tillPage + this.state.maxPageItems;
        }

        this.setState({ tillPage : tillPage })
    }

    componentDidUpdate(prevProps) {
        if(this.props.error) {
            toast.error('An error occured!');
            this.props.onErrorReset();
        }

        if(this.props.match.params.projectId !== prevProps.match.params.projectId) {
            this.props.onListRules(this.getListRulesParams());
        }
    }

    componentDidMount() {
        this.props.onListRules(this.getListRulesParams());
    }

    createRule = () => {
        this.props.history.push(`${this.props.match.url}/create`);
    }

    viewRule = (id) => {
        this.props.history.push(`${this.props.match.url}/${id}`);
    }

    render() {
        const spinner = this.props.loading ? <Spinner /> : null;

        return (
            <div className={classes.RulesList}>
                {spinner}
                <h2>Rules</h2>
                <Button variant="primary" className={classes.buttonPadding} onClick={this.createRule}>Create Rule</Button>
                <Table
                    tableView='rule'
                    tableData={this.props.ruleList ? this.props.ruleList.rules : null}
                    createClicked={this.createRule}
                    rowClicked={this.viewRule}
                    error={this.props.error}
                />
                <Pagination 
                    recordCount={this.props.ruleList && this.props.ruleList.count ? this.props.ruleList.count : 0}
                    perPage={this.state.perPage}
                    pageNo={this.state.pageNo}
                    tillPage={this.state.tillPage}
                    maxPageItems={this.state.maxPageItems}
                    pageSelectHandler={this.handlePageSelect}
                    prevNextHandler={this.handlePrevNext}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ruleList : state.decRule.ruleList,
        loading : state.decRule.loading,
        error : state.decRule.error,
        user_id : state.auth.userData.user_id
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onErrorReset : () => dispatch(actions.resetRuleError()),
        onListRules : (params) => dispatch(actions.listRules(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RulesList);

