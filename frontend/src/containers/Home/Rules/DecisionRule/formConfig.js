export const createRuleForm = {
    name :  {
        elementConfig : {
            type : 'text',
            name : 'name',
            placeholder : 'Rule Name',
            className : 'RuleNameInput',
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 200
        },
        valid : false,
        touched : false
    },
    description :  {
        elementConfig : {
            type : 'textarea',
            rows : 3,
            name : 'description',
            className : 'RuleTextArea',
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 500
        },
        valid : false,
        touched : false
    },
    decision_type :  {
        elementConfig : {
            type : 'select',
            name : 'decision_type',
            className : 'DecisionType',
            options : [
                {
                    value : '',
                    displayValue  :  'Decision Type'
                },
                {
                    value : 'string',
                    displayValue  :  'String'
                },
                {
                    value : 'number',
                    displayValue  :  'Number'
                },
                {
                    value : 'alpha_num',
                    displayValue  :  'AlphaNumeric'
                },
                {
                    value : 'json',
                    displayValue  :  'JSON'
                },
                {
                    value : 'compute',
                    displayValue  :  'Computational'
                },
                {
                    value : 'rule_result',
                    displayValue  :  'Rule Result'
                }
            ]
        },
        value : '',
        validation : {
            required : true
        },
        valid : false,
        touched : false
    },
    decision :  {
        elementConfig : {
            type : 'text',
            name : 'decision',
            placeholder : 'Decision',
            className : 'DecisionInput',
        },
        value : '',
        validation : {
            required : true,
            minLength : 1
        },
        valid : false,
        touched : false
    },
    specify_default_decision :  {
        elementConfig : {
            type : 'checkbox',
            name : 'specify_default_decision',
            className : 'RuleCheckBox',
            inline :  true,
            options : [
                {
                    value : 'yes',
                    displayValue  :  'Yes'
                },
                {
                    value : 'no',
                    displayValue  :  'No'
                }
            ]
        },
        value : '',
        validation : {
            required : true
        },
        valid : false,
        touched : false
    },
    default_decision_type :  {
        elementConfig : {
            type : 'select',
            name : 'default_decision_type',
            className : 'DecisionType',
            options : [
                {
                    value : '',
                    displayValue  :  'Decision Type'
                },
                {
                    value : 'string',
                    displayValue  :  'String'
                },
                {
                    value : 'number',
                    displayValue  :  'Number'
                },
                {
                    value : 'alpha_num',
                    displayValue  :  'AlphaNumeric'
                },
                {
                    value : 'json',
                    displayValue  :  'JSON'
                },
                {
                    value : 'compute',
                    displayValue  :  'Computational'
                }
            ]
        },
        value : '',
        validation : false,
        valid : true,
        touched : false
    },
    default_decision :  {
        elementConfig : {
            type : 'text',
            name : 'default_decision',
            placeholder : 'Default Decision',
            className : 'DecisionInput',
        },
        value : '',
        validation : false,
        valid : true,
        touched : false
    }
};

export const RULE_ATTRIBUTES = [
    {
        id: 1,
        displayValue : 'months in business',
        value : 'months_in_business',
        fieldType : 'number'
    },
    {
        id : 2,
        displayValue : 'revenue',
        value : 'revenue',
        fieldType : 'number'
    },
    {
        id : 3,
        displayValue : 'business structure',
        value : 'business_structure',
        fieldType : 'string'
    },
    {
        id : 4,
        displayValue : 'requested amount',
        value : 'requested_amount',
        fieldType : 'number'
    },
    {
        id : 5,
        displayValue : 'age',
        value : 'age',
        fieldType : 'number'
    },
    {
        id : 6,
        displayValue : 'country',
        value : 'country',
        fieldType : 'string'
    },
    {
        id : 7,
        displayValue : 'risk score',
        value : 'risk_score',
        fieldType : 'number'
    },
    {
        id : 8,
        displayValue : 'fraud factor',
        value : 'fraud_factor',
        fieldType : 'boolean'
    },
    {
        id : 9,
        displayValue : 'industry',
        value : 'industry',
        fieldType : 'string'
    },
    {
        id : 10,
        displayValue : 'average monthly revenue',
        value : 'avg_mon_revenue',
        fieldType : 'number'
    },
    {
        id : 11,
        displayValue : 'criminal records',
        value : 'criminal_record',
        fieldType : 'number'
    },
    {
        id : 12,
        displayValue : 'OFAC Record',
        value : 'ofac_record',
        fieldType : 'string'
    },
    {
        id : 13,
        displayValue : 'collection',
        value : 'collection',
        fieldType : 'number'
    },
    {
        id : 14,
        displayValue : 'negative trade account',
        value : 'neg_trade_account',
        fieldType : 'number'
    },
    {
        id : 15,
        displayValue : 'business location',
        value : 'biz_location',
        fieldType : 'string'
    }
];

export const MATH_OPERATORS = {
    'number' : {
        '+'       : '$add',
        '-'       : '$sub',
        '*'       : '$mul',
        '/'       : '$div',
        '^'       : '$exp',
        '='       : '$eq',
        '!='      : '$not_eq',
        'in'      : '$in',
        'not in'  : '$not_in',
        'is set'  : '$is_set',
        'is null' : '$is_null',
        '>'       : '$gt',
        '>='      : '$gte',
        '<'       : '$lt',
        '<='      : '$lte',
        'between' : '$between'
    },
    'string' : {
        '='       : '$eq',
        '!='      : '$not_eq',
        'in'      : '$in',
        'not in'  : '$not_in',
        'is set'  : '$is_set',
        'is null' : '$is_null',
    },
    'boolean' : {
        'is set'  : '$is_set',
        'is null' : '$is_null',
        '= true'  : '$true',
        '= false' : '$false',
    },
    'all' : {
        '+'       : '$add',
        '-'       : '$sub',
        '*'       : '$mul',
        '/'       : '$div',
        '^'       : '$exp',
        '='       : '$eq',
        '!='      : '$not_eq',
        'in'      : '$in',
        'not in'  : '$not_in',
        'is set'  : '$is_set',
        'is null' : '$is_null',
        '>'       : '$gt',
        '>='      : '$gte',
        '<'       : '$lt',
        '<='      : '$lte',
        'between' : '$between',
        '= true'  : '$true',
        '= false' : '$false',
    }
};

export const COMP_OPS = {
    '+' : '$add',
    '-' : '$sub',
    '*' : '$mul',
    '/' : '$div',
    '^' : '$exp',
    '(' : '$open_bracket',
    ')' : '$close_bracket'
}

export const EXP_OPTIONS = [
    {
        id : 1,
        displayValue : 'months in business > 24',
        value : [
            {
                control_type: "attribute",
                value: "months_in_business",
                displayValue: "months in business",
                id: 1,
                attribute_type: "number"
            },
            {
                control_type: "mathOperators",
                value: "$gt",
                displayValue: ">",
                id: 2
            },
            {
                control_type: "constant",
                value: "24",
                displayValue: "24",
                id: 3
            }
        ],
    },
    {
        id : 2,
        displayValue : 'revenue >= 200000',
        value : [
            {
                control_type: "attribute",
                value: "revenue",
                displayValue: "revenue",
                id: 1,
                attribute_type: "number"
            },
            {
                control_type: "mathOperators",
                value: "$gte",
                displayValue: ">=",
                id: 2
            },
            {
                control_type: "constant",
                value: "200000",
                displayValue: "200000",
                id: 3
            }
        ],
    },
    {
        id : 3,
        displayValue : 'country = US',
        value : [
            {
                control_type: "attribute",
                value: "country",
                displayValue: "country",
                id: 1,
                attribute_type: "string"
            },
            {
                control_type: "mathOperators",
                value: "$eq",
                displayValue: "=",
                id: 2
            },
            {
                control_type: "constant",
                value: "US",
                displayValue: "US",
                id: 3
            }
        ],
    }
];