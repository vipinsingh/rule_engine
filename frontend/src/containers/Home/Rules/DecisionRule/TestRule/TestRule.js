import React, { Component } from "react";
import * as actions from "../../../../../store/actions/index";
import { connect } from "react-redux";
import Spinner from "../../../../../components/UI/Spinner/Spinner";
import classes from './TestRule.module.css';
import { Button, Card, Form, Col } from "react-bootstrap";
import { FIELD_TYPE_CONFIG } from "../../../../../shared/constants";
import { updateObject } from "../../../../../shared/utility";
import { toast } from "react-toastify";

class TestRule extends Component {

    state = {
        fieldValues : {}
    }

    inputChangeHandler = (event, fieldName) => {
        const updatedFieldValues = updateObject(this.state.fieldValues, {
            [fieldName] : event.target.value
        });

        this.setState({ fieldValues : updatedFieldValues });
    }

    setFormControl = (fieldName, type) => {
        switch(type) {
            case 'string' :
                return (
                    <Form.Control
                        type={FIELD_TYPE_CONFIG[type].type}
                        value={this.state.fieldValues[fieldName] ? this.state.fieldValues[fieldName] : ''}
                        onChange={(event) => this.inputChangeHandler(event, fieldName)}
                    />
                );
            case 'number' :
                return (
                    <Form.Control
                        type={FIELD_TYPE_CONFIG[type].type}
                        value={this.state.fieldValues[fieldName] ? this.state.fieldValues[fieldName] : ''}
                        onChange={(event) => this.inputChangeHandler(event, fieldName)}
                    />
                );
            case 'boolean' :
                const options = FIELD_TYPE_CONFIG[type].options.map(option => {
                    return <option key={option.value} value={option.value}>{option.displayValue}</option>;
                });
                return (
                    <Form.Control
                        as={FIELD_TYPE_CONFIG[type].type}
                        value={this.state.fieldValues[fieldName] ? this.state.fieldValues[fieldName] : ''}
                        onChange={(event) => this.inputChangeHandler(event, fieldName)}
                        className={classes.FormControl}
                    > {options} </Form.Control>
                );
            default : return null;
        }
    }

    componentDidMount() {
        const params = {
            user_id : this.props.user_id, 
            project_id : this.props.match.params.projectId
        }
        this.props.onFetchRuleDetails(this.props.match.params.ruleId, params);
    }

    componentDidUpdate() {
        if(this.props.error && !this.props.error.errors) {
            toast.error('An error occured!');
            this.props.onErrorReset();
        }
    }

    onClickSendButton = () => {
        this.props.onGetDecision(this.props.match.params.ruleId, {...this.state.fieldValues})
    }

    render() {
        const spinner = this.props.loading ? <Spinner /> : null;

        const fields = {};

        this.props.rule_expression.forEach(expression => {
            if(expression.control_type === 'attribute') {
                if(!fields[expression.value]) {
                    fields[expression.value] = expression.attribute_type;
                }
            }
            if(expression.control_type === 'expression') {
                expression.value.forEach(exp => {
                    if(exp.control_type === 'attribute') {
                        if(!fields[exp.value]) {
                            fields[exp.value] = exp.attribute_type;
                        }
                    }
                })
            }
        });

        this.props.decision_expression.forEach(expression => {
            if(expression.control_type === 'attribute') {
                if(!fields[expression.value]) {
                    fields[expression.value] = expression.attribute_type;
                }
            }
        });

        this.props.default_decision_expression.forEach(expression => {
            if(expression.control_type === 'attribute') {
                if(!fields[expression.value]) {
                    fields[expression.value] = expression.attribute_type;
                }
            }
        });

        const form = Object.keys(fields).map(field => {
            const formControl = this.setFormControl(field, fields[field]);
            const apiKey = field.length > 20 ? field.substring(0,16) + '...' : field;

            return (
                <Form.Row key={field} className={classes.FormRow}>
                    <Col style={{ width : '100px' }}>
                        <Form.Label className={classes.FormLabel}><strong>{apiKey} :</strong></Form.Label>
                    </Col>
                    <Col>
                        {formControl}
                    </Col>
                </Form.Row>
            );
        });
        
        let jsonData = {
            'step1' : 'Fill the form',
            'step2' : 'Click on Send button'
        }

        if(this.props.ruleDecision) {
            jsonData = this.props.ruleDecision;
        }

        if(this.props.error && this.props.error.errors) {
            jsonData = this.props.error.errors;
        }

        return (
            <React.Fragment>
                {spinner}
                <h6 style={{ margin : '40px 50px' }}>
                    You can use this form to create an API request to your rule.
                </h6>
                <h3 style={{ marginLeft : '50px'}}>
                    {this.props.ruleInfo ? this.props.ruleInfo.name : ''}
                </h3>
                <label style={{ marginLeft : '50px', fontSize : '18px' }}>
                    {this.props.ruleInfo ? this.props.ruleInfo.description : ''}
                </label>
                <div className="row" style={{ width : '95%' }}>
                    <div className="col-md-6">
                        {form}
                        <Button variant="primary"
                            style={{float : 'right', marginTop : '30px', marginBottom : '10px'}}
                            onClick={this.onClickSendButton}
                        >
                            SEND
                        </Button>
                    </div>
                    <div className="col-md-6">
                        <Card style={{ marginTop : '30px', marginLeft : '50px'}}>
                            <Card.Body>
                                <pre>
                                    {JSON.stringify(jsonData, undefined, 2)}
                                </pre>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </React.Fragment>
        );
    }

    componentWillUnmount() {
        this.props.onClearRuleState();
    }
}

const mapStateToProps = state => {
    return {
        ruleInfo : state.decRule.ruleInfo,
        ruleDecision : state.decRule.ruleDecision,
        rule_expression : state.decRule.rule_expression,
        decision_expression : state.decRule.decision_expression,
        default_decision_expression : state.decRule.default_decision_expression,
        loading : state.decRule.loading,
        error : state.decRule.error,
        user_id : state.auth.userData.user_id
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchRuleDetails : (ruleId, params) => dispatch(actions.getRule(ruleId, params)),
        onClearRuleState : () => dispatch(actions.clearRuleState()),
        onErrorReset : () => dispatch(actions.resetRuleError()),
        onGetDecision : (ruleId, data) => dispatch(actions.getRuleDecision(ruleId, data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TestRule);