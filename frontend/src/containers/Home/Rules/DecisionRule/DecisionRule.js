import React, { Component } from "react";
import classes from './DecisionRule.module.css';
import { Form, Button, Col } from "react-bootstrap";
import { createRuleForm, RULE_ATTRIBUTES, MATH_OPERATORS, COMP_OPS } from './formConfig';
import FormControl from "../../../../components/DynamicForm/FormControl/FormControl";
import { updateObject, checkValidity, validateExpression, validateDecisionFieldAccordingToDecType } from "../../../../shared/utility";
import { connect } from "react-redux";
import { OPERATORS } from "../../../../shared/constants";
import Spinner from "../../../../components/UI/Spinner/Spinner";
import * as actions from '../../../../store/actions/index';
import { toast } from "react-toastify";

class DecisionRule extends Component {

    state = {
        createRuleForm : createRuleForm,
        formIsValid  :  false,
        showEditDel : {},
        filteredSuggestions : {},
        draggedItemIndex : null,
        decisionCompute : false,
        defaultDecisionCompute : false,
        currentSelectedExp : 1
    }

    inputChangedHandler = (event, controlName) => {
        let updatedControls = updateObject(this.state.createRuleForm, {
            [controlName] : updateObject(this.state.createRuleForm[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.createRuleForm[controlName].validation),
                touched : true
            }) 
        });

        if(controlName === 'decision_type' || controlName === 'default_decision_type') {
            const type = event.target.value;
            const fieldToValidate = controlName === 'decision_type' ? 'decision' : 'default_decision';
            updatedControls = validateDecisionFieldAccordingToDecType(updatedControls, type, fieldToValidate);

            this.setComputationalDecision(type, controlName);

            updatedControls = this.setRuleResultValidation(updatedControls, controlName, type);
        }

        if(controlName === 'specify_default_decision') {
            const specify_default_decision_value = event.target.value;
            updatedControls = this.validateDefaultDecisionFields(updatedControls, specify_default_decision_value);
        }

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        this.setEditViewMode();

        this.setState({createRuleForm : updatedControls, formIsValid : formIsValid});
    }

    setRuleResultValidation = (controls, controlName, type) => {
        if(controlName === 'decision_type' && type === 'rule_result') {
            ['decision', 'specify_default_decision', 'default_decision_type', 'default_decision'].forEach(field => {
                controls = updateObject(controls, {
                    [field] : updateObject(controls[field], {
                        value : '',
                        valid : true,
                        touched : false
                    }) 
                });
            })
        } else if(controlName === 'decision_type' && type !== 'rule_result') {
            ['decision', 'specify_default_decision', 'default_decision_type', 'default_decision'].forEach(field => {
                controls = updateObject(controls, {
                    [field] : updateObject(controls[field], {
                        value : controls[field].value,
                        valid : checkValidity(controls[field].value, controls[field].validation),
                        touched : controls[field].touched
                    }) 
                });
            })
        }
        return controls;
    }

    setComputationalDecision = (type, controlName) => {
        if(type === 'compute' && controlName === 'decision_type') {
            this.setState({ decisionCompute : true });
        } else if(type !== 'compute' && controlName === 'decision_type' && this.state.decisionCompute === true) {
            this.setState({ decisionCompute : false });
        }

        if(type === 'compute' && controlName === 'default_decision_type') {
            this.setState({ defaultDecisionCompute : true });
        } else if (type !== 'compute' && controlName === 'default_decision_type' && this.state.defaultDecisionCompute === true) {
            this.setState({ defaultDecisionCompute : false });
        }
    }

    validateDefaultDecisionFields = (controls, specifyField) => {
        const validateDecisionType = specifyField === 'yes' ? {
            required : true
        } : false;

        const validationDefaultDecision = specifyField === 'yes' ? {
            required : true,
            minLength : 1
        } : false;

        controls = updateObject(controls, {
            'default_decision_type' : updateObject(controls['default_decision_type'], {
                validation  : validateDecisionType,
                valid : checkValidity(controls['default_decision_type'].value, validateDecisionType),
                value : '',
                touched : false,
            }),
            'default_decision' : updateObject(controls['default_decision'], {
                validation  : validationDefaultDecision,
                valid : checkValidity(controls['default_decision'].value, validationDefaultDecision),
                value : '',
                touched : false,
            })
        });

        return controls;
    }

    addToExpression = (ruleExpression, controlType) => {
        const key = this.props[ruleExpression].length + 1;

        const updatedShowEditDel = updateObject(this.state.showEditDel, {
            [ruleExpression] : updateObject(this.state.showEditDel[ruleExpression], {
                [key] : false
            })
        });
    
        this.setState({ showEditDel : updatedShowEditDel })

        const controlObj = {
            control_type : controlType,
            value : '',
            displayValue : ''
        }

        this.setEditViewMode();

        this.props.onAddRuleExpression(ruleExpression, controlObj);
    }

    selectListOption = (value, displayValue, expressionId, ruleExpression, attributeType = null, expression_id = null) => {
        let editObj = {
            value,
            displayValue
        };

        if(attributeType) {
            editObj['attribute_type'] = attributeType;
        }
        
        if(expression_id) {
            editObj['expression_id'] = expression_id;
        }

        this.props.onEditExpression(ruleExpression, expressionId, editObj);
    }

    onClickLabel = (ruleExpression, expressionId) => {
        const updatedShowEditDel = updateObject(this.state.showEditDel, {
            [ruleExpression] :  updateObject(this.state.showEditDel[ruleExpression], {
                [expressionId] : !this.state.showEditDel[ruleExpression][expressionId]
            })
        });
        
        this.setState({ showEditDel : updatedShowEditDel });
    }

    onEnterConditionValueInput = (event, expressionId, ruleExpression) => {
        if (event.keyCode === 13) {
            this.props.onEditExpression(ruleExpression, expressionId, {
                value : event.target.value,
                displayValue : event.target.value
            });
        }
    }

    setFilteredSuggestions = (expressionId, ruleExpression, value, arrToFilterFrom) => {

        const suggestions = arrToFilterFrom.filter(option => {
            if(value && option.displayValue.indexOf(value) >  -1) {
                return option;
            }
            return false;
        })

        const suggestionArr = suggestions.length > 0 ? suggestions : [];

        const updatedFilteredSuggestions = updateObject(this.state.filteredSuggestions, {
            [ruleExpression] : updateObject(this.state.filteredSuggestions[ruleExpression], {
                [expressionId] : suggestionArr
            })
        });

        this.setState({ filteredSuggestions : updatedFilteredSuggestions});
    }

    ruleInputChangedHandler = (event, expressionId, expressionType, ruleExpression) => {
        if(expressionType === 'attribute') {
            this.setFilteredSuggestions(expressionId, ruleExpression, event.target.value, RULE_ATTRIBUTES);
        } else if(expressionType === 'expression') {
            const params = {
                user_id : this.props.user_id,
                project_id : this.props.match.params.projectId,
                value : event.target.value
            };
            this.props.onGetExpressions(params, expressionId, ruleExpression);
            // this.setFilteredSuggestions(expressionId, ruleExpression, event.target.value, EXP_OPTIONS);
        }
    }

    onEditDelSelControl = (ruleExpression, expressionId, action) => {
        if(action === 'edit') {
            this.setEditViewMode();
            this.props.onEditExpression(ruleExpression, expressionId, {
                value : '',
                displayValue : ''
            });
        } else if(action === 'delete') {
            this.setEditViewMode();
            this.props.onDeleteExpression(ruleExpression, expressionId);
        } else if(action === 'select' || action === 'deselect') {
            this.props.onEditExpression(ruleExpression, expressionId, {
                is_selected : action === 'select' ? true : undefined,
                exp_name : action === 'select' ? ('E' + this.state.currentSelectedExp) : undefined
            });
        }

        const updatedShowEditDel = updateObject(this.state.showEditDel, {
            [ruleExpression] :  updateObject(this.state.showEditDel[ruleExpression], {
                [expressionId] : !this.state.showEditDel[ruleExpression][expressionId]
            })
        });

        const updatedFilteredSuggestions = updateObject(this.state.filteredSuggestions, {
            [ruleExpression] :  updateObject(this.state.filteredSuggestions[ruleExpression], {
                [expressionId] : null
            })
        })
        
        this.setState({ showEditDel : updatedShowEditDel, filteredSuggestions : updatedFilteredSuggestions });
    }

    handleDragStart = (event) => {
        const start = event.target.id
        this.setState({ draggedItemIndex : start });
    }

    handleDragOver = (event) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'move' ;
    }

    handleDrop = (event, ruleExpression) => {
        const dropId = event.currentTarget.id;
        if(this.state.draggedItemIndex !== dropId) {
            this.setEditViewMode();
            this.props.onReOrderRuleExpression(ruleExpression, (this.state.draggedItemIndex - 1), (dropId - 1));
        }

        this.setState({ draggedItemIndex : null });
    }

    showMathOperatorButton = (ruleExpression) => {
        if(this.props[ruleExpression].length > 0 && ruleExpression === 'rule_expression') {
            let lastIndex = this.props[ruleExpression].length - 1;
            if(this.props[ruleExpression][lastIndex].control_type === 'operator') {
                return ['(', ')'].includes(this.props[ruleExpression][lastIndex].displayValue);
            }
            return ['attribute', 'constant'].includes(this.props[ruleExpression][lastIndex].control_type);
        }
        return true;
    }

    showConstantButton = (ruleExpression) => {
        if(this.props[ruleExpression].length > 0) {
            let lastIndex = this.props[ruleExpression].length - 1;
            if(this.props[ruleExpression][lastIndex].control_type === 'operator') {
                return this.props[ruleExpression][lastIndex].displayValue !== ')';
            }
            return !['attribute', 'expression', 'constant'].includes(this.props[ruleExpression][lastIndex].control_type);
        }
        return true;
    }

    showExpressionButton = () => {
        if(this.props.rule_expression.length > 0) {
            let lastIndex = this.props.rule_expression.length - 1;
            if(this.props.rule_expression[lastIndex].control_type === 'operator') {
                return this.props.rule_expression[lastIndex].displayValue !== ')';
            }
            return false;
        }
        return true;
    }

    showAttributeButton = (ruleExpression) => {
        if(this.props[ruleExpression].length > 0) {
            let lastIndex = this.props[ruleExpression].length - 1;
            if(this.props[ruleExpression][lastIndex].control_type === 'operator') {
                return this.props[ruleExpression][lastIndex].displayValue !== ')';
            }
            return this.props[ruleExpression][lastIndex].control_type === 'mathOperators';
        }
        return true;
    }

    getMathOpAccordingToAttriType = (expressionId) => {
        if(this.props.rule_expression.length > 0) {
            let prevIndex = (expressionId - 2) >=0 ? expressionId - 2 : 0;
            if(this.props.rule_expression[prevIndex].control_type === 'attribute') {
                return MATH_OPERATORS[this.props.rule_expression[prevIndex].attribute_type]
            }
            return MATH_OPERATORS['all'];
        }
        return MATH_OPERATORS['all'];
    }

    showSaveExpButton = () => {
        const selectedExp = this.props.rule_expression.filter(exp => {
            if(exp.is_selected && exp.exp_name && exp.exp_name === "E"  + this.state.currentSelectedExp) {
                return exp;
            }
            return false;
        });

        return selectedExp.length > 0;
    }

    saveExpression = () => {
        let selectedExp = this.props.rule_expression.filter(exp => {
            if(exp.is_selected && exp.exp_name && exp.exp_name === "E"  + this.state.currentSelectedExp) {
                return exp;
            }
            return false;
        });
        // console.log('sel_exp:::', selectedExp);
        if(!validateExpression(selectedExp)) {
            toast.error('Please select a valid expression!');
            return;
        } else {
            const expData = {
                user_id : this.props.user_id,
                project_id : this.props.match.params.projectId,
                expression : selectedExp
            };

            this.props.onSaveExpression(expData);
        }
        return;
    }

    setEditViewMode() {
        if(this.props.viewMode === 'view' && this.props.match.params.ruleId) {
            this.props.onSetViewMode('edit');
        }
    }

    componentDidMount() {
        if(this.props.match.params.ruleId && !this.props.isRuleCreated) {
            if(this.props.ruleInfo) {
                let updatedControls = this.state.createRuleForm;
                Object.keys(this.state.createRuleForm).forEach(control => {
                    updatedControls = updateObject(updatedControls, {
                        [control] : updateObject(updatedControls[control], {
                            value : this.props.ruleInfo[control] || '',
                            valid : true
                        })
                    })
                });
                this.setState({ createRuleForm : updatedControls});
                this.props.onSetViewMode('view');
            } else {
                const params = {
                    user_id : this.props.user_id, 
                    project_id : this.props.match.params.projectId
                }
                this.props.onFetchRuleDetails(this.props.match.params.ruleId, params);
            }
        }
    }

    componentDidUpdate(prevProps) {
        if(this.props.error) {
            toast.error('An error occured!');
            this.props.onErrorReset();
        }

        if(this.props.expression_saved === 'exp' || this.props.expression_saved === 'rule') {
            const expOrRule = this.props.expression_saved === 'exp' ? 'Expression' : 'Rule';
            toast.success(`${expOrRule} Saved!`);
            this.setState((prevState) => {
                return {
                    currentSelectedExp : prevState.currentSelectedExp + 1
                }
            });
            this.props.resetExpressionSaved();
        }

        if(this.props.isRuleCreated && !this.props.match.params.ruleId) {
            this.props.history.replace(`/projects/${this.props.match.params.projectId}/rules/${this.props.ruleInfo.id}`);
        }

        if(this.props.match.params.ruleId && this.props.isRuleCreated !== prevProps.isRuleCreated && this.props.isRuleCreated === true) {
            if(this.props.ruleInfo && this.props.isRuleCreated) {
                let updatedControls = this.state.createRuleForm;
                Object.keys(this.state.createRuleForm).forEach(control => {
                    updatedControls = updateObject(updatedControls, {
                        [control] : updateObject(updatedControls[control], {
                            value : this.props.ruleInfo[control] || '',
                            valid : true
                        })
                    })
                });

                let updatedShowEditDel = this.state.showEditDel;
                updatedShowEditDel = this.initUpdatedShowEditDel(updatedShowEditDel);

                this.setState({ createRuleForm : updatedControls, formIsValid : true, showEditDel : updatedShowEditDel});
            }
        }

        if(this.props.match.params.ruleId && this.props.ruleInfo !== prevProps.ruleInfo) {
            let updatedControls = this.state.createRuleForm;
            Object.keys(this.state.createRuleForm).forEach(control => {
                updatedControls = updateObject(updatedControls, {
                    [control] : updateObject(updatedControls[control], {
                        value : this.props.ruleInfo[control] || '',
                        valid : true
                    })
                })
            });

            let updatedShowEditDel = this.state.showEditDel;
            updatedShowEditDel = this.initUpdatedShowEditDel(updatedShowEditDel);

            let updatedDecisionCompute = false;
            let updatedDefaultDecisionCompute = false;
            if(this.props.ruleInfo.decision_type === 'compute' && this.props.decision_expression.length > 0) {
                updatedDecisionCompute = true;
                updatedControls = this.initComputationalFields(updatedControls, 'decision');
            }

            if(this.props.ruleInfo.default_decision_type === 'compute' && this.props.default_decision_expression.length > 0) {
                updatedDefaultDecisionCompute = true;
                updatedControls = this.initComputationalFields(updatedControls, 'default_decision');
            }

            this.setState({
                createRuleForm : updatedControls,
                formIsValid : true,
                showEditDel : updatedShowEditDel,
                decisionCompute : updatedDecisionCompute,
                defaultDecisionCompute : updatedDefaultDecisionCompute
            });
        }
    }

    initComputationalFields = (controls, field) => {
        controls = updateObject(controls, {
            [field] : updateObject(controls[field], {
                value : '',
                valid : true
            })
        })

        return controls
    }

    initUpdatedShowEditDel = (updatedShowEditDel) => {
        if(this.props.rule_expression && this.props.rule_expression.length > 0) {
            updatedShowEditDel = this.setUpdatedShowEditDel(updatedShowEditDel, 'rule_expression', this.props.rule_expression);
        }

        if(this.props.decision_expression && this.props.decision_expression.length > 0) {
            updatedShowEditDel = this.setUpdatedShowEditDel(updatedShowEditDel, 'decision_expression', this.props.decision_expression);
        }

        if(this.props.default_decision_expression && this.props.default_decision_expression.length > 0) {
            updatedShowEditDel = this.setUpdatedShowEditDel(updatedShowEditDel, 'default_decision_expression', this.props.default_decision_expression);
        }

        return updatedShowEditDel;
    }

    setUpdatedShowEditDel = (updatedShowEditDel, ruleExpression, expression) => {
        expression.forEach(exp => {
            updatedShowEditDel = updateObject(updatedShowEditDel, {
                [ruleExpression] : updateObject(updatedShowEditDel[ruleExpression], {
                    [exp.id] : false
                })
            })
        })

        return updatedShowEditDel;
    }

    onClickSubmitButton = (event, action) => {
        event.preventDefault();

        if(action === 'revert') {
            const params = {
                user_id : this.props.user_id, 
                project_id : this.props.match.params.projectId
            }
            this.props.onFetchRuleDetails(this.props.match.params.ruleId, params);
            this.setState({ decisionCompute : false, defaultDecisionCompute : false })
            return;
        }

        if(action === 'test_rule') {
            this.props.history.push(`/projects/${this.props.match.params.projectId}/rules/${this.props.match.params.ruleId}/test`);
            return;
        }

        if(!validateExpression(this.props.rule_expression)) {
            toast.error('Please create a valid rule!');
            return;
        }

        if(this.state.createRuleForm.decision_type.value === 'compute') {
            if(!validateExpression(this.props.decision_expression)) {
                toast.error('Decision computational expression is invalid!');
                return;
            }
        }

        if(this.state.createRuleForm.default_decision_type.value === 'compute') {
            if(!validateExpression(this.props.default_decision_expression)) {
                toast.error('Default Decision computational expression is invalid!');
                return;
            }
        }

        const ruleData = {
            user_id : this.props.user_id,
            project_id : this.props.match.params.projectId,
            name : this.state.createRuleForm.name.value,
            description : this.state.createRuleForm.description.value,
            rule_expression : this.props.rule_expression,
            decision_type : this.state.createRuleForm.decision_type.value,
            decision : this.state.createRuleForm.decision_type.value === 'compute' ? this.props.decision_expression : this.state.createRuleForm.decision.value,
            specify_default_decision : this.state.createRuleForm.specify_default_decision.value,
            default_decision_type : this.state.createRuleForm.default_decision_type.value,
            default_decision : this.state.createRuleForm.default_decision_type.value === 'compute' ? this.props.default_decision_expression : this.state.createRuleForm.default_decision.value,
        };

        if(action === 'save') {
            this.props.onSaveRule(ruleData);
        } else if(action === 'edit') {
            this.props.onEditRule(this.props.match.params.ruleId, ruleData);
        }
    }

    render() {
        // console.log('rule_exp:::', this.props.rule_expression);
        // console.log('dec_exp:::', this.props.decision_expression);
        // console.log('def_exp:::', this.props.default_decision_expression);
        // console.log('current sel exp:::', this.state.currentSelectedExp);
        // console.log('suggest:::', this.props.filtered_suggestions);
        // console.log('props:::', this.props);
        // console.log('rule form::::', this.state.createRuleForm);

        const spinner = this.props.loading ? <Spinner /> : null;
        
        const opList = (ruleExpression, expressionId) => {
            const list = Object.keys(OPERATORS).map(key => {
                return <li key={key} onClick={() => this.selectListOption(OPERATORS[key], key, expressionId, ruleExpression)}>{key}</li>;
            });
            return list;
        }

        const getSuggestion = (ruleExpression, expressionId) => {
            if(this.state.filteredSuggestions[ruleExpression] && this.state.filteredSuggestions[ruleExpression][expressionId] && this.state.filteredSuggestions[ruleExpression][expressionId].length > 0)  {
                const list = this.state.filteredSuggestions[ruleExpression][expressionId].map(option => {
                    return <li key={option.id} onClick={() => this.selectListOption(option.value, option.displayValue, expressionId, ruleExpression, option.fieldType)}>{option.displayValue}</li>;
                })
                return list;
            }
            return null;
        }

        const getSuggestionForExpression = (ruleExpression, expressionId) => {
            if(this.props.filtered_suggestions[ruleExpression] && this.props.filtered_suggestions[ruleExpression][expressionId] && this.props.filtered_suggestions[ruleExpression][expressionId].length > 0)  {
                const list = this.props.filtered_suggestions[ruleExpression][expressionId].map(option => {
                    return <li key={option.id} onClick={() => this.selectListOption(option.expression, option.displayValue, expressionId, ruleExpression, null, option.id)}>{option.displayValue}</li>;
                })
                return list;
            }
            return null;
        }

        const mathOpList = (ruleExpression, expressionId) => {
            const operatorObj = ruleExpression === 'rule_expression' ? this.getMathOpAccordingToAttriType(expressionId) : COMP_OPS;

            const list = Object.keys(operatorObj).map(key => {
                return <li key={key} onClick={() => this.selectListOption(operatorObj[key], key, expressionId, ruleExpression)}>{key}</li>;
            });
            return list;
        }

        let submitButton = <Button variant="primary" className={classes.FormButton} disabled={!this.state.formIsValid} onClick={(event) => this.onClickSubmitButton(event, 'save')}>SAVE</Button>;

        if(this.props.viewMode === 'view') {
            submitButton = <Button variant="info" className={classes.FormButton} onClick={(event) => this.onClickSubmitButton(event, 'test_rule')}>TEST RULE</Button>;
        } else if(this.props.viewMode === 'edit') {
            submitButton = (
                <div>
                    <Button variant="primary" className={classes.FormButton} disabled={!this.state.formIsValid} onClick={(event) => this.onClickSubmitButton(event, 'edit')}>SAVE CHANGES</Button>
                    <Button variant="secondary" style={{marginRight : '20px'}} className={classes.FormButton} onClick={(event) => this.onClickSubmitButton(event, 'revert')}>REVERT</Button>
                </div>
            );
        }

        const computationalDecisionField = (rule_expression) => {
            return (
                <div className={["row"].join(' ')}>
                    <div className={["col-10", "mt-4", "d-flex", classes.ruleParam, "pl-0", "pr-0"].join(' ')}>
                        <h6 style={{fontWeight : '600', marginLeft : '15px'}}>
                            {
                                rule_expression === 'decision_expression' ? 'Decision' : 'Default Decision'
                            }
                        </h6>
                        <div className={["ml-auto"].join(' ')}>
                            {
                                this.showAttributeButton(rule_expression) ?
                                (
                                    <button  className={[classes.btn, classes.expression, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression(rule_expression, 'attribute')}>
                                        Attributes
                                    </button>
                                ) : null
                            }
                            {
                                this.showConstantButton(rule_expression) ?
                                (
                                    <button className={[classes.btn, classes.constant, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression(rule_expression, 'constant')}>
                                        Constant
                                    </button>
                                ) : null
                            }
                            {
                                this.showMathOperatorButton(rule_expression) ? 
                                    (
                                        <button className={[classes.btn, classes.mathOperators, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression(rule_expression, 'mathOperators')}>
                                            Operators
                                        </button>
                                    ) : null
                            }
                        </div>
                    </div>
                    <div className={["col-10", "mt-4", classes["bg-light"], "pt-3"].join(' ')}>
                        <div  className={[classes.dropdown,  "mt-2", "d-inline-flex", "flex-wrap"].join(' ')}>
                            {ruleExpression(rule_expression)}
                        </div>
                    </div>
                </div>
            );
        }

        const ruleExpression = (ruleExpression) => {
            const dragObj = {
                draggable : true,
                onDragStart : (event) => this.handleDragStart(event),
                onDragOver : (event) => this.handleDragOver(event),
                onDrop : (event) => this.handleDrop(event, ruleExpression),
            }

            const toolTipObj = (exp) => {
                if(exp.is_selected && exp.exp_name) {
                    return {
                        'data-toggle' : "tooltip",
                        'data-placement' : "bottom",
                        'title' : exp.exp_name
                    }
                }
                return {};
            }

            return this.props[ruleExpression].map(expression => {

                const isExpSelectedLabel = expression.is_selected ? 'Deselect' : 'Select';
                const isExpSelectedAction = expression.is_selected ? 'deselect' : 'select';

                let editDelPopOver = this.state.showEditDel[ruleExpression] && this.state.showEditDel[ruleExpression][expression.id] ?
                    (
                        <div className={classes.editDel} style={{ width : ruleExpression === 'rule_expression' ? '150px' : '85px'}}>
                            <span className={classes.edit} onClick={() => this.onEditDelSelControl(ruleExpression, expression.id, 'edit')}>Edit </span>
                            <span className={classes.delete} onClick={() => this.onEditDelSelControl(ruleExpression, expression.id, 'delete')}>Delete </span>
                            {
                                ruleExpression === 'rule_expression' ? 
                                    <span className={classes.delete} onClick={() => this.onEditDelSelControl(ruleExpression, expression.id, isExpSelectedAction)}>{isExpSelectedLabel} </span>
                                    : null
                            }
                        </div>
                    ) : null;
    
                let labelClasses = this.state.showEditDel[ruleExpression] && this.state.showEditDel[ruleExpression][expression.id] ? [classes.active] :  [];

                let divSelectedClass = (expression.is_selected && ruleExpression === 'rule_expression') ? classes.expSelected : '';
                labelClasses.push(divSelectedClass);
    
                switch(expression.control_type) {
                    case 'attribute' :
                        labelClasses.push(classes.tableButton);
    
                        return (
                            <div key={expression.id} id={expression.id} {...dragObj} className={["pr-3", classes.tableDiv, classes.poSticky].join(' ')}>
                                {expression.value ? 
                                (<React.Fragment>
                                    <label className={labelClasses.join(' ')} {...toolTipObj(expression)} onClick={() => this.onClickLabel(ruleExpression, expression.id)}>{expression.displayValue}</label>
                                    {editDelPopOver}
                                </React.Fragment>) :
                                (<ul className={classes['dropdown-menu']}>
                                    <input type="text" placeholder="Search.." onChange={(event) => this.ruleInputChangedHandler(event, expression.id, expression.control_type, ruleExpression)}/>
                                    <div className={classes.dropDownList}>
                                        {getSuggestion(ruleExpression, expression.id)}
                                    </div>
                                </ul>)}                       
                            </div>
                        );
    
                    case 'expression' :
                        labelClasses.push(classes.tableButton2);
    
                        return (
                            <div key={expression.id} id={expression.id} {...dragObj} className={["pr-3", classes.tableDiv2, classes.poSticky].join(' ')}>
                                {expression.displayValue ?
                                (<React.Fragment>
                                    <label className={labelClasses.join(' ')} {...toolTipObj(expression)} onClick={() => this.onClickLabel(ruleExpression, expression.id)}>{expression.displayValue}</label>
                                    {editDelPopOver}
                                </React.Fragment>) :                                
                                (<ul className={classes['dropdown-menu']}>
                                    <input type="text" placeholder="Condition" onChange={(event) => this.ruleInputChangedHandler(event, expression.id, expression.control_type, ruleExpression)} />
                                    <div className={classes.dropDownList}>
                                        {getSuggestionForExpression(ruleExpression, expression.id)}
                                    </div>
                                </ul>)}                          
                            </div>                        
                        );
    
                    case 'operator' :
                        labelClasses.push(classes.tableButton3);
    
                        return (
                            <div key={expression.id} id={expression.id} {...dragObj} className={["pr-3", classes.tableDiv3, classes.poSticky].join(' ')}>
                                {expression.value ?
                                    (<React.Fragment>
                                        <label className={labelClasses.join(' ')} {...toolTipObj(expression)} onClick={() => this.onClickLabel(ruleExpression, expression.id)}>{expression.displayValue}</label>
                                        {editDelPopOver}
                                    </React.Fragment>) :
                                (<ul className={classes['dropdown-menu']}>
                                    <input type="text" placeholder="Operator" readOnly />
                                    <div className={classes.dropDownList}>
                                        {opList(ruleExpression, expression.id)}
                                    </div>
                                </ul>) }                               
                            </div>
                        );
                    
                    case 'constant' :
                        labelClasses.push(classes.tableButton4);
    
                        return (
                            <div key={expression.id} id={expression.id} {...dragObj} className={["pr-3", classes.tableDiv4, classes.poSticky].join(' ')}>
                                {expression.value ? 
                                    (<React.Fragment>
                                        <label className={labelClasses.join(' ')} {...toolTipObj(expression)} onClick={() => this.onClickLabel(ruleExpression, expression.id)}>{expression.value}</label>
                                        {editDelPopOver}
                                    </React.Fragment>) :
                                (<ul className={classes['dropdown-menu']} style={{border : '0px'}}>
                                    <input type="text" placeholder="Value" style={{ border : '1px solid #C32626'}} onKeyDown={(event) => this.onEnterConditionValueInput(event, expression.id, ruleExpression)} />
                                </ul>)}                                
                            </div>
                        );
    
                    case 'mathOperators' :
                        labelClasses.push(classes.tableButton5);
    
                        return (
                            <div key={expression.id} id={expression.id} {...dragObj} className={["pr-3", classes.tableDiv5, classes.poSticky].join(' ')}>
                                {expression.value ? 
                                (<React.Fragment>
                                    <label className={labelClasses.join(' ')} {...toolTipObj(expression)} onClick={() => this.onClickLabel(ruleExpression, expression.id)}>{expression.displayValue}</label>
                                    {editDelPopOver}
                                </React.Fragment>) :                                
                                (<ul className={classes['dropdown-menu']}>
                                    <input type="text" placeholder="Operator" readOnly />
                                    <div className={classes.dropDownList}>
                                        {mathOpList(ruleExpression, expression.id)}
                                    </div>
                                </ul>)}                          
                            </div>                        
                        );
    
                    default : return null;
                }
            })
        }

        return (
            <div className={["container", classes.RootContainer].join(' ')}>
                {spinner}
                <Form>
                    <Form.Group>
                        <Form.Label className={classes.FormLabel}>Rule Name</Form.Label>
                        <FormControl config={this.state.createRuleForm['name']} changed={this.inputChangedHandler} />
                        <Form.Label className={classes.FormLabel}>Rule Description</Form.Label>
                        <FormControl config={this.state.createRuleForm['description']} changed={this.inputChangedHandler} />
                    </Form.Group>
                </Form>
                <div className={["row"].join(' ')}>
                    <div className={["col-10", "mt-4", "d-flex", classes.ruleParam, "pl-0", "pr-0"].join(' ')}>
                        <h3>Rule Parameter</h3>
                        <div className={["ml-auto"].join(' ')}>
                            {
                                this.showAttributeButton('rule_expression') ?
                                (
                                    <button  className={[classes.btn, classes.expression, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression('rule_expression', 'attribute')}>
                                        Attributes
                                    </button>
                                ) : null
                            }
                            {
                                this.showConstantButton('rule_expression') ?
                                (
                                    <button className={[classes.btn, classes.constant, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression('rule_expression', 'constant')}>
                                        Constant
                                    </button>
                                ) : null
                            }
                            {
                                this.showMathOperatorButton('rule_expression') ?
                                    (
                                        <button className={[classes.btn, classes.mathOperators, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression('rule_expression', 'mathOperators')}>
                                            Operators
                                        </button>
                                    ) : null
                            }
                            {
                                this.showExpressionButton() ?
                                    (
                                        <button className={[classes.btn, classes.conditions, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression('rule_expression', 'expression')}>
                                            Expressions
                                        </button>
                                    ) : null
                            }
                            <button  className={[classes.btn, classes.operator].join(' ')} type="button" onClick={() => this.addToExpression('rule_expression', 'operator')}>Logical Operators</button>
                            {
                                this.showSaveExpButton() ?
                                    <Button size="sm" variant="info" className={["ml-2"].join(' ')} onClick={this.saveExpression}>Save Expression</Button>
                                    : null
                            }
                        </div>
                    </div>
                    <div className={["col-10", "mt-4", classes["bg-light"], "pt-3"].join(' ')}>
                        <div  className={[classes.dropdown,  "mt-2", "d-inline-flex", "flex-wrap"].join(' ')}>
                            {ruleExpression('rule_expression')}
                        </div>
                    </div>
                </div>
                <div className={["row"].join(' ')} style={{ marginLeft : '-10px'}}>
                    <Form.Row className={["col-10", "mt-4", classes["bg-light-new"], "pt-3"].join(' ')}>
                        <Col className={["col-2"].join(' ')}>
                            <Form.Label className={classes.FormLabelNew}>DECISION TYPE</Form.Label>
                        </Col>
                        <Col  className={["col-2"].join(' ')}>
                            <FormControl config={this.state.createRuleForm['decision_type']} changed={this.inputChangedHandler} />
                        </Col>
                        {
                            (this.state.createRuleForm.decision_type.value !== 'rule_result' && !this.state.decisionCompute)
                            ? <Col className={["col-3"].join(' ')}>
                                <FormControl config={this.state.createRuleForm['decision']} changed={this.inputChangedHandler} />
                            </Col> : null
                        }
                    </Form.Row>
                </div>
                <br></br>
                {
                    this.state.createRuleForm.decision_type.value !== 'rule_result' ?
                        (this.state.decisionCompute ? computationalDecisionField('decision_expression') : null)
                    : null
                }
                <div className={["row"].join(' ')} style={{ marginLeft : '-10px'}}>
                    <Form.Row className={["col-10", "mt-4", classes["bg-light-new"], "pt-3"].join(' ')}>
                        <Col className={["col-3"].join(' ')}>
                            <Form.Label className={classes.FormLabelNew}>SPECIFY DEFAULT DECISION</Form.Label>
                        </Col>
                        <Col  className={["col-2"].join(' ')}>
                            <FormControl config={this.state.createRuleForm['specify_default_decision']} changed={this.inputChangedHandler} />
                        </Col>
                        {
                            this.state.createRuleForm['specify_default_decision'].value === 'yes'
                            ? (
                                <React.Fragment>
                                    <Col className={["col-3"].join(' ')}>
                                        <Form.Label className={classes.FormLabelNew}>DEFAULT DECISION TYPE</Form.Label>
                                    </Col>
                                    <Col  className={["col-2"].join(' ')}>
                                        <FormControl config={this.state.createRuleForm['default_decision_type']} changed={this.inputChangedHandler} />
                                    </Col>
                                    {
                                        !this.state.defaultDecisionCompute ?
                                        <Col  className={["col-2"].join(' ')}>
                                            <FormControl config={this.state.createRuleForm['default_decision']} changed={this.inputChangedHandler} />
                                        </Col> : null
                                    }
                                </React.Fragment>
                            ) : null
                        }
                    </Form.Row>
                </div>
                <br></br>
                {
                    this.state.defaultDecisionCompute ? 
                    computationalDecisionField('default_decision_expression') : null
                }
                {submitButton}
            </div>
        );
    }

    componentWillUnmount() {
        this.props.onClearRuleState();
    }
}

const mapStateToProps = state => {
    return {
        ruleInfo : state.decRule.ruleInfo,
        rule_expression : state.decRule.rule_expression,
        decision_expression : state.decRule.decision_expression,
        default_decision_expression : state.decRule.default_decision_expression,
        filtered_suggestions : state.decRule.filtered_suggestions,
        expression_saved : state.decRule.expression_saved,
        loading : state.decRule.loading,
        error : state.decRule.error,
        user_id : state.auth.userData.user_id,
        viewMode : state.decRule.viewMode,
        isRuleCreated : state.decRule.isRuleCreated,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddRuleExpression : (ruleExpression, control) => dispatch(actions.addToRuleExpression(ruleExpression, control)),
        onEditExpression : (ruleExpression, controlId, controlObj) => dispatch(actions.editRuleExpression(ruleExpression, controlId, controlObj)),
        onDeleteExpression : (ruleExpression, controlId) => dispatch(actions.deleteRuleExpression(ruleExpression, controlId)),
        onReOrderRuleExpression : (ruleExpression, startIndex, endIndex) => dispatch(actions.reorderRuleExpression(ruleExpression, startIndex, endIndex)),
        onSaveExpression : (expData) => dispatch(actions.saveExpression(expData)),
        onGetExpressions : (params, expId, ruleExpression) => dispatch(actions.getExpressions(params, expId, ruleExpression)),
        onErrorReset : () => dispatch(actions.resetRuleError()),
        resetExpressionSaved : () => dispatch(actions.resetExpressionSaved()),
        onSaveRule : (ruleData) => dispatch(actions.saveRule(ruleData)),
        onSetViewMode : (mode) => dispatch(actions.setRuleViewMode(mode)),
        onClearRuleState : () => dispatch(actions.clearRuleState()),
        onFetchRuleDetails : (ruleId, params) => dispatch(actions.getRule(ruleId, params)),
        onEditRule : (ruleId, data) => dispatch(actions.editDecisionRule(ruleId, data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DecisionRule);