import React, { Component } from 'react';
import Logo from '../../../assets/images/biz2X-logo.svg';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import classes from './NavigationBar.module.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import CreateProject from '../Projects/CreateProject/CreateProject';
import Spinner from '../../../components/UI/Spinner/Spinner';
import * as actions from '../../../store/actions/index';

class NavigationBar extends Component {

    state = {
        createProject : false
    }

    createProject = () => {
        this.setState({ createProject : true });
    }

    closeCreateProject = () => {
        this.setState({ createProject : false });
    }

    componentDidMount() {
        if(this.props.userData && (!this.props.projects || this.props.projects.count === 0)) {
            this.props.onFetchProjects(this.props.userData.user_id);
        }
    }

    onClickProject = (projectId) => {
        this.props.onClickProject(projectId);
    }

    render() {
        const projects = this.props.projects && Array.isArray(this.props.projects.projects) ? this.props.projects.projects : [];

        let navigationItems = <NavDropdown.Item>No Current Projects</NavDropdown.Item>;

        if(projects.length > 0) {
            navigationItems = [];
            for(let i = 0; i < 3; i++) {
                if(projects[i]) {
                    let link = `/projects/${projects[i].id}/tables`;
                    navigationItems.push(
                        <NavDropdown.Item key={projects[i].id} as={Link} to={link} onClick={() => this.onClickProject(projects[i].id)}>{projects[i].name}</NavDropdown.Item>
                    );
                }
            }
        }

        const createProject = this.state.createProject ? <CreateProject
            close={this.closeCreateProject}
        /> : null;

        const spinner = this.props.loading ? <Spinner /> : null;

        let historyAndTablesLink = null;
        if(this.props.currentProj) {
            historyAndTablesLink =   (   
                <React.Fragment>
                    <Nav.Link as={Link} to={`/projects/${this.props.currentProj.id}/tables`}>Tables</Nav.Link>
                    <Nav.Link as={Link} to={`/projects/${this.props.currentProj.id}/rules`}>Rules</Nav.Link>
                    <Nav.Link as={Link} to={`/projects/${this.props.currentProj.id}/rule_set`}>Rule Sets</Nav.Link>
                    <Nav.Link as={Link} to={`/projects/${this.props.currentProj.id}/history`}>History</Nav.Link>
                </React.Fragment>
            );
        };

        return (
            <div>
                <Navbar bg="dark" expand="lg" variant="dark">
                    <Navbar.Brand> <Link to="/projects" ><img src={Logo} className={classes.logoImg} alt="Biz2xLogo"/></Link></Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
                        <Nav className="mr-auto">
                            <Nav.Link as={Link} to="/projects">Home</Nav.Link>
                            <NavDropdown title={this.props.currentProj ? this.props.currentProj.name : "Projects"} id="basic-nav-dropdown">
                                {navigationItems}
                                {projects && projects.length  > 3 ? <NavDropdown.Item as={Link} to="/projects"><strong>View All</strong></NavDropdown.Item> : null}
                                <NavDropdown.Divider />
                                <NavDropdown.Item onClick={this.createProject}>Create Project</NavDropdown.Item>
                            </NavDropdown>
                            {historyAndTablesLink}
                        </Nav>
                        <Nav className="ml-auto">
                            <NavDropdown title={this.props.userData.userName} id="basic-nav-dropdown" className={classes.dropDownWidth}>
                                <NavDropdown.Item>View Profile</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item as={Link} to="/logout">Logout</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                {createProject}
                {spinner}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        userData : state.auth.userData,
        projects : state.projects.projects,
        loading : state.projects.loading,
        currentProj : state.projects.currentProj 
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchProjects : (user_id) => dispatch(actions.fetchProjects(user_id)),
        onClickProject : (projectId) => dispatch(actions.setCurrentProject(projectId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar);