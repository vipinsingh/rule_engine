import React, { Component } from 'react';
import NavigationBar from './NavigationBar/NavigationBar';
import { Route, Switch, Redirect } from 'react-router-dom';
import Projects from './Projects/Projects';
import asyncComponent from '../../hoc/asyncComponent/asyncComponent';

const asyncTablesList = asyncComponent(() => {
    return import('./Tables/TablesList/TablesList');
});

const asyncRulesList = asyncComponent(() => {
    return import('./Rules/RulesList/RulesList');
});

const asyncRuleSetList = asyncComponent(() => {
    return import('./RuleSet/RuleSetList/RuleSetList');
});

const asyncCreateRuleSet = asyncComponent(() => {
    return import('./RuleSet/CreateRuleSet/CreateRuleSet');
});

const asyncDecisionTable = asyncComponent(() => {
    return import('./Tables/DecisionTable/DecisionTable');
});

const asyncDecisionRule = asyncComponent(() => {
    return import('./Rules/DecisionRule/DecisionRule');
});

const asyncDecisionRuleTest = asyncComponent(() => {
    return import('./Rules/DecisionRule/TestRule/TestRule');
});

const asyncDebugger = asyncComponent(() => {
    return import('./Tables/DecisionTable/Debugger/Debugger');
});

const asyncDecisionHistory = asyncComponent(() => {
    return import('./DecisionHistory/DecisionHistory');
});

const asyncDecisionDetails = asyncComponent(() => {
    return import('./DecisionHistory/DecisionDetails/DecisionDetails');
});

class Home extends Component {

    render() {
        let routes = (
            <Switch>
                <Route path="/projects" exact component={Projects} />
                <Route path={`${this.props.match.path}/:projectId/tables`} exact component={asyncTablesList} />
                <Route path={`${this.props.match.path}/:projectId/rules`} exact component={asyncRulesList} />
                <Route path={`${this.props.match.path}/:projectId/rule_set`} exact component={asyncRuleSetList} />
                <Route path={`${this.props.match.path}/:projectId/history`} exact component={asyncDecisionHistory} />
                <Route path={`${this.props.match.path}/:projectId/history/:decId`} exact component={asyncDecisionDetails} />
                <Route path={`${this.props.match.path}/:projectId/tables/create`} component={asyncDecisionTable} />
                <Route path={`${this.props.match.path}/:projectId/tables/:tableId`} exact component={asyncDecisionTable} />
                <Route path={`${this.props.match.path}/:projectId/tables/:tableId/debug`} component={asyncDebugger} />
                <Route path={`${this.props.match.path}/:projectId/tables/:tableId/history`} exact component={asyncDecisionHistory} />
                <Route path={`${this.props.match.path}/:projectId/tables/:tableId/history/:decId`} component={asyncDecisionDetails} />
                <Route path={`${this.props.match.path}/:projectId/rules/create`} component={asyncDecisionRule} />
                <Route path={`${this.props.match.path}/:projectId/rules/:ruleId`} exact component={asyncDecisionRule} />
                <Route path={`${this.props.match.path}/:projectId/rules/:ruleId/test`} component={asyncDecisionRuleTest} />
                <Route path={`${this.props.match.path}/:projectId/rule_set/create`} component={asyncCreateRuleSet} />
                <Route path={`${this.props.match.path}/:projectId/rule_set/:ruleSetId`} component={asyncCreateRuleSet} />
                <Redirect to="/projects" />
            </Switch>
        );

        return (
            <div>
                <NavigationBar />
                {routes}
            </div>
        );
    }
}

export default Home;