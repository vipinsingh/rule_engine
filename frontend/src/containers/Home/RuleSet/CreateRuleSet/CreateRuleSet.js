import React, { Component } from "react";
import classes from './CreateRuleSet.module.css';
import { connect } from "react-redux";
import Spinner from "../../../../components/UI/Spinner/Spinner";
import { Form, Button } from "react-bootstrap";
import { createRuleSetForm } from "./formConfig";
import { updateObject, checkValidity, validateExpression } from "../../../../shared/utility";
import FormControl from "../../../../components/DynamicForm/FormControl/FormControl";
import Expression from "./Expression/Expression";
import * as actions from '../../../../store/actions/index';
import DeleteExpModal from "./DeleteExpModal/DeleteExpModal";
import { toast } from "react-toastify";

class CreateRuleSet extends Component {

    state = {
        createRuleSetForm : createRuleSetForm,
        formIsValid : false,
        showOpDeleteModal : false,
        opDeleteModal : null,
    }

    inputChangedHandler = (event, controlName) => {
        let updatedControls = updateObject(this.state.createRuleSetForm, {
            [controlName] : updateObject(this.state.createRuleSetForm[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.createRuleSetForm[controlName].validation),
                touched : true
            }) 
        });

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        if(this.props.viewMode === 'view') {
            this.props.onSetViewMode('edit');
        }

        this.setState({createRuleSetForm : updatedControls, formIsValid : formIsValid});
    }

    onDeleteOp = (opId) => {
        const opDeleteModal = <DeleteExpModal
                opId={opId}
                close={this.onDeleteOpClose}
            />;
        this.setState({ opDeleteModal : opDeleteModal, showOpDeleteModal : true });
    }

    onDeleteOpClose = () => {
        this.setState({ opDeleteModal : null, showOpDeleteModal : false });
    }

    isRuleSetExpValid = () => {
        let ruleSetLength = this.props.rule_set_exp.length;
        if(ruleSetLength < 2) {
            return false;
        }

        if(this.props.rule_set_exp[0].operator === '$if' && this.props.rule_set_exp[ruleSetLength - 1].operator === '$else') {
            for(let i = 1; i < ruleSetLength - 1; i++) {
                if(this.props.rule_set_exp[i].operator !== '$else_if') {
                    return false;
                }
            }
        } else {
            return false;
        }

        for(let i = 0; i < ruleSetLength; i++) {

            if(this.props.rule_set_exp[i].decision_type === 'compute') {
                if(!validateExpression(this.props.rule_set_exp[i].decision_exp)) {
                    return false;
                }
            } else if(!this.props.rule_set_exp[i].decision_type || !this.props.rule_set_exp[i].then_decision) {
                return false;
            }

            if(this.props.rule_set_exp[i].operator !== '$else') {
                if(!validateExpression(this.props.rule_set_exp[i].rule_expression)) {
                    return false;
                }
            }
        }

        return true;
    }

    onClickSubmitButton = (action) => {
        if(!this.isRuleSetExpValid()) {
            toast.error('Invalid Rule Set!');
            return;
        }

        const ruleSetData = {
            user_id : this.props.user_id,
            project_id : this.props.match.params.projectId,
            name : this.state.createRuleSetForm.name.value,
            description : this.state.createRuleSetForm.description.value,
            rule_set_exp : this.props.rule_set_exp
        }

        if(action === 'save') {
            this.props.onSaveRuleSet(ruleSetData);
        } else if(action === 'update') {
            this.props.onUpdateRuleSet(this.props.match.params.ruleSetId, ruleSetData);
        }

    }

    componentDidUpdate(prevProps) {
        if(this.props.error) {
            toast.error('An error occured!');
            this.props.onResetError();
        }

        if(this.props.isRuleSetCreated && !this.props.match.params.ruleSetId) {
            toast.success('Rule Set Saved!');
            this.props.history.replace(`/projects/${this.props.match.params.projectId}/rule_set/${this.props.rule_set_info.id}`);
        }

        //when we load the component first time
        if(this.props.match.params.ruleSetId && prevProps.rule_set_info !== this.props.rule_set_info) {
            let updatedControls = this.state.createRuleSetForm;

            Object.keys(this.state.createRuleSetForm).forEach(control => {
                updatedControls = updateObject(updatedControls, {
                    [control] : updateObject(updatedControls[control], {
                        value : this.props.rule_set_info[control] || '',
                        valid : true
                    })
                })
            });

            this.updateStateForControls(updatedControls);
        }

        //when component is updated
        if(this.props.match.params.ruleSetId && this.props.isRuleSetCreated !== prevProps.isRuleSetCreated && this.props.isRuleSetCreated === true) {
            let updatedControls = this.state.createRuleSetForm;

            Object.keys(this.state.createRuleSetForm).forEach(control => {
                updatedControls = updateObject(updatedControls, {
                    [control] : updateObject(updatedControls[control], {
                        value : this.props.rule_set_info[control] || '',
                        valid : true
                    })
                })
            });

            toast.success('Rule Set Updated!');
            this.updateStateForControls(updatedControls);
        }
    }

    updateStateForControls = (updatedControls) => {
        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        this.setState({ createRuleSetForm : updatedControls, formIsValid : formIsValid });
    }

    componentDidMount() {
        if(this.props.match.params.ruleSetId && !this.props.isRuleSetCreated) {
            this.props.onGetRuleSet(this.props.match.params.ruleSetId, {
                user_id : this.props.user_id,
                project_id : this.props.match.params.projectId
            });
        }
    }

    render() {
        // console.log('rule_set:::', this.props.rule_set_exp);

        const spinner = this.props.loading ? <Spinner /> : null;

        const expressions = this.props.rule_set_exp.map(rule_set => {
            return <Expression
                        key={rule_set.id}
                        ruleSetOp={rule_set}
                        match={this.props.match}
                        onOperatorDelete={this.onDeleteOp}
                    />
        });

        let submitButton = <Button className={classes.FormButton} variant="primary" disabled={!this.state.formIsValid} onClick={() => this.onClickSubmitButton('save')}>SAVE</Button>;

        if(this.props.viewMode === 'view') {
            submitButton = null;
        }

        if(this.props.viewMode === 'edit') {
            submitButton = <Button className={classes.FormButton} variant="primary" disabled={!this.state.formIsValid} onClick={() => this.onClickSubmitButton('update')}>SAVE CHANGES</Button>;
        }

        return (
            <div className={["container", classes.RootContainer].join(' ')}>
                {spinner}
                <Form>
                    <Form.Group>
                        <Form.Label className={classes.FormLabel}>Rule Set Name</Form.Label>
                        <FormControl config={this.state.createRuleSetForm['name']} changed={this.inputChangedHandler} />
                        <Form.Label className={classes.FormLabel}>Rule Set Description</Form.Label>
                        <FormControl config={this.state.createRuleSetForm['description']} changed={this.inputChangedHandler} />
                    </Form.Group>
                </Form>
                {this.state.showOpDeleteModal ? this.state.opDeleteModal : null}
                <Form style={{marginBottom : '30px'}}>
                    <Form.Group>
                        {expressions}
                    </Form.Group>
                </Form>
                {submitButton}
            </div>
        );
    }

    componentWillUnmount() {
        this.props.onClearState();
    }
}

const mapStateToProps = state => {
    return {
        user_id : state.auth.userData.user_id,
        rule_set_info : state.ruleSet.rule_set_info,
        rule_set_exp : state.ruleSet.rule_set_exp,
        loading : state.ruleSet.loading,
        error : state.ruleSet.error,
        viewMode : state.ruleSet.viewMode,
        isRuleSetCreated : state.ruleSet.isRuleSetCreated,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSaveRuleSet : (data) => dispatch(actions.saveRuleSet(data)),
        onGetRuleSet : (ruleSetId, params) => dispatch(actions.getRuleSet(ruleSetId, params)),
        onUpdateRuleSet : (ruleSetId, data) => dispatch(actions.updateRuleSet(ruleSetId, data)),
        onSetViewMode : (mode) => dispatch(actions.setRuleSetViewMode(mode)),
        onResetError : () => dispatch(actions.resetRuleSetError()),
        onClearState : () => dispatch(actions.clearRuleSetState()) 
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateRuleSet);