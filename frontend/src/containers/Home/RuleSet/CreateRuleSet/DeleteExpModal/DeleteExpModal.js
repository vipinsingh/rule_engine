import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../../../../store/actions/index";
import Modal from "../../../../../components/UI/Modal/Modal";

class DeleteExpModal extends Component {

    state = {
        showModal  : true
    }

    modalCloseHandler = () => {
        this.setState({showModal : false});
        this.props.close();
    }

    modalSubmitHandler = (event) => {
        event.preventDefault();
        this.props.onDeleteOpExp(this.props.opId);
        if(this.props.viewMode === 'view') {
            this.props.onSetViewMode('edit');
        }
        this.props.close();
    }

    render() {
        return (
            <Modal
                show={this.state.showModal}
                handleClose={this.modalCloseHandler}
                modalTitle="Delete Expression?"
                submitLabel="Yes"
                closeLabel="Cancel"
                handleSubmit={this.modalSubmitHandler}
            >
                <p>
                    Are you sure you want to delete this expression?
                </p>
            </Modal>
        );
    }
}

const mapStateToProps = state => {
    return {
        viewMode : state.ruleSet.viewMode
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDeleteOpExp : (opId) => dispatch(actions.deleteOpRuleSet(opId)),
        onSetViewMode : (mode) => dispatch(actions.setRuleSetViewMode(mode)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeleteExpModal);