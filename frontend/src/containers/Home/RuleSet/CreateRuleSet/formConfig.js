export const createRuleSetForm = {
    name :  {
        elementConfig : {
            type : 'text',
            name : 'name',
            placeholder : 'Rule Set Name',
            className : 'RuleNameInput',
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 200
        },
        valid : false,
        touched : false
    },
    description :  {
        elementConfig : {
            type : 'textarea',
            rows : 3,
            name : 'description',
            className : 'RuleTextArea',
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 500
        },
        valid : false,
        touched : false
    }
};

export const ifElseDropdown = {
    elementConfig : {
        type : 'select',
        name : 'flow_control_drop_down',
        className : 'IfElseDropDown',
        options  : [
            {
                value : '',
                displayValue : 'Add Operator'
            },
            {
                value : '$else_if',
                displayValue : 'Else If'
            },
            {
                value : '$else',
                displayValue : 'Else'
            }
        ]
    },
    value : '',
    validation : false,
    valid : false,
    touched : false
};

export const thenDecisionForm  = {
    decision_type :  {
        elementConfig : {
            type : 'select',
            name : 'decision_type',
            className : 'DecisionType',
            options : [
                {
                    value : 'string',
                    displayValue  :  'String'
                },
                {
                    value : 'number',
                    displayValue  :  'Number'
                },
                {
                    value : 'alpha_num',
                    displayValue  :  'AlphaNumeric'
                },
                {
                    value : 'json',
                    displayValue  :  'JSON'
                },
                {
                    value : 'compute',
                    displayValue  :  'Computational'
                }
            ]
        },
        value : 'string',
        validation : false,
        valid : false,
        touched : false
    },
    decision :  {
        elementConfig : {
            type : 'text',
            name : 'decision',
            placeholder : 'Decision',
            className : 'DecisionInput',
        },
        value : '',
        validation : {
            required : true,
            minLength : 1
        },
        valid : false,
        touched : false
    }
}

export const FLOW_CONTROL_OPS = {
    '$if'      : 'IF',
    '$else_if' : 'ELSE IF',
    '$else'    : 'ELSE',
};

export const RULE_SUGGESTIONS = [
    {
        id : 1,
        value : 'Rule 1',
        displayValue : 'Rule 1'
    },
    {
        id : 2,
        value : 'Rule 2',
        displayValue : 'Rule 2'
    },
    {
        id : 3,
        value : 'Rule 3',
        displayValue : 'Rule 3'
    },
    {
        id : 4,
        value : 'Rule 4',
        displayValue : 'Rule 4'
    },
];

export const RULE_OPS = {
    '='       : '$eq',
    '!='      : '$not_eq',
    '>'       : '$gt',
    '>='      : '$gte',
    '<'       : '$lt',
    '<='      : '$lte',
    'in'      : '$min',
    'not in'  : '$not_in',
    'between' : '$between'
}    