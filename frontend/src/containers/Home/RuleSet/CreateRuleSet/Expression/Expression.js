import React, { Component } from "react";
import classes from './Expression.module.css';
import { connect } from "react-redux";
import { thenDecisionForm, RULE_OPS, FLOW_CONTROL_OPS } from "../formConfig";
import { Form, Col } from "react-bootstrap";
import FormControl from "../../../../../components/DynamicForm/FormControl/FormControl";
import { updateObject, checkValidity, validateDecisionFieldAccordingToDecType } from "../../../../../shared/utility";
import * as actions from "../../../../../store/actions/index";
import { RULE_ATTRIBUTES, COMP_OPS } from "../../../Rules/DecisionRule/formConfig";
import { OPERATORS } from "../../../../../shared/constants";
import CloseIcon from '../../../../../assets/images/icons-close.png';

class Expression extends Component {

    state = {
        thenDecisionForm : thenDecisionForm,
        formIsValid : false,
        decisionCompute : false,
        draggedItemIndex : null,
        filteredSuggestions : {},
        showEditDel : {},
    }

    static getDerivedStateFromProps(props, state) {
        let updatedThenDecisionForm = state.thenDecisionForm;
        if(props.ruleSetOp.decision_type) {
            updatedThenDecisionForm = updateObject(updatedThenDecisionForm, {
                'decision_type' : updateObject(updatedThenDecisionForm['decision_type'], {
                    value : props.ruleSetOp.decision_type,
                    valid : true
                })
            });
        }

        if(props.ruleSetOp.then_decision) {
            updatedThenDecisionForm = updateObject(updatedThenDecisionForm, {
                'decision' : updateObject(updatedThenDecisionForm['decision'], {
                    value : props.ruleSetOp.then_decision,
                    valid : true
                })
            })
        }

        return {
            thenDecisionForm : updatedThenDecisionForm
        };
    }

    inputChangedHandler = (event, controlName) => {
        let updatedControls = updateObject(this.state.thenDecisionForm, {
            [controlName] : updateObject(this.state.thenDecisionForm[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.thenDecisionForm[controlName].validation),
                touched : true
            }) 
        });

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        if(controlName === 'decision_type') {
            const type = event.target.value;
            const fieldToValidate = 'decision';
            updatedControls = validateDecisionFieldAccordingToDecType(updatedControls, type, fieldToValidate);

            if(type === 'compute') {
                this.setState({ decisionCompute : true });
            } else if(type !== 'compute' && this.state.decisionCompute === true) {
                this.setState({ decisionCompute : false });
            }
        }

        const editData = {
            decision_type : updatedControls.decision_type.value,
            then_decision : updatedControls.decision.value
        }

        this.setEditViewMode();
        this.props.onEditOperator(this.props.ruleSetOp.id, editData);

        this.setState({thenDecisionForm : updatedControls, formIsValid : formIsValid});
    }

    handleDragStart = (event) => {
        const start = event.target.id
        this.setState({ draggedItemIndex : start });
    }

    handleDragOver = (event) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'move' ;
    }

    handleDrop = (event, ruleExpression) => {
        const dropId = event.currentTarget.id;
        if(this.state.draggedItemIndex !== dropId) {
            this.setEditViewMode();
            this.props.onReOrderExpOp(this.props.ruleSetOp.id, ruleExpression, (this.state.draggedItemIndex - 1), (dropId - 1));
        }

        this.setState({ draggedItemIndex : null });
    }

    showMathOperatorButton = (ruleExpression) => {
        if(this.props.ruleSetOp[ruleExpression].length > 0 && ruleExpression === 'rule_expression') {
            let lastIndex = this.props.ruleSetOp[ruleExpression].length - 1;
            if(this.props.ruleSetOp[ruleExpression][lastIndex].control_type === 'operator') {
                return ['(', ')'].includes(this.props.ruleSetOp[ruleExpression][lastIndex].displayValue);
            }
            return ['attribute', 'rule'].includes(this.props.ruleSetOp[ruleExpression][lastIndex].control_type);
        }
        return true;
    }

    showConstantButton = (ruleExpression) => {
        if(this.props.ruleSetOp[ruleExpression].length > 0) {
            let lastIndex = this.props.ruleSetOp[ruleExpression].length - 1;
            if(this.props.ruleSetOp[ruleExpression][lastIndex].control_type === 'operator') {
                return this.props.ruleSetOp[ruleExpression][lastIndex].displayValue !== ')';
            }
            return !['attribute', 'rule', 'constant'].includes(this.props.ruleSetOp[ruleExpression][lastIndex].control_type);
        }
        return true;
    }

    showRuleButton = () => {
        if(this.props.ruleSetOp.rule_expression.length > 0) {
            let lastIndex = this.props.ruleSetOp.rule_expression.length - 1;
            if(this.props.ruleSetOp.rule_expression[lastIndex].control_type === 'operator') {
                return this.props.ruleSetOp.rule_expression[lastIndex].displayValue !== ')';
            }
            return false;
        }
        return true;
    }

    showAttributeButton = (ruleExpression) => {
        if(this.props.ruleSetOp[ruleExpression].length > 0) {
            let lastIndex = this.props.ruleSetOp[ruleExpression].length - 1;
            if(this.props.ruleSetOp[ruleExpression][lastIndex].control_type === 'operator') {
                return this.props.ruleSetOp[ruleExpression][lastIndex].displayValue !== ')';
            }
            return this.props.ruleSetOp[ruleExpression][lastIndex].control_type === 'mathOperators';
        }
        return true;
    }

    showElseIfButton = () => {
        const lastElem = this.props.rule_set_exp.length - 1;
        if(this.props.rule_set_exp[lastElem].operator !== '$else') {
            return true;
        }

        return false;
    }

    showElseButton = () => {
        const lastElem = this.props.rule_set_exp.length - 1;
        if(this.props.rule_set_exp[lastElem].operator !== '$else') {
            return true;
        }

        return false;
    }

    setFilteredSuggestions = (expressionId, ruleExpression, value, arrToFilterFrom) => {

        const suggestions = arrToFilterFrom.filter(option => {
            if(value && option.displayValue.indexOf(value) >  -1) {
                return option;
            }
            return false;
        })

        const suggestionArr = suggestions.length > 0 ? suggestions : [];

        const updatedFilteredSuggestions = updateObject(this.state.filteredSuggestions, {
            [ruleExpression] : updateObject(this.state.filteredSuggestions[ruleExpression], {
                [expressionId] : suggestionArr
            })
        });

        this.setState({ filteredSuggestions : updatedFilteredSuggestions});
    }

    ruleInputChangedHandler = (event, expressionId, expressionType, ruleExpression) => {
        if(expressionType === 'attribute') {
            this.setFilteredSuggestions(expressionId, ruleExpression, event.target.value, RULE_ATTRIBUTES);
        } else if(expressionType === 'rule') {
            const params = {
                user_id : this.props.user_id,
                project_id : this.props.match.params.projectId,
                rule_name : event.target.value
            };
            this.props.onGetRuleSuggestions(params, this.props.ruleSetOp.id, expressionId);
        }
    }

    onEditDelControl = (ruleExpression, expressionId, action) => {
        if(action === 'edit') {
            this.setEditViewMode();
            this.props.onEditExpOp(this.props.ruleSetOp.id, expressionId, ruleExpression, {
                value : '',
                displayValue : ''
            });
        } else if(action === 'delete') {
            this.setEditViewMode();
            this.props.onDeleteExpOp(this.props.ruleSetOp.id, expressionId, ruleExpression);
        }

        const updatedShowEditDel = updateObject(this.state.showEditDel, {
            [ruleExpression] :  updateObject(this.state.showEditDel[ruleExpression], {
                [expressionId] : !this.state.showEditDel[ruleExpression][expressionId]
            })
        });

        const updatedFilteredSuggestions = updateObject(this.state.filteredSuggestions, {
            [ruleExpression] :  updateObject(this.state.filteredSuggestions[ruleExpression], {
                [expressionId] : null
            })
        })
        
        this.setState({ showEditDel : updatedShowEditDel, filteredSuggestions : updatedFilteredSuggestions });
    }

    selectListOption = (value, displayValue, expressionId, ruleExpression, attributeType = null, expression_id = null, rule_data = null) => {
        let editObj = {
            value,
            displayValue
        };

        if(attributeType) {
            editObj['attribute_type'] = attributeType;
        }
        
        if(expression_id) {
            editObj['expression_id'] = expression_id;
        }

        if(rule_data) {
            editObj['rule_data'] = rule_data;
        }

        this.props.onEditExpOp(this.props.ruleSetOp.id, expressionId, ruleExpression, editObj);
    }

    onClickLabel = (ruleExpression, expressionId) => {
        const updatedShowEditDel = updateObject(this.state.showEditDel, {
            [ruleExpression] :  updateObject(this.state.showEditDel[ruleExpression], {
                [expressionId] : !this.state.showEditDel[ruleExpression][expressionId]
            })
        });
        
        this.setState({ showEditDel : updatedShowEditDel });
    }

    onEnterConditionValueInput = (event, expressionId, ruleExpression) => {
        if (event.keyCode === 13) {
            this.props.onEditExpOp(this.props.ruleSetOp.id, expressionId, ruleExpression, {
                value : event.target.value,
                displayValue : event.target.value
            });
        }
    }

    addToExpression = (ruleExpression, controlType) => {
        const key = this.props.ruleSetOp[ruleExpression].length + 1;

        const updatedShowEditDel = updateObject(this.state.showEditDel, {
            [ruleExpression] : updateObject(this.state.showEditDel[ruleExpression], {
                [key] : false
            })
        });
    
        this.setState({ showEditDel : updatedShowEditDel })

        const controlObj = {
            control_type : controlType,
            value : '',
            displayValue : ''
        }

        this.setEditViewMode();
        this.props.onAddExpToOp(this.props.ruleSetOp.id, ruleExpression, controlObj);
    }

    addOperatorClick = (opValue) => {
        const operator = {
            operator : opValue,
            displayValue : FLOW_CONTROL_OPS[opValue],
            then_decision : null,
            decision_type : null,
            decision_exp : []
        }

        if(opValue === '$else_if') {
            operator['rule_expression'] = [];
        }

        this.setEditViewMode();

        this.props.onAddOperator(operator);
    }

    setEditViewMode = () => {
        if(this.props.viewMode === 'view') {
            this.props.onSetViewMode('edit');
        }
    }

    render() {
        // console.log('props_rule_set:::', this.props.ruleSetOp);
        // console.log('rule_suggestions:::', this.props.rule_suggestions);

        const isOpElse = this.props.ruleSetOp.operator === '$else';

        const opList = (ruleExpression, expressionId) => {
            const list = Object.keys(OPERATORS).map(key => {
                return <li key={key} onClick={() => this.selectListOption(OPERATORS[key], key, expressionId, ruleExpression)}>{key}</li>;
            });
            return list;
        }

        const getSuggestion = (ruleExpression, expressionId) => {
            if(this.state.filteredSuggestions[ruleExpression] && this.state.filteredSuggestions[ruleExpression][expressionId] && this.state.filteredSuggestions[ruleExpression][expressionId].length > 0)  {
                const list = this.state.filteredSuggestions[ruleExpression][expressionId].map(option => {
                    return <li key={option.id} onClick={() => this.selectListOption(option.value, option.displayValue, expressionId, ruleExpression, option.fieldType)}>{option.displayValue}</li>;
                })
                return list;
            }
            return null;
        }

        const getSuggestionForRule = (ruleExpression, expressionId) => {
            if(this.props.rule_suggestions[this.props.ruleSetOp.id] && this.props.rule_suggestions[this.props.ruleSetOp.id][expressionId] && this.props.rule_suggestions[this.props.ruleSetOp.id][expressionId].length > 0)  {
                const list = this.props.rule_suggestions[this.props.ruleSetOp.id][expressionId].map(option => {
                    const ruleData = {
                        description : option.description,
                        decision_type : option.decision_type,
                        default_decision_type : option.default_decision_type,
                    };
                    return <li key={option.id} onClick={() => this.selectListOption(option.id, option.name, expressionId, ruleExpression, null, null, ruleData)}>{option.name}</li>;
                })
                return list;
            }
            return null;
        }

        const mathOpList = (ruleExpression, expressionId) => {
            const operatorObj = ruleExpression === 'rule_expression' ? RULE_OPS : COMP_OPS;

            const list = Object.keys(operatorObj).map(key => {
                return <li key={key} onClick={() => this.selectListOption(operatorObj[key], key, expressionId, ruleExpression)}>{key}</li>;
            });
            return list;
        }

        const ruleExpression = (ruleExpression) => {
            const dragObj = {
                draggable : true,
                onDragStart : (event) => this.handleDragStart(event),
                onDragOver : (event) => this.handleDragOver(event),
                onDrop : (event) => this.handleDrop(event, ruleExpression),
            }
            const toolTipObj = (exp) => {
                if(exp.control_type === 'rule' && exp.rule_data) {
                    const toolTip = `${exp.rule_data.description}, Returns ${exp.rule_data.decision_type}${exp.rule_data.default_decision_type ? ' or ' + exp.rule_data.default_decision_type : ''}`;

                    return {
                        'data-toggle' : "tooltip",
                        'data-placement' : "bottom",
                        'title' : toolTip
                    }
                }
                return {};
            }

            return this.props.ruleSetOp[ruleExpression].map(expression => {

                let editDelPopOver = this.state.showEditDel[ruleExpression] && this.state.showEditDel[ruleExpression][expression.id] ?
                    (
                        <div className={classes.editDel}>
                            <span className={classes.edit} onClick={() => this.onEditDelControl(ruleExpression, expression.id, 'edit')}>Edit </span>
                            <span className={classes.delete} onClick={() => this.onEditDelControl(ruleExpression, expression.id, 'delete')}>Delete </span>
                        </div>
                    ) : null;
    
                let labelClasses = this.state.showEditDel[ruleExpression] && this.state.showEditDel[ruleExpression][expression.id] ? [classes.active] :  [];

                switch(expression.control_type) {
                    case 'attribute' :
                        labelClasses.push(classes.tableButton);
    
                        return (
                            <div key={expression.id} id={expression.id} {...dragObj} className={["pr-3", classes.tableDiv, classes.poSticky].join(' ')}>
                                {expression.value ? 
                                (<React.Fragment>
                                    <label className={labelClasses.join(' ')} onClick={() => this.onClickLabel(ruleExpression, expression.id)}>{expression.displayValue}</label>
                                    {editDelPopOver}
                                </React.Fragment>) :
                                (<ul className={classes['dropdown-menu']}>
                                    <input type="text" placeholder="Search.." onChange={(event) => this.ruleInputChangedHandler(event, expression.id, expression.control_type, ruleExpression)}/>
                                    <div className={classes.dropDownList}>
                                        {getSuggestion(ruleExpression, expression.id)}
                                    </div>
                                </ul>)}                       
                            </div>
                        );
    
                    case 'rule' :
                        labelClasses.push(classes.tableButton2);
    
                        return (
                            <div key={expression.id} id={expression.id} {...toolTipObj(expression)} {...dragObj} className={["pr-3", classes.tableDiv2, classes.poSticky].join(' ')}>
                                {expression.displayValue ?
                                (<React.Fragment>
                                    <label className={labelClasses.join(' ')} onClick={() => this.onClickLabel(ruleExpression, expression.id)}>{expression.displayValue}</label>
                                    {editDelPopOver}
                                </React.Fragment>) :                                
                                (<ul className={classes['dropdown-menu']}>
                                    <input type="text" placeholder="Rule Name" onChange={(event) => this.ruleInputChangedHandler(event, expression.id, expression.control_type, ruleExpression)} />
                                    <div className={classes.dropDownList}>
                                        {getSuggestionForRule(ruleExpression, expression.id)}
                                    </div>
                                </ul>)}                          
                            </div>                        
                        );
    
                    case 'operator' :
                        labelClasses.push(classes.tableButton3);
    
                        return (
                            <div key={expression.id} id={expression.id} {...dragObj} className={["pr-3", classes.tableDiv3, classes.poSticky].join(' ')}>
                                {expression.value ?
                                    (<React.Fragment>
                                        <label className={labelClasses.join(' ')} onClick={() => this.onClickLabel(ruleExpression, expression.id)}>{expression.displayValue}</label>
                                        {editDelPopOver}
                                    </React.Fragment>) :
                                (<ul className={classes['dropdown-menu']}>
                                    <input type="text" placeholder="Operator" readOnly />
                                    <div className={classes.dropDownList}>
                                        {opList(ruleExpression, expression.id)}
                                    </div>
                                </ul>) }                               
                            </div>
                        );
                    
                    case 'constant' :
                        labelClasses.push(classes.tableButton4);
    
                        return (
                            <div key={expression.id} id={expression.id} {...dragObj} className={["pr-3", classes.tableDiv4, classes.poSticky].join(' ')}>
                                {expression.value ? 
                                    (<React.Fragment>
                                        <label className={labelClasses.join(' ')} onClick={() => this.onClickLabel(ruleExpression, expression.id)}>{expression.value}</label>
                                        {editDelPopOver}
                                    </React.Fragment>) :
                                (<ul className={classes['dropdown-menu']} style={{border : '0px'}}>
                                    <input type="text" placeholder="Value" style={{ border : '1px solid #C32626'}} onKeyDown={(event) => this.onEnterConditionValueInput(event, expression.id, ruleExpression)} />
                                </ul>)}                                
                            </div>
                        );
    
                    case 'mathOperators' :
                        labelClasses.push(classes.tableButton5);
    
                        return (
                            <div key={expression.id} id={expression.id} {...dragObj} className={["pr-3", classes.tableDiv5, classes.poSticky].join(' ')}>
                                {expression.value ? 
                                (<React.Fragment>
                                    <label className={labelClasses.join(' ')} onClick={() => this.onClickLabel(ruleExpression, expression.id)}>{expression.displayValue}</label>
                                    {editDelPopOver}
                                </React.Fragment>) :                                
                                (<ul className={classes['dropdown-menu']}>
                                    <input type="text" placeholder="Operator" readOnly />
                                    <div className={classes.dropDownList}>
                                        {mathOpList(ruleExpression, expression.id)}
                                    </div>
                                </ul>)}                          
                            </div>                        
                        );
    
                    default : return null;
                }
            })
        }

        const computationalDecisionField = (rule_expression) => {
            return (
                <React.Fragment>
                    <div className={["col-10", "mt-4", "d-flex", classes.ruleParam, "pl-0", "pr-0"].join(' ')}>
                        <h6 style={{ marginLeft : '5px', fontWeight : '600'}}>
                            Decision
                        </h6>
                        <div className={["ml-auto"].join(' ')}>
                            {
                                this.showAttributeButton(rule_expression) ?
                                (
                                    <button  className={[classes.btn, classes.expression, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression(rule_expression, 'attribute')}>
                                        Attributes
                                    </button>
                                ) : null
                            }
                            {
                                this.showConstantButton(rule_expression) ?
                                (
                                    <button className={[classes.btn, classes.constant, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression(rule_expression, 'constant')}>
                                        Constant
                                    </button>
                                ) : null
                            }
                            {
                                this.showMathOperatorButton(rule_expression) ? 
                                    (
                                        <button className={[classes.btn, classes.mathOperators, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression(rule_expression, 'mathOperators')}>
                                            Operators
                                        </button>
                                    ) : null
                            }
                        </div>
                    </div>
                    <div className={["col-10", "mt-4", classes["bg-light"], "pt-3"].join(' ')}>
                        <div  className={[classes.dropdown,  "mt-2", "d-inline-flex", "flex-wrap"].join(' ')}>
                            {ruleExpression(rule_expression)}
                        </div>
                    </div>
                </React.Fragment>
            );
        }

        const removeOpToolTip = {
            'data-toggle' : "tooltip",
            'data-placement' : "bottom",
            'title' : 'Remove Operator'
        }

        return (
            <div style={{marginLeft : '5px', marginRight : '15px'}} className={["row"].join(' ')}>
                {
                    this.props.ruleSetOp.operator !== '$else' ?
                    (
                        <React.Fragment>
                            <div className={["col-10", "mt-4", "d-flex", classes.ruleParam, "pl-0", "pr-0"].join(' ')}>
                                {
                                    this.props.ruleSetOp.operator === '$if' ?
                                        (   
                                            <React.Fragment>
                                                <h6 style={{ marginTop : '3px'}}><strong>ADD</strong></h6>
                                                <div className={["mr-auto"].join(' ')}>
                                                    {
                                                        this.showElseIfButton() ?
                                                            (
                                                                <button className={[classes['btn-new'], "ml-4"].join(' ')} type="button" onClick={() => this.addOperatorClick('$else_if')}>
                                                                    <strong>Else If</strong>
                                                                </button>
                                                            ) : null

                                                    }
                                                    {
                                                        this.showElseButton() ?
                                                            (
                                                                <button className={[classes['btn-new'], "ml-3"].join(' ')} type="button" onClick={() => this.addOperatorClick('$else')}>
                                                                    <strong>Else</strong>
                                                                </button>
                                                            ) : null

                                                    }
                                                </div>
                                            </React.Fragment>
                                        ) : null
                                }
                                <div className={["ml-auto"].join(' ')}>
                                    {
                                        this.showRuleButton() ?
                                            (
                                                <button className={[classes.btn, classes.conditions, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression('rule_expression', 'rule')}>
                                                    Rule
                                                </button>
                                            ) : null
                                    }
                                    {
                                        this.showConstantButton('rule_expression') ?
                                        (
                                            <button className={[classes.btn, classes.constant, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression('rule_expression', 'constant')}>
                                                Constant
                                            </button>
                                        ) : null
                                    }
                                    {
                                        this.showMathOperatorButton('rule_expression') ?
                                            (
                                                <button className={[classes.btn, classes.mathOperators, "mr-2"].join(' ')} type="button" onClick={() => this.addToExpression('rule_expression', 'mathOperators')}>
                                                    Operators
                                                </button>
                                            ) : null
                                    }
                                    <button  className={[classes.btn, classes.operator].join(' ')} type="button" onClick={() => this.addToExpression('rule_expression', 'operator')}>Logical Operators</button>
                                </div>
                            </div>
                            <div className={["col-10", "mt-4", classes["bg-light"], "pt-3"].join(' ')}>
                                <div  className={[classes.dropdown,  "mt-2", "d-inline-flex", "flex-wrap"].join(' ')}>
                                    {
                                        this.props.ruleSetOp.operator === '$else_if' ?
                                            <span className={["col-1", "mr-2"].join(' ')}>
                                                <img {...removeOpToolTip} style={{marginRight : '20px', marginBottom : '4px', cursor : 'pointer'}} src={CloseIcon} alt="closeIcon" onClick={() => this.props.onOperatorDelete(this.props.ruleSetOp.id)}/>
                                            </span> : null
                                    }
                                    <span style={{marginLeft : '10px', marginRight : '30px'}}><strong>{this.props.ruleSetOp.displayValue}</strong></span>
                                    {ruleExpression('rule_expression')}
                                </div>
                            </div>
                        </React.Fragment>
                    ) : null
                }
                <Form.Row style={{marginLeft : '1px'}} className={["col-10", "mt-4", classes["bg-light-new"], "pt-3"].join(' ')}>
                    <Col className={["col-2"].join(' ')}>
                        {
                            isOpElse ? 
                                <img {...removeOpToolTip} style={{marginRight : '20px', marginBottom : '4px', cursor : 'pointer'}} src={CloseIcon} alt="closeIcon" onClick={() => this.props.onOperatorDelete(this.props.ruleSetOp.id)}/>
                                : null
                        }
                        <Form.Label className={classes.FormLabelNew}>{isOpElse ? 'ELSE' : 'THEN'}</Form.Label>
                    </Col>
                    <Col  className={["col-2"].join(' ')}>
                        <FormControl config={this.state.thenDecisionForm['decision_type']} changed={this.inputChangedHandler} />
                    </Col>
                    {
                        !this.state.decisionCompute ? 
                            <Col className={["col-3", "ml-3"].join(' ')}>
                                <FormControl config={this.state.thenDecisionForm['decision']} changed={this.inputChangedHandler} />
                            </Col> : null
                    }
                </Form.Row>
                {
                    this.state.decisionCompute ? computationalDecisionField('decision_exp') : null
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_id : state.auth.userData.user_id,
        viewMode : state.ruleSet.viewMode,
        rule_set_exp : state.ruleSet.rule_set_exp,
        rule_suggestions : state.ruleSet.rule_suggestions,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddOperator : (operator) => dispatch(actions.addOpToRuleSet(operator)),
        onEditOperator : (opId, data) => dispatch(actions.editOpRuleSet(opId, data)),
        onSetViewMode : (mode) => dispatch(actions.setRuleSetViewMode(mode)),
        onAddExpToOp : (opId, exp_name, expression) => dispatch(actions.addExpToRuleSetOp(opId, exp_name, expression)),
        onEditExpOp : (opId, expId, exp_name, expression) => dispatch(actions.editExpRuleSetOp(opId, expId, exp_name, expression)),
        onDeleteExpOp : (opId, expId, exp_name) => dispatch(actions.deleteExpRuleSetOp(opId, expId, exp_name)),
        onReOrderExpOp : (opId, exp_name, start, end) => dispatch(actions.reorderExpRuleSetOp(opId, exp_name, start, end)),
        onGetRuleSuggestions : (params, opId, expId) => dispatch(actions.getRuleSuggestions(params, opId, expId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Expression);