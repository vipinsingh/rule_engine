import React, { Component } from "react";
import { connect } from "react-redux";
import classes from './RuleSetList.module.css';
import Spinner from "../../../../components/UI/Spinner/Spinner";
import * as actions from '../../../../store/actions/index';
import { Button } from "react-bootstrap";
import Table from '../../../../components/Table/Table';
import Pagination from "../../../../components/Pagination/Pagination";
import { toast } from "react-toastify";

class RuleSetList extends Component {

    state = {
        perPage : 15,
        pageNo : 1,
        tillPage : 5,
        maxPageItems : 5
    }

    handlePageSelect = (number) => {
        this.setState({ pageNo : number });
        
        this.props.onListRuleSet(this.getListRuleSetParams(number));
    }

    getListRuleSetParams = (pageNo = null) => {
        return {
            user_id : this.props.user_id,
            project_id : this.props.match.params.projectId,
            page_no : pageNo ? pageNo : this.state.pageNo,
            per_page : this.state.perPage
        }
    }

    handlePrevNext = (action) => {
        let tillPage = null;
        if(action === 'prev') {
            tillPage = this.state.tillPage - this.state.maxPageItems;
        } else if(action === 'next') {
            tillPage = this.state.tillPage + this.state.maxPageItems;
        }

        this.setState({ tillPage : tillPage })
    }

    componentDidUpdate(prevProps) {
        if(this.props.error) {
            toast.error('An error occured!');
            this.props.onResetError();
        }

        if(this.props.match.params.projectId !== prevProps.match.params.projectId) {
            this.props.onListRuleSet(this.getListRuleSetParams());
        }
    }

    componentDidMount() {
        this.props.onListRuleSet(this.getListRuleSetParams());
    }

    createRuleSet = () => {
        this.props.history.push(`${this.props.match.url}/create`);
    }

    viewRuleSet = (id) => {
        this.props.history.push(`${this.props.match.url}/${id}`);
    }

    render() {
        const spinner = this.props.loading ? <Spinner /> : null;

        return (
            <div className={classes.RuleSetList}>
                {spinner}
                <h2>Rule Sets</h2>
                <Button variant="primary" className={classes.buttonPadding} onClick={this.createRuleSet}>Create Rule Set</Button>
                <Table
                    tableView='rule set'
                    tableData={this.props.rule_set_list ? this.props.rule_set_list.rule_set : null}
                    createClicked={this.createRuleSet}
                    rowClicked={this.viewRuleSet}
                    error={this.props.error}
                />
                <Pagination
                    recordCount={this.props.rule_set_list && this.props.rule_set_list.count ? this.props.rule_set_list.count : 0}
                    perPage={this.state.perPage}
                    pageNo={this.state.pageNo}
                    tillPage={this.state.tillPage}
                    maxPageItems={this.state.maxPageItems}
                    pageSelectHandler={this.handlePageSelect}
                    prevNextHandler={this.handlePrevNext}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_id : state.auth.userData.user_id,
        loading : state.ruleSet.loading,
        error : state.ruleSet.error,
        rule_set_list : state.ruleSet.rule_set_list,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onResetError : () => dispatch(actions.resetRuleSetError()),
        onListRuleSet : (params) => dispatch(actions.listRuleSet(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RuleSetList);