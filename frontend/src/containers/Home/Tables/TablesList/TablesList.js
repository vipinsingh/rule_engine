import React, { Component } from "react";
import classes from './TablesList.module.css';
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import Table from '../../../../components/Table/Table';
import * as actions from '../../../../store/actions/index';
import Spinner from "../../../../components/UI/Spinner/Spinner";
import Pagination from "../../../../components/Pagination/Pagination";
import { toast } from "react-toastify";

class TablesList extends Component {

    state = {
        perPage : 15,
        pageNo : 1,
        tillPage : 5,
        maxPageItems : 5
    }

    createTable = () => {
        this.props.history.push(`${this.props.match.url}/create`);
    }

    viewTableDetails = (id) => {
        this.props.history.push(`${this.props.match.url}/${id}`);
    }

    handlePageSelect = (number) => {
        this.setState({ pageNo : number });
        this.props.onListTables(this.props.user_id, this.props.match.params.projectId, number, this.state.perPage);
    }

    handlePrevNext = (action) => {
        let tillPage = null;
        if(action === 'prev') {
            tillPage = this.state.tillPage - this.state.maxPageItems;
        } else if(action === 'next') {
            tillPage = this.state.tillPage + this.state.maxPageItems;
        }

        this.setState({ tillPage : tillPage })
    }

    componentDidUpdate(prevProps) {
        if(this.props.error) {
            toast.error('An error occured!');
            this.props.onErrorReset();
        }

        if(this.props.match.params.projectId !== prevProps.match.params.projectId) {
            this.props.onListTables(this.props.user_id, this.props.match.params.projectId);
        }
    }

    componentDidMount() {
        this.props.onListTables(this.props.user_id, this.props.match.params.projectId);
    }

    render() {
        const spinner = this.props.loading ? <Spinner /> : null;

        return (
            <div className={classes.TablesList}>
                {spinner}
                <h2>Tables</h2>
                <Button variant="primary" className={classes.buttonPadding} onClick={this.createTable}>Create Table</Button>
                <Table
                    tableView='table'
                    tableData={this.props.tables ? this.props.tables.tables : null}
                    createClicked={this.createTable}
                    rowClicked={this.viewTableDetails}
                    error={this.props.error}
                />
                <Pagination 
                    recordCount={this.props.tables && this.props.tables.count ? this.props.tables.count : 0}
                    perPage={this.state.perPage}
                    pageNo={this.state.pageNo}
                    tillPage={this.state.tillPage}
                    maxPageItems={this.state.maxPageItems}
                    pageSelectHandler={this.handlePageSelect}
                    prevNextHandler={this.handlePrevNext}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        tables : state.decTable.tables,
        loading : state.decTable.loading,
        error : state.decTable.error,
        user_id : state.auth.userData.user_id
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onListTables : (user_id, project_id, pageNo = 1, perPage = 15) => dispatch(actions.listTables(user_id, project_id, pageNo, perPage)),
        onErrorReset : () =>  dispatch(actions.errorReset())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TablesList);