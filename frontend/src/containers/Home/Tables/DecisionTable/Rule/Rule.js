import React, { Component } from "react";
import HamburgerButton from "../../../../../components/UI/HamburgerButton/HamburgerButton";
import FormControl from "../../../../../components/DynamicForm/FormControl/FormControl";
import { addRuleForm, CONDITIONS, VALIDATIONS } from "../createTableFormConfigs";
import { updateObject, checkValidity } from "../../../../../shared/utility";
import classes from './Rule.module.css';
import { connect } from "react-redux";
import * as actions from '../../../../../store/actions/index';
import CopyIcon from '../../../../../assets/images/icons-copy.png';
import CloseIcon from '../../../../../assets/images/icons-close.png';

class Rule extends Component {

    state = {
        addRuleForm : addRuleForm,
        formIsValid : false,
        updateFieldValues : false
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.addRuleForm, {
            [controlName] : updateObject(this.state.addRuleForm[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.addRuleForm[controlName].validation),
                touched : true
            }) 
        });

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        const editedRule = this.props.rules.find(rule => rule.id === this.props.ruleId);

        if(editedRule[controlName] !== event.target.value) {
            const editedData = {
                [controlName] : event.target.value
            };
    
            this.props.onEditRule(this.props.ruleId, editedData);
        }

        if(this.props.viewMode === 'view') {
            this.props.onSetViewMode('edit');
        }

        this.setState({addRuleForm : updatedControls, formIsValid : formIsValid});
    }

    setValidationForField(controls, field, validationObj, update = true) {
        let fieldValidation = null;

        if(update === true) {
            fieldValidation = updateObject(controls[field].validation, validationObj);
        } else {
            fieldValidation = validationObj;
        }

        controls = updateObject(controls, {
            [field] : updateObject(controls[field], {
                validation  : fieldValidation,
                valid : checkValidity(controls[field].value, fieldValidation)
            }) 
        });

        return controls;
    }

    setDecisionTextArea(controls, undo = false) {
        let configObj = {
            type : 'text'
        }

        if(!undo) {
            configObj = {
                type : 'textarea',
                rows : 3
            }
        }
        
        controls = updateObject(controls, {
            'decision' : updateObject(controls['decision'], {
                elementConfig : updateObject(controls['decision'].elementConfig, configObj)
            }) 
        });
        return controls;
    }

    componentDidUpdate(prevProps, prevState) {

        const current_rule = this.props.rules.find(rule => rule.id === this.props.ruleId);

        if(current_rule.title !== this.state.addRuleForm.title.value ||
            current_rule.description !== this.state.addRuleForm.description.value ||
            current_rule.decision !== this.state.addRuleForm.decision.value) {

                let updatedControls = this.state.addRuleForm;

                Object.keys(this.state.addRuleForm).forEach(control => {
                    updatedControls = updateObject(updatedControls, {
                        [control] : updateObject(updatedControls[control], {
                            value : current_rule[control] || ''
                        })
                    })
                })
                this.setState({ addRuleForm : updatedControls });
        }

        if(this.props.tableType === 'scoring' && !prevState.addRuleForm.decision.validation.isNumericPositive) {
            let updatedControls = prevState.addRuleForm;
            updatedControls = this.setValidationForField(updatedControls, 'decision', { isNumericPositive : true });
            this.setState({ addRuleForm : updatedControls });
        } else if(this.props.tableType !== 'scoring' && prevState.addRuleForm.decision.validation.isNumericPositive) {
            let updatedControls = prevState.addRuleForm;
            updatedControls = this.setValidationForField(updatedControls, 'decision', { isNumericPositive : false });
            this.setState({ addRuleForm : updatedControls });
        }

        if(this.props.tableType !== 'scoring' && this.props.decisionType !== prevProps.decisionType) {
            let updatedControls = prevState.addRuleForm;
            if(updatedControls['decision'].elementConfig.type === 'textarea' && this.props.decisionType !== 'json') {
                updatedControls = this.setDecisionTextArea(updatedControls, true);
            }
            switch(this.props.decisionType) {
                case 'string' :
                    updatedControls = this.setValidationForField(updatedControls, 'decision', VALIDATIONS['string'], false);
                    break;
                case 'number' :
                    updatedControls = this.setValidationForField(updatedControls, 'decision', VALIDATIONS['number'], false);
                    break;
                case 'alpha_num' :
                    updatedControls = this.setValidationForField(updatedControls, 'decision', VALIDATIONS['alpha_num'], false);
                    break;
                case 'json' :
                    updatedControls = this.setValidationForField(updatedControls, 'decision', VALIDATIONS['json'], false);
                    updatedControls = this.setDecisionTextArea(updatedControls);
                    break;
                default :
                    updatedControls = this.setValidationForField(updatedControls, 'decision', false, false);
                    break;
            }
            this.setState({ addRuleForm : updatedControls });
        }

        if(this.state.updateFieldValues !== prevState.updateFieldValues && this.state.updateFieldValues === true) {
            const currentRule = this.props.rules.find(rule => rule.id === this.props.ruleId);
            let updatedControls = this.state.addRuleForm;

            Object.keys(this.state.addRuleForm).forEach(control => {
                updatedControls = updateObject(updatedControls, {
                    [control] : updateObject(updatedControls[control], {
                        value : currentRule[control] || ''
                    })
                })
            })
            this.setState({ addRuleForm : updatedControls });
        }
    }

    componentDidMount() {
        const currentRule = this.props.rules.find(rule => rule.id === this.props.ruleId);
        
        if(currentRule.title || currentRule.description || currentRule.decision) {
            this.setState({ updateFieldValues : true });
        }
    }

    onClickCopyRule = () => {
        this.props.onCopyRule(this.props.ruleId);
        if(this.props.viewMode === 'view') {
            this.props.onSetViewMode('edit');
        }
    }

    render() {
        const conditionInputs = this.props.fields.map(field => {

            const conditionForField = this.props.conditions.find(condition => condition.field_id === field.id);
            if(!conditionForField) {
                return (
                    <td key={field.id} onClick={() => this.props.onAddCondition(field, this.props.ruleId)}></td>
                );
            } else {
                return (
                    <td key={field.id} onClick={() => this.props.onEditCondition(field, conditionForField)}>
                        {CONDITIONS[conditionForField['condition']].displayValue} {conditionForField.value}
                    </td>
                );
            }
        });
        
        const copyToolTip = {
            'data-toggle' : "tooltip",
            'data-placement' : "bottom",
            'title' : 'Duplicate Rule'
        };

        const removeTooltip = {
            'data-toggle' : "tooltip",
            'data-placement' : "bottom",
            'title' : 'Delete Rule'
        };

        return (
            <React.Fragment>
                {this.state.showConditionModal ? this.state.conditionModal : null}
                <tr className={classes.Rule}>
                    <td><HamburgerButton /></td>
                    <td>
                        <FormControl config={this.state.addRuleForm['title']} changed={this.inputChangedHandler}></FormControl>
                        <FormControl config={this.state.addRuleForm['description']} changed={this.inputChangedHandler}></FormControl>
                    </td>
                    {conditionInputs}
                    <td></td>
                    <td><FormControl config={this.state.addRuleForm['decision']} changed={this.inputChangedHandler}></FormControl></td>
                    <td>
                        <img {...copyToolTip} src={CopyIcon} style={{cursor : 'pointer'}} alt="copyIcon" onClick={this.onClickCopyRule}/>
                        <img {...removeTooltip} style={{marginLeft : '20px', cursor : 'pointer'}} src={CloseIcon} alt="closeIcon" onClick={() => this.props.onRuleDelete(this.props.ruleId)}/>
                    </td>
                </tr>
            </React.Fragment>
        );

    }

}

const mapStateToProps = state => {
    return {
        rules : state.decTable.rules,
        viewMode : state.decTable.viewMode
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onEditRule : (ruleId, editedData) => dispatch(actions.editRule(ruleId, editedData)),
        onCopyRule : (ruleId) => dispatch(actions.copyRule(ruleId)),
        onSetViewMode : (mode) => dispatch(actions.setViewMode(mode))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Rule);