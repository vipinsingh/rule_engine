import React, { Component } from "react";
import { optionsByDataType, conditionForm } from "../../createTableFormConfigs";
import { updateObject, checkValidity } from "../../../../../../shared/utility";
import classes from './Condition.module.css';
import Modal from "../../../../../../components/UI/Modal/Modal";
import DynamicForm from '../../../../../../components/DynamicForm/DynamicForm';
import { connect } from "react-redux";
import * as actions from '../../../../../../store/actions/index';

class Condition extends Component {

    state = {
        conditionForm :  conditionForm,
        formIsValid : false,
        showModal : true
    }

    modalCloseHandler = () => {
        this.setState({showModal : false});
        this.props.close();
    }

    modalSubmitHandler = (event) => {
        event.preventDefault();
        let conditionIn = this.state.conditionForm.conditionInput.value;
        let conditionVal = this.state.conditionForm.conditionValue.value;

        if(['$true', '$false'].includes(conditionIn)) {
            conditionVal = conditionIn === '$true' ? 'true' : 'false';
            conditionIn = '$eq';
        }

        const conditionData = {
            rule_id    : this.props.ruleId,
            field_id   : this.props.fieldId,
            field_name : this.props.fieldName,
            condition  : conditionIn,
            value      : conditionVal
        };
        
        if(this.props.condition) {
            this.props.onEditCondition(this.props.ruleId, conditionData);
        } else {
            this.props.onAddCondition(this.props.ruleId, conditionData);
        }

        if(this.props.viewMode === 'view') {
            this.props.onSetViewMode('edit');
        }

        this.props.close();
    }

    componentDidMount() {
        const selectOptions = optionsByDataType[this.props.fieldType];
        let updatedControls = updateObject(this.state.conditionForm, {
            conditionInput : updateObject(this.state.conditionForm.conditionInput, {
                elementConfig : updateObject(this.state.conditionForm.conditionInput.elementConfig, {
                    options : selectOptions
                })
            })
        });

        let formIsValid = true;

        if(this.props.condition) {
            let conditionIn = this.props.condition.condition;
            let conditionVal = this.props.condition.value;

            if(conditionIn === '$eq' && ['true', 'false'].includes(conditionVal)) {
                conditionIn = '$' + conditionVal;
                conditionVal = '';
            }

            updatedControls = updateObject(updatedControls, {
                conditionInput : updateObject(updatedControls.conditionInput, {
                    value : conditionIn,
                    valid : checkValidity(conditionIn, updatedControls.conditionInput.validation),
                    touched : true
                }),
                conditionValue : updateObject(updatedControls.conditionValue, {
                    value : conditionVal,
                    valid : checkValidity(conditionVal, updatedControls.conditionValue.validation),
                    touched : true
                })
            });

            if(['$is_set', '$is_null'].includes(conditionIn)) {
                updatedControls = updateObject(updatedControls, {
                    conditionValue : updateObject(updatedControls.conditionValue, {
                        elementType : 'hidden',
                        valid : true
                    })
                });
            }

            if(this.props.fieldType === 'number' && ['$in', '$not_in', '$between'].includes(conditionIn)) {
                updatedControls = updateObject(updatedControls, {
                    conditionValue : updateObject(updatedControls.conditionValue, {
                        validation : updateObject(updatedControls.conditionValue.validation, {
                            isNumericComma : true,
                            isNumeric : false
                        })
                    })
                });
            }
        }

        if(this.props.fieldType === 'boolean') {
            updatedControls = updateObject(updatedControls, {
                conditionValue : updateObject(updatedControls.conditionValue, {
                    elementType : 'hidden',
                    valid : true
                })
            });
        }

        if(this.props.fieldType === 'number' && !this.props.condition) {
            updatedControls = updateObject(updatedControls, {
                conditionValue : updateObject(updatedControls.conditionValue, {
                    validation : updateObject(updatedControls.conditionValue.validation, {
                        isNumeric : true
                    })
                })
            });
        }

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }
        
        this.setState({ conditionForm : updatedControls, formIsValid : formIsValid });
    }

    inputChangedHandler = (event, controlName) => {
        let updatedControls = updateObject(this.state.conditionForm, {
            [controlName] : updateObject(this.state.conditionForm[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.conditionForm[controlName].validation),
                touched : true
            }) 
        });

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        if(controlName === 'conditionInput' && ['$is_set', '$is_null'].includes(event.target.value)) {
            updatedControls = updateObject(updatedControls, {
                conditionValue : updateObject(updatedControls.conditionValue, {
                    elementType : 'hidden',
                    value : ''
                })
            });

            formIsValid = true;
        } else if(controlName === 'conditionInput' && !['$is_set', '$is_null'].includes(event.target.value) && this.props.fieldType !== 'boolean') {
            updatedControls = updateObject(updatedControls, {
                conditionValue : updateObject(updatedControls.conditionValue, {
                    elementType : 'input'
                })
            });
        }
        
        if(controlName === 'conditionInput' && ['$in', '$not_in', '$between'].includes(event.target.value) && this.props.fieldType === 'number') {
            let validationObj = updateObject(updatedControls.conditionValue.validation, {
                isNumericComma : true,
                isNumeric : false
            });
            updatedControls = updateObject(updatedControls, {
                conditionValue : updateObject(updatedControls.conditionValue, {
                    validation : validationObj,
                    valid : checkValidity(updatedControls.conditionValue.value, validationObj)
                })
            });
        }

        if(controlName === 'conditionInput' && !['$in', '$not_in', '$between'].includes(event.target.value) && this.props.fieldType === 'number') {
            let validationObj = updateObject(updatedControls.conditionValue.validation, {
                isNumericComma : false,
                isNumeric : true
            });
            updatedControls = updateObject(updatedControls, {
                conditionValue : updateObject(updatedControls.conditionValue, {
                    validation : validationObj,
                    valid : checkValidity(updatedControls.conditionValue.value, validationObj)
                })
            });
        }

        this.setState({conditionForm : updatedControls, formIsValid : formIsValid});
    }

    render() {
        let dynamicForm = null;
        
        if(this.state.conditionForm.conditionInput.elementConfig.options) {
            dynamicForm = <DynamicForm changed={this.inputChangedHandler} valid={this.state.formIsValid} controls={this.state.conditionForm}/>;
        }
        
        return (
            <div className={classes.Condition}>
                <Modal
                    show={this.state.showModal}
                    handleClose={this.modalCloseHandler}
                    modalTitle="Add Condition"
                    submitDisable={!this.state.formIsValid}
                    handleSubmit={this.modalSubmitHandler}
                >
                    <form>
                        {dynamicForm}
                    </form>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        viewMode : state.decTable.viewMode
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddCondition : (ruleId, conditionData) => dispatch(actions.addCondition(ruleId, conditionData)),
        onEditCondition : (ruleId, conditionData) => dispatch(actions.editCondition(ruleId, conditionData)),
        onSetViewMode : (mode) => dispatch(actions.setViewMode(mode))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Condition);