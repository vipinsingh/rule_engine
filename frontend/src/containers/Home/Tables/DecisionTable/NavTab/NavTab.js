import React, { Component } from "react";
import { Tabs, Tab } from "react-bootstrap";
import classes from './NavTab.module.css';

class NavTab extends Component {

    state = {
        key : 'edit'
    }

    handleSelect = (key) => {
        this.setState({ key : key });
        
        switch(key) {
            case 'debug' :
                this.props.history.push(`/projects/${this.props.match.params.projectId}/tables/${this.props.match.params.tableId}/debug`);
                break;
            case 'history' :
                this.props.history.push(`/projects/${this.props.match.params.projectId}/tables/${this.props.match.params.tableId}/history`);
                break;
            default :
                this.props.history.push(`/projects/${this.props.match.params.projectId}/tables/${this.props.match.params.tableId}`);
                break;
        }
    }

    componentDidMount() {
        //last segment of  URL
        let key = this.props.match.url.substring(this.props.match.url.lastIndexOf('/') + 1);

        if(this.props.match.params.decId) {
            key = 'history';
        } else {
            switch(key) {
                case 'debug' : break;
                case 'history' : break;
                default :  key = 'edit'; break;
            }
        }

        this.setState({ key : key });
    }

    render() {

        return (
            <Tabs
                className={classes.NavTab}
                id="controlled-tab-example"
                activeKey={this.state.key}
                onSelect={(k) => this.handleSelect(k)}
            >
                <Tab eventKey="edit" title="Edit" />
                <Tab eventKey="debug" title="Debugger" />
                <Tab eventKey="history" title="History" />
            </Tabs>
        );
    }
}

export default NavTab;