export const createTableForm = {
    name :  {
        elementConfig : {
            type : 'text',
            name : 'name',
            placeholder : 'Table Name',
            className : 'InputWithBottom',
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 100
        },
        valid : false,
        touched : false
    },
    description :  {
        elementConfig : {
            type : 'text',
            name : 'description',
            placeholder : 'Description',
            className : 'InputWithBottom',
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 200
        },
        valid : false,
        touched : false
    },
    default_title :  {
        elementConfig : {
            type : 'text',
            name : 'default_title',
            placeholder : 'Default Title',
            className : 'DefaultDecision',
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 200
        },
        valid : false,
        touched : false
    },
    default_description :  {
        elementConfig : {
            type : 'text',
            name : 'default_description',
            placeholder : 'Default Description',
            className : 'DefaultDecision',
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 200
        },
        valid : false,
        touched : false
    },
    default_decision :  {
        elementConfig : {
            type : 'text',
            name : 'default_decision',
            placeholder : 'Default Decision',
            className : 'DefaultDecision',
        },
        value : '',
        validation : {
            required : true
        },
        valid : false,
        touched : false
    },
    decision_type :  {
        elementConfig : {
            type : 'select',
            name : 'decision_type',
            className : 'DefaultDecision',
            options : [
                {
                    value : '',
                    displayValue : 'Decision Type'
                },
                {
                    value : 'string',
                    displayValue : 'String'
                },
                {
                    value : 'alpha_num',
                    displayValue : 'AlphaNumeric'
                },
                {
                    value : 'number',
                    displayValue : 'Number'
                },
                {
                    value : 'json',
                    displayValue : 'JSON'
                }
            ]
        },
        value : '',
        validation : false,
        valid : false,
        touched : false
    },
    table_type :  {
        elementConfig : {
            type : 'select',
            name : 'table_type',
            className : 'DefaultDecision',
            options : [
                {
                    value : '',
                    displayValue : 'Table Type'
                },
                {
                    value : 'decision',
                    displayValue : 'Decision'
                },
                {
                    value : 'scoring',
                    displayValue : 'Scoring'
                }
            ]
        },
        value : '',
        validation : false,
        valid : false,
        touched : false
    }
};

export const VALIDATIONS = {
    'string' : {
        required : true,
        maxLength : 200
    },
    'alpha_num' : {
        required : true,
        isAlphaNum : true,
        maxLength : 200
    },
    'number' : {
        required : true,
        isNumeric : true
    },
    'json' : {
        required : true,
        isJson : true
    }
}

export const addFieldModalForm = {
    title :  {
        elementType : 'input',
        elementConfig : {
            type : 'text',
            placeholder : 'Title'
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 50
        },
        valid : false,
        touched : false,
        label : 'Title'
    },
    api_key : {
        elementType : 'input',
        elementConfig : {
            type : 'text',
            placeholder : 'Api Field Key'
        },
        value : '',
        validation : {
            required : true,
            minLength : 2,
            maxLength : 50,
            isApiKey : true
        },
        valid : false,
        touched : false,
        label : 'Api Field Key'
    },
    fieldType : {
        elementType : 'select',
        elementConfig : {
            options  : [
                {
                    value : 'string',
                    displayValue : 'String'
                },
                {
                    value : 'number',
                    displayValue : 'Number'
                },
                {
                    value : 'boolean',
                    displayValue : 'Boolean'
                }
            ]
        },
        value : 'string',
        validation : false,
        valid : true,
        label : 'Type'
    }
};

export const CONDITIONS = {
    'blank' : {
        value : '',
        displayValue : 'Please select'
    },
    '$eq' : {
        value : '$eq',
        displayValue : '='
    },
    '$not_eq' : {
        value : '$not_eq',
        displayValue : '!='
    },
    '$in' : {
        value : '$in',
        displayValue : 'in'
    },
    '$not_in' : {
        value : '$not_in',
        displayValue : 'not in'
    },
    '$is_set' : {
        value : '$is_set',
        displayValue : 'is set'
    },
    '$is_null' : {
        value : '$is_null',
        displayValue : 'is null'
    },
    '$true' : {
        value : '$true',
        displayValue : 'true'
    },
    '$false' : {
        value : '$false',
        displayValue : 'false'
    },
    '$gt' : {
        value : '$gt',
        displayValue : '>'
    },
    '$gte' : {
        value : '$gte',
        displayValue : '>='
    },
    '$lt' : {
        value : '$lt',
        displayValue : '<'
    },
    '$lte' : {
        value : '$lte',
        displayValue : '<='
    },
    '$between' : {
        value : '$between',
        displayValue : 'between'
    }
}

export const addRuleForm = {
    title :  {
        elementConfig : {
            type : 'text',
            name : 'title',
            placeholder : 'Title',
            className : 'RuleTitleInput',
        },
        value : '',
        validation : false,
        valid : false,
        touched : false
    },
    description :  {
        elementConfig : {
            type : 'text',
            name : 'description',
            placeholder : 'Description',
            className : 'RuleTitleInput',
        },
        value : '',
        validation : false,
        valid : false,
        touched : false
    },
    decision :  {
        elementConfig : {
            type : 'text',
            name : 'decision',
            placeholder : 'Decision',
            className : 'RuleDecisionInput',
        },
        value : '',
        validation : {
            required : true
        },
        valid : false,
        touched : false
    }
};

export const conditionForm = {
    conditionInput :  {
        elementType : 'select',
        elementConfig : {},
        value : '',
        validation : false,
        valid : false,
        touched : false,
        label : 'Condition'
    },
    conditionValue :  {
        elementType : 'input',
        elementConfig : {
            type : 'text',
            placeholder : 'Value',
        },
        value : '',
        validation : {
            required : true
        },
        valid : false,
        touched : false,
        label :  'Value'
    }
}

export const optionsByDataType = {
    string : [
        CONDITIONS['blank'],
        CONDITIONS['$eq'],
        CONDITIONS['$not_eq'],
        CONDITIONS['$in'],
        CONDITIONS['$not_in'],
        CONDITIONS['$is_set'],
        CONDITIONS['$is_null']
    ],
    number : [
        CONDITIONS['blank'],
        CONDITIONS['$eq'],
        CONDITIONS['$not_eq'],
        CONDITIONS['$in'],
        CONDITIONS['$not_in'],
        CONDITIONS['$gt'],
        CONDITIONS['$gte'],
        CONDITIONS['$lt'],
        CONDITIONS['$lte'],
        CONDITIONS['$between'],
        CONDITIONS['$is_set'],
        CONDITIONS['$is_null']
    ],
    boolean : [
        CONDITIONS['blank'],
        CONDITIONS['$true'],
        CONDITIONS['$false'],
        CONDITIONS['$is_set'],
        CONDITIONS['$is_null']
    ]
};