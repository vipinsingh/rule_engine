import React, { Component } from "react";
import Modal from "../../../../../components/UI/Modal/Modal";
import { connect } from "react-redux";
import * as actions from "../../../../../store/actions/index";

class DeleteRuleModal extends Component {

    state = {
        showModal  : true
    }

    modalCloseHandler = () => {
        this.setState({showModal : false});
        this.props.close();
    }

    modalSubmitHandler = (event) => {
        event.preventDefault();
        this.props.onDeleteRule(this.props.ruleId);
        if(this.props.viewMode === 'view') {
            this.props.onSetViewMode('edit');
        }
        this.props.close();
    }

    render() {
        return (
            <Modal
                show={this.state.showModal}
                handleClose={this.modalCloseHandler}
                modalTitle="Delete Rule?"
                submitLabel="Yes"
                closeLabel="Cancel"
                handleSubmit={this.modalSubmitHandler}
            >
                <p>
                    Are you sure you want to delete this rule?
                </p>
            </Modal>
        );
    }
}

const mapStateToProps = state => {
    return {
        viewMode : state.decTable.viewMode
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDeleteRule : (ruleId) => dispatch(actions.deleteRule(ruleId)),
        onSetViewMode : (mode) => dispatch(actions.setViewMode(mode))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeleteRuleModal);