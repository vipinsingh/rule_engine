import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Table, Col, Button } from "react-bootstrap";
import classes from './DecisionTable.module.css';
import { updateObject, checkValidity } from "../../../../shared/utility";
import { createTableForm, VALIDATIONS } from './createTableFormConfigs';
import FormControl from "../../../../components/DynamicForm/FormControl/FormControl";
import AddField from "./AddField/AddField";
import Rule from "./Rule/Rule";
import Condition from "./Rule/Condition/Condition";
import * as actions from '../../../../store/actions/index';
import DeleteRuleModal from "./DeleteRuleModal/DeleteRuleModal";
import Spinner from "../../../../components/UI/Spinner/Spinner";
import { toast } from "react-toastify";
import NavTab from "./NavTab/NavTab";

class DecisionTable extends Component  {

    state = {
        createTableForm : createTableForm,
        createTableFormIsValid : false,
        showAddFieldModal : false,
        showConditionModal : false,
        conditionModal : null,
        addFieldModal : null,
        showRuleDeleteModal : false,
        deleteRuleModal : null
    }

    onClickAddCondition = (field, ruleId) => {
        const conditionModal = <Condition
                                    ruleId={ruleId}
                                    fieldId={field.id}
                                    fieldType={field.fieldType}
                                    fieldName={field.api_key}
                                    close={this.closeAddCondition}
                                />;
        this.setState({ conditionModal : conditionModal, showConditionModal : true});
    }

    onClickEditCondition = (field, condition) => {
        const conditionModal = <Condition
                                    ruleId={condition.rule_id}
                                    fieldId={field.id}
                                    fieldType={field.fieldType}
                                    fieldName={field.api_key}
                                    close={this.closeAddCondition}
                                    condition={condition}
                                />;
        this.setState({ conditionModal : conditionModal, showConditionModal : true});
    }

    closeAddCondition = () => {
        this.setState({ conditionModal : null, showConditionModal : false});
    }

    onClickAddField = () => {
        this.setState({showAddFieldModal : true, addFieldModal : null});
    }

    closeAddFieldModal = () => {
        this.setState({ addFieldModal : null, showAddFieldModal : false});
    }

    onClickEditField = (field) => {
        const addField = <AddField close={this.closeAddFieldModal} field={field} />;

        this.setState({ addFieldModal : addField, showAddFieldModal : true });
    }

    checkAllRulesValid = () => {
        if(this.props.rules.length > 0) {
            let rulesValid = true;
            for(let i = 0; i < this.props.rules.length; i++) {
                if(this.props.rules[i].decision === '' || (this.props.rules[i].conditions.length === 0 || this.props.rules[i].conditions.length !== this.props.fields.length)) {
                    rulesValid = false;
                    break;
                }
            }
            return rulesValid;
        }

        return false;
    }

    setValidationAccordingToTableType = (controls, validationBool) => {
        const default_decision_validation = updateObject(controls['default_decision'].validation, {
            isNumericPositive : validationBool
        });
        
        let decision_type_valid = true;

        if(!validationBool) {
            let validationObj = updateObject(controls.decision_type.validation ? controls.decision_type.validation : {}, {
                required : true
            })
            decision_type_valid = checkValidity(controls.decision_type.value, validationObj);
        }

        controls = updateObject(controls, {
            'decision_type' : updateObject(controls['decision_type'], {
                elementConfig : updateObject(controls['decision_type'].elementConfig, {
                    type : validationBool ? 'hidden' : 'select'
                }),
                value : validationBool ? '' : controls.decision_type.value,
                valid : validationBool ? true : decision_type_valid
            }),
            'default_decision' : updateObject(controls['default_decision'], {
                validation  : default_decision_validation,
                valid : checkValidity(controls['default_decision'].value, default_decision_validation)
            })
        });

        return controls;
    }

    setDefaultDecisionValidation = (controls, validationObj) => {
        controls = updateObject(controls, {
            'default_decision' : updateObject(controls['default_decision'], {
                validation  : validationObj,
                valid : checkValidity(controls['default_decision'].value, validationObj)
            })
        });

        return controls;
    }

    setDecisionTextArea = (controls, undo = false) => {
        let configObj = {
            type : 'text'
        }

        if(!undo) {
            configObj = {
                type : 'textarea',
                rows : 3,
            }
        }

        controls = updateObject(controls, {
            'default_decision' : updateObject(controls['default_decision'], {
                elementConfig : updateObject(controls['default_decision'].elementConfig, configObj)
            })
        });

        return controls;
    }

    inputChangedHandler = (event, controlName) => {
        let updatedControls = updateObject(this.state.createTableForm, {
            [controlName] : updateObject(this.state.createTableForm[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.createTableForm[controlName].validation),
                touched : true
            }) 
        });

        if(controlName === 'table_type' && event.target.value === 'scoring') {
            updatedControls = this.setValidationAccordingToTableType(updatedControls, true);
            
        } else if((this.state.createTableForm.decision_type.elementConfig.type === 'hidden' && this.state.createTableForm.table_type.value !== 'scoring')
            || (controlName === 'table_type' && event.target.value !== 'scoring')) {
            updatedControls = this.setValidationAccordingToTableType(updatedControls, false);
        }

        if(controlName === 'decision_type') {
            const type = event.target.value;
            if(updatedControls.default_decision.elementConfig.type === 'textarea' && type !== 'json') {
                updatedControls = this.setDecisionTextArea(updatedControls, true);
            }

            switch(type) {
                case 'string' : 
                    updatedControls = this.setDefaultDecisionValidation(updatedControls, VALIDATIONS['string']);
                    break;
                case 'number' : 
                    updatedControls = this.setDefaultDecisionValidation(updatedControls, VALIDATIONS['number']);
                    break;
                case 'alpha_num' :
                    updatedControls = this.setDefaultDecisionValidation(updatedControls, VALIDATIONS['alpha_num']);
                    break;
                case 'json' :
                    updatedControls = this.setDefaultDecisionValidation(updatedControls, VALIDATIONS['json']);
                    if(updatedControls.default_decision.elementConfig.type !== 'textarea') {
                        updatedControls = this.setDecisionTextArea(updatedControls);
                    }
                    break;
                default : break;
            }
        }

        if(this.props.viewMode === 'view' && this.props.match.params.tableId) {
            this.props.onSetViewMode('edit');
        }

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        this.setState({createTableForm : updatedControls, createTableFormIsValid : formIsValid});
    }

    onDeleteRule = (ruleId) => {
        const deleteRuleModal = <DeleteRuleModal
                ruleId={ruleId}
                close={this.onDeleteRuleClose}
            />;
        this.setState({ deleteRuleModal : deleteRuleModal, showRuleDeleteModal : true });
    }

    onDeleteRuleClose = () => {
        this.setState({ deleteRuleModal : null, showRuleDeleteModal : false });
    }

    onAddRuleClick = () => {
        this.props.onAddRule();
    }

    onClickCreateTable = (event) => {
        event.preventDefault();

        const tableData = {
            name                : this.state.createTableForm.name.value,
            description         : this.state.createTableForm.description.value,
            table_type          : this.state.createTableForm.table_type.value,
            decision_type       : this.state.createTableForm.decision_type.value,
            default_title       : this.state.createTableForm.default_title.value,
            default_description : this.state.createTableForm.default_description.value,
            default_decision    : this.state.createTableForm.default_decision.value,
            fields              : this.props.fields,
            rules               : this.props.rules
        };

        this.props.onCreateTable(tableData, this.props.user_id, this.props.match.params.projectId);
    }

    onClickUpdateTable = (event) => {
        event.preventDefault();

        const tableData = {
            name                : this.state.createTableForm.name.value,
            description         : this.state.createTableForm.description.value,
            table_type          : this.state.createTableForm.table_type.value,
            decision_type       : this.state.createTableForm.decision_type.value,
            default_title       : this.state.createTableForm.default_title.value,
            default_description : this.state.createTableForm.default_description.value,
            default_decision    : this.state.createTableForm.default_decision.value,
            fields              : this.props.fields,
            rules               : this.props.rules
        };

        this.props.onUpdateTable(tableData, this.props.user_id, this.props.match.params.projectId, this.props.match.params.tableId);
    }

    componentDidUpdate(prevProps) {
        if(this.props.error) {
            toast.error('An error occured!');
            this.props.onErrorReset();
        }

        if(this.props.isTableCreated && !this.props.match.params.tableId) {
            this.props.history.replace(`/projects/${this.props.match.params.projectId}/tables/${this.props.tableInfo.id}`);
        }

        if(this.props.match.params.tableId && this.props.isTableCreated !== prevProps.isTableCreated && this.props.isTableCreated === true) {
            if(this.props.tableInfo && this.props.isTableCreated) {
                let updatedControls = this.state.createTableForm;
                Object.keys(this.state.createTableForm).forEach(control => {
                    updatedControls = updateObject(updatedControls, {
                        [control] : updateObject(updatedControls[control], {
                            value : this.props.tableInfo[control] || '',
                            valid : true
                        })
                    })
                });
                this.setState({ createTableForm : updatedControls, createTableFormIsValid : true});
            }
        }

        if(this.props.match.params.tableId && this.props.tableInfo !== prevProps.tableInfo) {
            let updatedControls = this.state.createTableForm;
            Object.keys(this.state.createTableForm).forEach(control => {
                updatedControls = updateObject(updatedControls, {
                    [control] : updateObject(updatedControls[control], {
                        value : this.props.tableInfo[control] || '',
                        valid : true
                    })
                })
            });

            this.setState({ createTableForm : updatedControls, createTableFormIsValid : true});
        }
    }

    componentDidMount() {
        if(this.props.match.params.tableId && !this.props.isTableCreated) {
            if(this.props.tableInfo) {
                let updatedControls = this.state.createTableForm;
                Object.keys(this.state.createTableForm).forEach(control => {
                    updatedControls = updateObject(updatedControls, {
                        [control] : updateObject(updatedControls[control], {
                            value : this.props.tableInfo[control] || '',
                            valid : true
                        })
                    })
                });
                this.setState({ createTableForm : updatedControls});
                this.props.onSetViewMode('view');
            } else {
                this.props.onFetchTableDetails(this.props.match.params.tableId, this.props.user_id, this.props.match.params.projectId);
            }
        }
    }

    componentWillUnmount() {
        this.props.onClearTableState();
    }

    render() {
        const addFieldModal = this.state.showAddFieldModal ? 
            ( this.state.addFieldModal ? this.state.addFieldModal : <AddField close={this.closeAddFieldModal} />) 
            : null;

        const decision_fields = this.props.fields.map(field => {
            return <th key={field.id} onClick={() => this.onClickEditField(field)}>{field.title.toUpperCase()}<br></br><span>{field.api_key}</span></th>;
        })

        const addRuleColspan = 5 + this.props.fields.length;

        const navTab = this.props.viewMode !== 'create' ? 
            <NavTab history={this.props.history} match={this.props.match} /> : null;

        const decision_rules = this.props.rules.map(rule => {
            return (
                <Rule
                    key={rule.id}
                    fields={this.props.fields}
                    conditions={rule.conditions}
                    ruleId={rule.id}
                    tableType={this.state.createTableForm.table_type.value}
                    decisionType={this.state.createTableForm.decision_type.value}
                    onAddCondition={this.onClickAddCondition}
                    onEditCondition={this.onClickEditCondition}
                    onRuleDelete={this.onDeleteRule}
                />
            );
        });

        let submitButton = null;

        if(this.props.viewMode === 'create') {
            submitButton = (
                <Button variant="primary"
                    style={{float : 'right', marginRight : '30px'}}
                    disabled={!(this.state.createTableFormIsValid && this.checkAllRulesValid())}
                    onClick={(event) =>  this.onClickCreateTable(event)}
                >
                    CREATE TABLE
                </Button>
            );
        }

        if(this.props.viewMode === 'edit') {
            submitButton = (
                <Button variant="primary"
                    style={{float : 'right', marginRight : '30px'}}
                    disabled={!(this.state.createTableFormIsValid && this.checkAllRulesValid())}
                    onClick={(event) => this.onClickUpdateTable(event)}
                >
                    SAVE CHANGES
                </Button>
            );
        }

        const spinner = this.props.loading ? <Spinner /> : null;
        
        const decisionType = this.state.createTableForm['decision_type'].value ? this.state.createTableForm['decision_type'].value : ''; 

        return (
            <React.Fragment>
                {navTab}
                {spinner}
                <Form>
                    <Form.Group controlId="formTableInfo" className={classes.Form1}>
                        <FormControl config={this.state.createTableForm['name']} changed={this.inputChangedHandler}/>
                        <FormControl config={this.state.createTableForm['description']} changed={this.inputChangedHandler}/>
                    </Form.Group>
                    <Form.Group controlId="formDecisionTable">
                        {addFieldModal}
                        {this.state.showConditionModal ? this.state.conditionModal : null}
                        {this.state.showRuleDeleteModal ? this.state.deleteRuleModal : null}
                        <div className={['table-responsive'].join(' ')}>
                            <Table striped bordered hover className={classes.DecisionTable}>
                                <thead> 
                                    <tr style={{verticalAlign : 'middle'}}>
                                        <th style={{ color : '#696969', minWidth : '30px'}}>#</th>
                                        <th style={{minWidth : '400px'}}>TITLE<br></br><span>Description</span></th>
                                        {decision_fields}
                                        <th style={{ color : 'blue', cursor : 'pointer', minWidth : '30px'}} onClick={this.onClickAddField}>+</th>
                                        <th style={{minWidth : '200px'}}>
                                            {
                                                this.state.createTableForm.table_type.value === 'scoring' ? 
                                                    'SCORING' : 
                                                    (<div>
                                                        DECISION<br></br><span>type : {decisionType}</span>
                                                    </div>)
                                            }
                                        </th>
                                        <th style={{minWidth : '100px'}}>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {decision_rules}
                                    <tr>
                                        <td colSpan={addRuleColspan} style={{textAlign : 'center', cursor : 'pointer'}} onClick={this.onAddRuleClick}>+ ADD RULE</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                    </Form.Group>
                    <Form.Row className={classes.FormRow}>
                        <Col>
                            <Form.Label><strong>Default Decision</strong></Form.Label>
                        </Col>
                        <Col>
                            <FormControl config={this.state.createTableForm['default_title']} changed={this.inputChangedHandler}/>
                        </Col>
                        <Col>
                            <FormControl config={this.state.createTableForm['default_description']} changed={this.inputChangedHandler}/>
                        </Col>
                        <Col>
                            <FormControl config={this.state.createTableForm['default_decision']} changed={this.inputChangedHandler}/>
                        </Col>
                    </Form.Row>
                    <Form.Row className={classes.FormRow}>
                        <Col>
                            <Form.Label ><strong>Table Type</strong></Form.Label>
                        </Col>
                        <Col>
                            <FormControl config={this.state.createTableForm['table_type']} changed={this.inputChangedHandler}/>
                        </Col>
                        <Col></Col>
                        <Col></Col>
                    </Form.Row>
                    <Form.Row className={classes.FormRow}>
                        <Col>
                            {
                                this.state.createTableForm.table_type.value === 'scoring' ?
                                '' : <Form.Label><strong>Decision Type</strong></Form.Label>
                            }
                        </Col>
                        <Col>
                            <FormControl config={this.state.createTableForm['decision_type']} changed={this.inputChangedHandler}/>
                        </Col>
                        <Col></Col>
                        <Col>
                            {submitButton}
                        </Col>
                    </Form.Row>
                </Form>
            </React.Fragment>
        );

    }
}

const mapStateToProps = state => {
    return {
        tableInfo : state.decTable.tableInfo,
        fields : state.decTable.fields,
        rules : state.decTable.rules,
        loading : state.decTable.loading,
        error : state.decTable.error,
        user_id : state.auth.userData.user_id,
        isTableCreated : state.decTable.isTableCreated,
        viewMode : state.decTable.viewMode
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddRule : () => dispatch(actions.addRule()),
        onCreateTable : (tableData, user_id, project_id) => dispatch(actions.createTable(tableData, user_id, project_id)),
        onUpdateTable : (tableData, user_id, project_id, table_id) => dispatch(actions.updateTable(tableData, user_id, project_id, table_id)),
        onErrorReset : () => dispatch(actions.errorReset()),
        onSetViewMode : (mode) => dispatch(actions.setViewMode(mode)),
        onFetchTableDetails : (tableId, user_id, project_id) => dispatch(actions.fetchTableDetails(tableId, user_id, project_id)),
        onClearTableState : () => dispatch(actions.clearTableState())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DecisionTable);