import React, { Component } from "react";
import { connect } from "react-redux";
import NavTab from "../NavTab/NavTab";
import { updateObject } from "../../../../../shared/utility";
import { Form, Col, Card, Button } from "react-bootstrap";
import Spinner from "../../../../../components/UI/Spinner/Spinner";
import * as actions from '../../../../../store/actions/index';
import classes from './Debugger.module.css';
import { toast } from "react-toastify";
import { FIELD_TYPE_CONFIG } from "../../../../../shared/constants";

class Debugger extends Component {

    state = {
        fieldValues : {}
    }

    inputChangeHandler = (event, fieldName) => {
        const updatedFieldValues = updateObject(this.state.fieldValues, {
            [fieldName] : event.target.value
        });

        this.setState({ fieldValues : updatedFieldValues });
    }

    setFormControl = (field) => {
        switch(field.fieldType) {
            case 'string' :
                return (
                    <Form.Control
                        type={FIELD_TYPE_CONFIG[field.fieldType].type}
                        value={this.state.fieldValues[field.api_key] ? this.state.fieldValues[field.api_key] : ''}
                        onChange={(event) => this.inputChangeHandler(event, field.api_key)}
                    />
                );
            case 'number' :
                return (
                    <Form.Control
                        type={FIELD_TYPE_CONFIG[field.fieldType].type}
                        value={this.state.fieldValues[field.api_key] ? this.state.fieldValues[field.api_key] : ''}
                        onChange={(event) => this.inputChangeHandler(event, field.api_key)}
                    />
                );
            case 'boolean' :
                const options = FIELD_TYPE_CONFIG[field.fieldType].options.map(option => {
                    return <option key={option.value} value={option.value}>{option.displayValue}</option>;
                });
                return (
                    <Form.Control
                        as={FIELD_TYPE_CONFIG[field.fieldType].type}
                        value={this.state.fieldValues[field.api_key] ? this.state.fieldValues[field.api_key] : ''}
                        onChange={(event) => this.inputChangeHandler(event, field.api_key)}
                        className={classes.FormControl}
                    > {options} </Form.Control>
                );
            default : return null;
        }
    }

    componentDidMount() {
        if(this.props.fields.length === 0) {
            this.props.onFetchTableDetails(this.props.match.params.tableId, this.props.user_id, this.props.match.params.projectId);
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.fields !== this.props.fields) {
            const fields = {};
            this.props.fields.forEach(field => {
                fields[field.api_key] = '';
            })
            this.setState({ fieldValues : fields });
        }

        if(this.props.error && !this.props.error.errors) {
            toast.error('An error occured!');
            this.props.onErrorReset();
        }
    }

    componentWillUnmount() {
        this.props.onClearTableState();
    }

    onClickSendButton = () => {
        const decisionParams = {
            table_id : this.props.match.params.tableId,
            ...this.state.fieldValues
        }

        this.props.onGetDecision(decisionParams);
    }

    render() {
        const spinner = this.props.loading ? <Spinner /> : null;

        const form = this.props.fields.map(field => {
            const formControl = this.setFormControl(field);
            const apiKey = field.api_key.length > 20 ? field.api_key.substring(0,16) + '...' : field.api_key;

            return (
                <Form.Row key={field.id} className={classes.FormRow}>
                    <Col style={{ width : '100px' }}>
                        <Form.Label className={classes.FormLabel}><strong>{apiKey} :</strong></Form.Label>
                    </Col>
                    <Col>
                        {formControl}
                    </Col>
                </Form.Row>
            );
        });

        let jsonData = {
            'step1' : 'Fill the form',
            'step2' : 'Click on Send button'
        }

        if(this.props.decisionData) {
            jsonData = this.props.decisionData;
        }

        if(this.props.error && this.props.error.errors) {
            jsonData = this.props.error.errors;
        }

        return (
            <React.Fragment>
                <NavTab history={this.props.history} match={this.props.match} />
                {spinner}
                <h6 style={{ marginLeft : '50px', marginBottom : '40px'}}>
                    You can use this form to create an API request to your table.
                </h6>
                <h3 style={{ marginLeft : '50px'}}>
                    {this.props.tableInfo ? this.props.tableInfo.name : ''}
                </h3>
                <label style={{ marginLeft : '50px', fontSize : '18px' }}>
                    {this.props.tableInfo ? this.props.tableInfo.description : ''}
                </label>
                <div className="row" style={{ width : '95%' }}>
                    <div className="col-md-6">
                        {form}
                        <Button variant="primary"
                            style={{float : 'right', marginTop : '30px', marginBottom : '10px'}}
                            onClick={this.onClickSendButton}
                        >
                            SEND
                        </Button>
                    </div>
                    <div className="col-md-6">
                        <Card style={{ marginTop : '30px', marginLeft : '50px'}}>
                            <Card.Body>
                                <pre>
                                    {JSON.stringify(jsonData, undefined, 2)}
                                </pre>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        tableInfo :  state.decTable.tableInfo,
        fields : state.decTable.fields,
        loading : state.decTable.loading,
        error : state.decTable.error,
        user_id : state.auth.userData.user_id,
        decisionData : state.decTable.decisionData
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchTableDetails : (tableId, user_id, project_id) => dispatch(actions.fetchTableDetails(tableId, user_id, project_id)),
        onClearTableState : () => dispatch(actions.clearTableState()),
        onGetDecision : (decisionParams) => dispatch(actions.getDecision(decisionParams)),
        onErrorReset : () => dispatch(actions.errorReset()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Debugger);