import React, { Component } from "react";
import { addFieldModalForm } from "../createTableFormConfigs";
import DynamicForm from '../../../../../components/DynamicForm/DynamicForm';
import Modal from "../../../../../components/UI/Modal/Modal";
import { updateObject, checkValidity } from "../../../../../shared/utility";
import classes from './AddField.module.css';
import { connect } from "react-redux";
import { Button } from 'react-bootstrap';
import * as actions from '../../../../../store/actions/index';

class AddField extends Component {

    state = {
        addFieldModalForm : addFieldModalForm,
        formIsValid :  false,
        showModal : true,
        showDeleteModal : false
    }

    modalCloseHandler = () => {
        this.setState({showModal : false});
        this.props.close();
    }

    modalSubmitHandler = (event) => {
        event.preventDefault();
        const fieldData = {
            title     : this.state.addFieldModalForm.title.value,
            api_key   : this.state.addFieldModalForm.api_key.value,
            fieldType : this.state.addFieldModalForm.fieldType.value,
        };

        if(this.props.field) {
            this.props.onEditField(this.props.field.id, fieldData);
        } else {
            this.props.onAddField(fieldData);
        }

        if(this.props.viewMode === 'view') {
            this.props.onSetViewMode('edit');
        }

        this.props.close();
    }

    deleteModalCloseHandler = () => {
        this.setState({showDeleteModal : false});
        this.props.close();
    }

    deleteModalSubmitHandler = (event) => {
        event.preventDefault();
        this.props.onDeleteField(this.props.field.id);
        if(this.props.viewMode === 'view') {
            this.props.onSetViewMode('edit');
        }
        this.props.close();
    }

    onClickDeleteField = () => {
        this.setState({ showModal : false, showDeleteModal : true});
    }

    componentDidMount() {
        if(this.props.field) {
            const updatedControls = updateObject(this.state.addFieldModalForm, {
                title : updateObject(this.state.addFieldModalForm.title, {
                    value : this.props.field.title,
                    valid : checkValidity(this.props.field.title, this.state.addFieldModalForm.title.validation),
                    touched : true
                }),
                api_key : updateObject(this.state.addFieldModalForm.api_key, {
                    value : this.props.field.api_key,
                    valid : checkValidity(this.props.field.api_key, this.state.addFieldModalForm.api_key.validation),
                    touched : true
                }),
                fieldType : updateObject(this.state.addFieldModalForm.fieldType, {
                    value : this.props.field.fieldType,
                    valid : checkValidity(this.props.field.fieldType, this.state.addFieldModalForm.fieldType.validation),
                    touched : true
                })
            });

            let formIsValid = true;

            for(let control in updatedControls) {
                formIsValid = updatedControls[control].valid && formIsValid;
            }

            this.setState({addFieldModalForm : updatedControls, formIsValid : formIsValid});
        }
    }
 
    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.addFieldModalForm, {
            [controlName] : updateObject(this.state.addFieldModalForm[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.addFieldModalForm[controlName].validation),
                touched : true
            }) 
        });

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        this.setState({addFieldModalForm : updatedControls, formIsValid : formIsValid});
    }

    render() {
        const dynamicForm = <DynamicForm changed={this.inputChangedHandler} valid={this.state.formIsValid} controls={this.state.addFieldModalForm}/>;

        return (
            <div className={classes.AddField}>
                <Modal
                    show={this.state.showModal}
                    handleClose={this.modalCloseHandler}
                    modalTitle="Add Field"
                    submitDisable={!this.state.formIsValid}
                    handleSubmit={this.modalSubmitHandler}
                >
                    <form>
                        {dynamicForm}
                    </form>
                    {this.props.field ? 
                        <Button onClick={this.onClickDeleteField} style={{ margin : '10px 10px'}} variant="danger">DELETE</Button> : null
                    }
                </Modal>
                <Modal
                    show={this.state.showDeleteModal}
                    handleClose={this.deleteModalCloseHandler}
                    modalTitle="Delete Field?"
                    submitLabel="Yes"
                    closeLabel="Cancel"
                    handleSubmit={this.deleteModalSubmitHandler}
                    isCentered
                >
                    <p>
                        Are you sure you want to delete this field?
                    </p>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        viewMode : state.decTable.viewMode
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddField : (fieldData) => dispatch(actions.addField(fieldData)),
        onEditField : (fieldId, fieldData) => dispatch(actions.editField(fieldId, fieldData)),
        onSetViewMode : (mode) => dispatch(actions.setViewMode(mode)),
        onDeleteField : (fieldId) => dispatch(actions.deleteField(fieldId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddField);