import React, { Component } from "react";
import { connect } from "react-redux";
import { Table, Card } from "react-bootstrap";
import classes from './DecisionDetails.module.css';
import NavTab from '../../Tables/DecisionTable/NavTab/NavTab';
import Spinner from '../../../../components/UI/Spinner/Spinner';
import * as actions from '../../../../store/actions/index';
import { toast } from "react-toastify";
import { CONDITIONS } from "../../Tables/DecisionTable/createTableFormConfigs";

class DecisionDetails extends Component {

    componentDidMount() {
        const params = {
            user_id : this.props.user_id,
            project_id : this.props.match.params.projectId
        }

        if(this.props.match.params.tableId) {
            params['table_id'] = this.props.match.params.tableId;
        }

        this.props.onGetDecisionDetails(this.props.match.params.decId, params);
    }

    componentDidUpdate(prevProps) {
        if(this.props.error && !this.props.error.errors) {
            toast.error('An error occured!');
            this.props.onErrorReset();
        }
    }

    render() {
        const spinner = this.props.loading ? <Spinner /> : null;

        const navTab = this.props.match.params.tableId ? <NavTab history={this.props.history} match={this.props.match}/> : null;

        let jsonData = null;

        if(this.props.decision && this.props.decision.request) {
            jsonData = this.props.decision.request;
        }

        let tableFields = null;

        let col_span = 2; 

        if(this.props.decision && this.props.decision.fields) {
            tableFields = this.props.decision.fields.map(field => {
                col_span++;
                return (
                    <th key={field.id}>{field.title}</th>
                );
            })
        }

        let tableBody = (
            <tr>
                <td colSpan={col_span} style={{textAlign : 'center', color : 'blue'}}>No decision to display.</td>
            </tr>
        );

        if(this.props.decision && this.props.decision.rules) {
            tableBody = this.props.decision.rules.map(rule => {

                let conditions = rule.conditions.map(condition => {
                    let bg_color = condition.matched ? '#90EE90' : 'salmon';
                    return (
                        <td key={condition.field_id} style={{ backgroundColor : bg_color }}>
                            {CONDITIONS[condition.condition].displayValue} {condition.value}
                        </td>
                    );
                })

                let bg_color = rule.matched ? '#90EE90' : 'salmon';

                return (
                    <tr key={rule.id}>
                        <td>
                            <span><strong>{rule.title}</strong></span>
                            <br></br>
                            <span>{rule.description}</span>
                        </td>
                        {conditions}
                        <td style={{ backgroundColor : bg_color}}>{rule.decision}</td>
                    </tr>
                );
            })
        }

        
        return (
            <React.Fragment>
                {navTab}
                {spinner}
                <h3 style={{ margin : '30px 50px'}}>
                    Decisions Details
                </h3>
                <label style={{ marginLeft : '50px', fontSize : '16px' }}>
                    Here you can see all request parameters, rule engine response and decision table that was applied when a request was made. 
                    Whenever you change your decision table, it won't affect it's history and you will be able to understand decision flow for 
                    each moment of time.
                </label>
                <h3 style={{ marginLeft : '50px', marginTop : '30px'}}>
                    {this.props.decision && this.props.decision.table_info ? this.props.decision.table_info.name : ''}
                </h3>
                <label style={{ margin : '10px 50px', fontSize : '18px' }}>
                    {this.props.decision && this.props.decision.table_info ? this.props.decision.table_info.description : ''}
                </label>
                <h5 style={{ marginLeft : '50px', marginTop : '20px'}}>
                    Request :
                </h5>
                <Card style={{ marginTop : '10px', marginLeft : '40px', width : '70%'}}>
                    <Card.Body>
                        <pre>
                            {JSON.stringify(jsonData, undefined, 2)}
                        </pre>
                    </Card.Body>
                </Card>
                <div className={['table-responsive'].join(' ')}>
                    <Table striped bordered hover style={{width : '90%', margin : '30px 40px'}} className={classes.DecisionDetails}>
                        <thead>
                            <tr>
                                <th>RULE TITLE<br></br><span>Description</span></th>
                                {tableFields}
                                <th>DECISION</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableBody}                        
                        </tbody>
                    </Table>
                </div>
                <h5 style={{ marginLeft : '50px', marginTop : '20px'}}>
                    Default Decision : 
                    <span style={{ fontWeight : 'normal', fontSize : '18px' }}>
                        {this.props.decision && this.props.decision.table_info ?  ' ' + this.props.decision.table_info.default_decision : ''}
                    </span>
                </h5>
                <h5 style={{ marginLeft : '50px', marginTop : '20px', marginBottom : '20px'}}>
                    Final Decision : 
                    <span style={{ fontWeight : 'normal', fontSize : '18px' }}>
                        {this.props.decision ? ' ' + this.props.decision.final_decision : ''}
                    </span>
                </h5>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        decision :  state.decTable.decisionDetails,
        loading : state.decTable.loading,
        error : state.decTable.error,
        user_id : state.auth.userData.user_id
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onGetDecisionDetails : (decId, params) => dispatch(actions.getDecisionDetails(decId, params)),
        onErrorReset : () => dispatch(actions.errorReset())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DecisionDetails);