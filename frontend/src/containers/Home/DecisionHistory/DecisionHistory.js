import React, { Component } from "react";
import { connect } from "react-redux";
import { Table } from "react-bootstrap";
import classes from './DecisionHistory.module.css';
import NavTab from '../Tables/DecisionTable/NavTab/NavTab';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Pagination from '../../../components/Pagination/Pagination';
import * as actions from '../../../store/actions/index';
import { toast } from "react-toastify";
import { getDateTimeFromISO } from '../../../shared/utility';

class DecisionHistory extends Component {

    state = {
        perPage : 15,
        pageNo : 1,
        tillPage : 5,
        maxPageItems : 5
    }

    handlePageSelect = (number) => {
        this.setState({ pageNo : number });

        const params = {
            user_id : this.props.user_id,
            project_id : this.props.match.params.projectId
        }

        if(this.props.match.params.tableId) {
            params['table_id'] = this.props.match.params.tableId;
        }

        this.props.onListDecisions(params, number, this.state.perPage);
    }

    handlePrevNext = (action) => {
        let tillPage = null;
        if(action === 'prev') {
            tillPage = this.state.tillPage - this.state.maxPageItems;
        } else if(action === 'next') {
            tillPage = this.state.tillPage + this.state.maxPageItems;
        }

        this.setState({ tillPage : tillPage })
    }

    componentDidMount() {
        const params = {
            user_id : this.props.user_id,
            project_id : this.props.match.params.projectId
        }

        if(this.props.match.params.tableId) {
            params['table_id'] = this.props.match.params.tableId;
        }

        this.props.onListDecisions(params);
    }

    componentDidUpdate(prevProps) {
        if(this.props.error && !this.props.error.errors) {
            toast.error('An error occured!');
            this.props.onErrorReset();
        }

        if(this.props.match.params.tableId !== prevProps.match.params.tableId) {
            const params = {
                user_id : this.props.user_id,
                project_id : this.props.match.params.projectId
            }
            this.props.onListDecisions(params);
            this.setState({ pageNo : 1, tillPage : 5});
        }
    }

    // getDateTimeFromISO(ISOstring) {
    //     const date = new Date(ISOstring);
    //     const year = date.getFullYear();
    //     let month = date.getMonth() + 1;
    //     let dt = date.getDate();
    //     const time = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) +
    //         ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) +
    //         ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());

    //     if (dt < 10) {
    //         dt = '0' + dt;
    //     }
    //     if (month < 10) {
    //         month = '0' + month;
    //     }

    //     return (month + '-' + dt + '-' + year + ' ' + time);
    // }

    viewDecisionDetails = (decId) => {
        this.props.history.push(`${this.props.match.url}/${decId}`)
    }

    render() {
        const spinner = this.props.loading ? <Spinner /> : null;

        const navTab = this.props.match.params.tableId ? <NavTab history={this.props.history} match={this.props.match}/> : null;

        let tableBody = (
            <tr>
                <td colSpan="4" style={{textAlign : 'center', color : 'blue'}}>No decision to display.</td>
            </tr>
        );

        if(this.props.decisions && this.props.decisions.decisions.length > 0) {
            tableBody = this.props.decisions.decisions.map(decision => {
                return (
                    <tr key={decision.id} onClick={() => this.viewDecisionDetails(decision.id)}>
                        <td>
                            <span><strong>{decision.rule_title}</strong></span>
                            <br></br>
                            <span>{decision.rule_description}</span>
                        </td>
                        <td>{decision.final_decision}</td>
                        <td>
                            <span><strong>Name :</strong> {decision.table_info.name}</span>
                            <br></br>
                            <span><strong>Description :</strong> {decision.table_info.description}</span>
                        </td>
                        <td>{getDateTimeFromISO(decision.created_at)}</td>
                    </tr>
                );
            })
        }

        return (
            <React.Fragment>
                {navTab}
                {spinner}
                <h3 style={{ margin : '30px 50px'}}>
                    Decisions History
                </h3>
                <label style={{ marginLeft : '50px', fontSize : '16px' }}>
                    Here you can see the history of all decisions of a project/table. 
                </label>
                <div className={['table-responsive'].join(' ')}>
                    <Table striped bordered hover style={{width : '90%', marginLeft : '40px'}} className={classes.DecisionHistory}>
                        <thead>
                            <tr>
                                <th>RULE TITLE<br></br><span>Description</span></th>
                                <th>DECISION</th>
                                <th>TABLE INFO</th>
                                <th>TIMESTAMP</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableBody}                        
                        </tbody>
                    </Table>
                </div>
                <Pagination 
                    recordCount={this.props.decisions && this.props.decisions.count ? this.props.decisions.count : 0}
                    perPage={this.state.perPage}
                    pageNo={this.state.pageNo}
                    tillPage={this.state.tillPage}
                    maxPageItems={this.state.maxPageItems}
                    pageSelectHandler={this.handlePageSelect}
                    prevNextHandler={this.handlePrevNext}
                />
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        decisions : state.decTable.decisionsList,
        loading : state.decTable.loading,
        error : state.decTable.error,
        user_id : state.auth.userData.user_id
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onListDecisions : (params, pageNo = 1, perPage = 15) => dispatch(actions.listDecisions(params, pageNo, perPage)),
        onErrorReset : () => dispatch(actions.errorReset())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DecisionHistory);