import React, { Component } from "react";
import classes from './Projects.module.css';
import Table from "../../../components/Table/Table";
import { connect } from "react-redux";
import * as actions from '../../../store/actions/index';
import Spinner from "../../../components/UI/Spinner/Spinner";
import { toast } from "react-toastify";
import CreateProject from "./CreateProject/CreateProject";
import Pagination from "../../../components/Pagination/Pagination";

class Projects extends Component {

    state = {
        createProject : false,
        perPage : 15,
        pageNo : 1,
        tillPage : 5,
        maxPageItems : 5
    }

    componentDidMount() {
        this.props.onFetchProjects(this.props.user_id, this.state.pageNo, this.state.perPage);
        if(this.props.currentProj) {
            this.props.unsetCurrentProject();
        }
    }

    createProject = () => {
        this.setState({ createProject : true });
    }

    closeCreateProject = () => {
        this.setState({ createProject : false });
    }

    viewProjectDetails = (id) => {
        this.props.onViewCurrentProject(id);
        this.props.history.push(`/projects/${id}/tables`);
    }

    componentDidUpdate() {
        if(this.props.error) {
            toast.error('An error occured!');
            this.props.onErrorReset();
        }
    }

    handlePageSelect = (number) => {
        this.setState({ pageNo : number });
        this.props.onFetchProjects(this.props.user_id, number, this.state.perPage);
    }

    handlePrevNext = (action) => {
        let tillPage = null;
        if(action === 'prev') {
            tillPage = this.state.tillPage - this.state.maxPageItems;
        } else if(action === 'next') {
            tillPage = this.state.tillPage + this.state.maxPageItems;
        }

        this.setState({ tillPage : tillPage })
    }

    render() {

        const spinner = this.props.loading ? <Spinner /> : null;
        const createProject = this.state.createProject ? <CreateProject
            close={this.closeCreateProject}
        /> : null;

        return (
            <div className={classes.Projects}>
                {spinner}
                <h2>Projects</h2>
                {createProject}
                <Table
                    tableView='project'
                    tableData={this.props.projects ? this.props.projects.projects : []}
                    createClicked={this.createProject}
                    rowClicked={this.viewProjectDetails}
                    error={this.props.error}
                />
                <Pagination 
                    recordCount={this.props.projects && this.props.projects.count ? this.props.projects.count : 0}
                    perPage={this.state.perPage}
                    pageNo={this.state.pageNo}
                    tillPage={this.state.tillPage}
                    maxPageItems={this.state.maxPageItems}
                    pageSelectHandler={this.handlePageSelect}
                    prevNextHandler={this.handlePrevNext}
                />
                
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        projects : state.projects.projects,
        currentProj : state.projects.currentProj,
        loading : state.projects.loading,
        error : state.projects.error,
        user_id : state.auth.userData.user_id
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchProjects : (user_id, pageNo = 1, perPage = 15) => dispatch(actions.fetchProjects(user_id, pageNo, perPage)),
        onErrorReset : () => dispatch(actions.resetError()),
        onViewCurrentProject : (projectId) => dispatch(actions.setCurrentProject(projectId)),
        unsetCurrentProject : () => dispatch(actions.unsetCurrentProject())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Projects);