export const createProjectControls = {
    name :  {
        elementType : 'input',
        elementConfig : {
            type : 'input',
            placeholder : 'Project Name'
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 100
        },
        valid : false,
        touched : false
    },
    description :  {
        elementType : 'input',
        elementConfig : {
            type : 'input',
            placeholder : 'Project Description'
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 200
        },
        valid : false,
        touched : false
    }
}