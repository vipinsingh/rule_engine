import React, { Component } from "react";
import { connect } from "react-redux";
import classes from './CreateProject.module.css';
import { createProjectControls } from "./createProjectFormConfig";
import Modal from "../../../../components/UI/Modal/Modal";
import { updateObject, checkValidity } from "../../../../shared/utility";
import DynamicForm from '../../../../components/DynamicForm/DynamicForm';
import * as actions from '../../../../store/actions/index';

class CreateProject extends Component {

    state = {
        createProjectForm : createProjectControls,
        formIsValid : false,
        showModal : true
    }

    modalCloseHandler = () => {
        this.setState({showModal : false});
        this.props.close();
    }

    modalSubmitHandler = (event) => {
        event.preventDefault();
        const projectData = {
            user_id     : this.props.user_id,
            name        : this.state.createProjectForm.name.value,
            description : this.state.createProjectForm.description.value
        };
        this.props.close();
        this.props.onCreateProject(projectData);
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.createProjectForm, {
            [controlName] : updateObject(this.state.createProjectForm[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.createProjectForm[controlName].validation),
                touched : true
            }) 
        });

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        this.setState({createProjectForm : updatedControls, formIsValid : formIsValid});
    }

    render() {
        const dynamicForm = <DynamicForm changed={this.inputChangedHandler} valid={this.state.formIsValid} controls={this.state.createProjectForm}/>

        return (
            <div className={classes.CreateProject}>
                <Modal
                    show={this.state.showModal}
                    handleClose={this.modalCloseHandler}
                    modalTitle="Create Project"
                    submitDisable={!this.state.formIsValid}
                    handleSubmit={this.modalSubmitHandler}
                >
                    <form>
                        {dynamicForm}
                    </form>
                </Modal>
            </div>
        );

    }

}

const mapStateToProps = state => {
    return {
        user_id : state.auth.userData.user_id
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onCreateProject : (data) => dispatch(actions.createProject(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateProject);