import React, { Component } from 'react';
import { controls } from './form-config';
import classes from './SignIn.module.css';
import DynamicForm from '../../../components/DynamicForm/DynamicForm';
import Button from '../../../components/UI/Button/Button';
import { updateObject, checkValidity } from '../../../shared/utility';
import Header from '../../../components/Header/Header';
import Aux from '../../../hoc/Auxillary/Auxillary';
import Spinner from '../../../components/UI/Spinner/Spinner';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import { toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';

class SignIn extends Component {
    state = {
        formControls : controls,
        formIsValid : false
    }

    onSubmitHandler = (event) => {
        event.preventDefault();
        this.props.onSignIn(this.state.formControls.email.value, this.state.formControls.password.value);
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.formControls, {
            [controlName] : updateObject(this.state.formControls[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.formControls[controlName].validation),
                touched : true
            }) 
        });

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        this.setState({formControls : updatedControls, formIsValid : formIsValid});
    }

    goToSignUp = () => {
        this.props.history.push('/signup');
    }

    componentDidMount() {
        if(this.props.redirectPath !== '/projects' && !localStorage.getItem('token')) {
            this.props.onSetAuthRedirectPath();
        }
    }

    componentDidUpdate() {
        if(this.props.error) {
            if(this.props.error.code === 401) {
                toast.error('Invalid Credentials!', { position : toast.POSITION.BOTTOM_CENTER });
            } else {
                toast.error('An error occured!', { position : toast.POSITION.BOTTOM_CENTER });
            }
            this.props.onErrorReset();
        }
    }

    render() {
        const dynamicForm = <DynamicForm changed={this.inputChangedHandler} valid={this.state.formIsValid} controls={this.state.formControls}/>
        const spinner = this.props.loading ? <Spinner /> : null;

        let authRedirect = null;
        if(this.props.isAuthenticated) {
            authRedirect = <Redirect to={this.props.redirectPath}/>
        }

        return (
            <Aux>
                {spinner}
                {authRedirect}
                <Header />
                <div className={classes.SignIn}>
                    <form onSubmit={this.onSubmitHandler}>
                        {dynamicForm}
                        <Button btnType="Success" disabled={!this.state.formIsValid}>LOGIN</Button>
                    </form>
                    <p>Not a user? <Button btnType="Success" clicked={this.goToSignUp}>REGISTER</Button></p>
                </div>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading : state.auth.loading,
        error : state.auth.error,
        isAuthenticated : state.auth.token !== null,
        redirectPath : state.auth.authRedirectPath
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSignIn : (email, password) => dispatch(actions.auth(email, password)),
        onSetAuthRedirectPath : () => dispatch(actions.setAuthRedirectPath('/projects')),
        onErrorReset : () => dispatch(actions.resetAuth())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);