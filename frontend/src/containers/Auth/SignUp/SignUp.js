import React, { Component } from 'react';
import { controls } from './form-config';
import classes from './SignUp.module.css';
import DynamicForm from '../../../components/DynamicForm/DynamicForm';
import Button from '../../../components/UI/Button/Button';
import { updateObject, checkValidity } from '../../../shared/utility';
import Header from '../../../components/Header/Header';
import Aux from '../../../hoc/Auxillary/Auxillary';
import axios from '../../../axiosInstance';
import { toast } from 'react-toastify';
import Spinner from '../../../components/UI/Spinner/Spinner';
import md5 from 'md5';

class SignUp extends Component {
    state = {
        formControls : controls,
        formIsValid : false,
        loading : false
    }

    onSubmitHandler = (event) => {
        event.preventDefault();

        let data = {
            name : this.state.formControls.name.value,
            email : this.state.formControls.email.value,
            password : md5(this.state.formControls.password.value),
        };

        this.setState({loading : true});

        axios.post('/api/v1/user/signup', data)
            .then(data => {
                this.setState({loading : false});
                toast('Success! Login to continue!');
                this.props.history.push('/');
            })
            .catch(error => {
                this.setState({loading : false});
                toast.error('An error occured!');
            });
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.formControls, {
            [controlName] : updateObject(this.state.formControls[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, this.state.formControls[controlName].validation),
                touched : true
            }) 
        });

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        this.setState({formControls : updatedControls, formIsValid : formIsValid});
    }

    render() {
        const dynamicForm = <DynamicForm changed={this.inputChangedHandler} valid={this.state.formIsValid} controls={this.state.formControls}/>
        const spinner = this.state.loading ? <Spinner /> : null;

        return (
            <Aux>
                <Header />
                {spinner}
                <div className={classes.SignUp}>
                    <form onSubmit={this.onSubmitHandler}>
                        {dynamicForm}
                        <Button btnType="Success" disabled={!this.state.formIsValid}>REGISTER</Button>
                    </form>
                </div>
            </Aux>
        );
    }
}

export default SignUp;