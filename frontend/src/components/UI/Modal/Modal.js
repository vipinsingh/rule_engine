import React, { Component } from "react";
import { Modal as RBModal, Button } from 'react-bootstrap';

class Modal extends Component {

    // shouldComponentUpdate(nextProps, nextState) {
    //     return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    // }

    render() {

        return (
            <div>
                <RBModal show={this.props.show} onHide={this.props.handleClose} animation={false} centered={this.props.isCentered}>
                    <RBModal.Header closeButton>
                        <RBModal.Title>{this.props.modalTitle}</RBModal.Title>
                    </RBModal.Header>

                    <RBModal.Body>
                        {this.props.children}
                    </RBModal.Body>

                    <RBModal.Footer>
                        <Button variant="secondary" onClick={this.props.handleClose}>
                        {this.props.closeLabel ? this.props.closeLabel : 'Close'}
                        </Button>
                        <Button variant="primary" disabled={this.props.submitDisable} onClick={this.props.handleSubmit}>
                            {this.props.submitLabel ? this.props.submitLabel : 'Submit'}
                        </Button>
                    </RBModal.Footer>
                </RBModal>
            </div>
        );
    }
}

export default Modal;