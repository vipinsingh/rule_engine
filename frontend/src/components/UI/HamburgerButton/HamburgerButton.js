import React from 'react';
import classes from './HamburgerButton.module.css';

const hamburgerButton = () => {
    return (
        <div className={classes.HamburgerButton}>
            <span></span>
            <span></span>
            <span></span>
        </div>
    );
}

export default hamburgerButton;