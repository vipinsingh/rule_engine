import React from 'react';
import Logo from '../../assets/images/biz2X-logo.svg';
import classes from './Header.module.css';

const header = () => {
    return (
        <div className={classes.Header}>
            <h1>
                <img src={Logo} alt="Biz2xLogo"/>
                Rule Engine
            </h1>
        </div>
    );
}

export default header;