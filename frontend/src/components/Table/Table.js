import React from 'react';
import { Table } from 'react-bootstrap'; 
import { getDateTimeFromISO } from '../../shared/utility';

const table = (props) => {

    let colSpan = 3;

    let tableData = (
        <tr onClick={props.createClicked}>
            <td colSpan={colSpan} style={{textAlign : 'center', color : 'blue'}}> Get Started, create your first {props.tableView} </td>
        </tr>
    );
    
    if(props.tableData && props.tableData.length > 0) {
        tableData = props.tableData.map(data => {
            return (
                <tr key={data.id} onClick={() => props.rowClicked(data.id)}>
                    <td>{data.name}</td>
                    <td>{data.description}</td>
                    <td>{getDateTimeFromISO(data.created_at)}</td>
                </tr>
            )
        })
    }

    if(props.error) {
        tableData = (
            <tr>
                <td colSpan={colSpan} style={{textAlign : 'center', color : 'red'}}>Unable to fetch details right now, please try later.</td>
            </tr>
        );
    }

    return (
        <Table striped bordered hover style={{width : '90%', marginLeft : '40px'}}>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Creation Date</th>
                </tr>
            </thead>
            <tbody>
                {tableData}
            </tbody>
        </Table>
    );
}

export default table;