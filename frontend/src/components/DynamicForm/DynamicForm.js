import React from 'react';
import Input from './FormElements/Input/Input';
import Select from './FormElements/Select/Select';
import TextArea from './FormElements/TextArea/TextArea';

const dynamicForm = (props) => {

    let formElements = [];

    for(let key in props.controls) {
        formElements.push({
            id : key,
            config : props.controls[key]
        });
    }
    

    let form = formElements.map(formElement => {
        let childProps = {
            key: formElement.id,
            elementConfig: formElement.config.elementConfig,
            value: formElement.config.value,
            invalid: !formElement.config.valid,
            shouldValidate: formElement.config.validation,
            touched: formElement.config.touched,
            changed: (event) => props.changed(event, formElement.id),
            label: formElement.config.label
        };

        switch(formElement.config.elementType) {
            case 'input' : return <Input {...childProps} />;
            case 'textarea' : return <TextArea {...childProps} />;
            case 'select' : return <Select {...childProps} />;
            default : return null;
        }
    });

    return (form);
}

export default dynamicForm;