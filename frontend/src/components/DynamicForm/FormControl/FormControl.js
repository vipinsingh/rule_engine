import React from 'react';
import { Form } from 'react-bootstrap';
import classes from './FormControl.module.css';

const formControl = (props) => {

    const controlConfig = props.config;
    
    const controlClasses = [ classes[controlConfig.elementConfig.className] ];

    if(controlConfig.touched && controlConfig.validation && !controlConfig.valid) {
        controlClasses.push(classes.Invalid);
    }

    switch(controlConfig.elementConfig.type) {
        case 'text' : 
            return (
                <Form.Control
                    type={controlConfig.elementConfig.type}
                    value={controlConfig.value}
                    placeholder={controlConfig.elementConfig.placeholder}
                    onChange={(event) => props.changed(event, controlConfig.elementConfig.name)}
                    className={controlClasses.join(' ')}
                />
            );
        case 'select' :
            const options = controlConfig.elementConfig.options.map(option => {
                return <option key={option.value} value={option.value}>{option.displayValue}</option>;
            });
            return (
                <Form.Control
                    as={controlConfig.elementConfig.type}
                    value={controlConfig.value}
                    onChange={(event) => props.changed(event, controlConfig.elementConfig.name)}
                    className={controlClasses.join(' ')}
                > {options} </Form.Control>
            );
        case 'textarea' :
            return (
                <Form.Control
                    as={controlConfig.elementConfig.type}
                    rows={controlConfig.elementConfig.rows}
                    value={controlConfig.value}
                    placeholder={controlConfig.elementConfig.placeholder}
                    onChange={(event) => props.changed(event, controlConfig.elementConfig.name)}
                    className={controlClasses.join(' ')}
                />
            );
        case 'radio' :
            const radioOptions = controlConfig.elementConfig.options.map(option => {
                return <Form.Check 
                            key={option.value}
                            type={controlConfig.elementConfig.type}
                            className={controlClasses.join(' ')}
                            label={option.displayValue}
                            inline={controlConfig.elementConfig.inline}
                            value={option.value}
                            checked={controlConfig.value === option.value}
                            onChange={(event) => props.changed(event, controlConfig.elementConfig.name)}
                        />;
            });
            return radioOptions;
        case 'checkbox' :
            const checkOptions = controlConfig.elementConfig.options.map(option => {
                return <Form.Check 
                            key={option.value}
                            type={controlConfig.elementConfig.type}
                            className={controlClasses.join(' ')}
                            label={option.displayValue}
                            inline={controlConfig.elementConfig.inline}
                            value={option.value}
                            checked={controlConfig.value === option.value}
                            onChange={(event) => props.changed(event, controlConfig.elementConfig.name)}
                        />;
            });
            return checkOptions;            
        default : return null;
    }
}

export default formControl;