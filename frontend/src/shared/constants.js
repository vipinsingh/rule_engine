export const OPERATORS = {
    'AND' : '$and',
    'OR'  : '$or',
    'NOT' : '$not',
    '('   : '$open_bracket',
    ')'   : '$close_bracket',
};

export const CONDITIONS = {
    '='       : '$eq',
    '!='      : '$not_eq',
    'in'      : '$in',
    'not in'  : '$not_in',
    'is set'  : '$is_set',
    'is null' : '$is_null',
    'true'    : '$true',
    'false'   : '$false',
    '>'       : '$gt',
    '>='      : '$gte',
    '<'       : '$lt',
    '<='      : '$lte',
    'between' : '$between'
}

export const VALIDATIONS = {
    'string' : {
        required : true,
        maxLength : 600
    },
    'alpha_num' : {
        required : true,
        isAlphaNum : true,
        maxLength : 600
    },
    'number' : {
        required : true,
        isNumeric : true
    },
    'json' : {
        required : true,
        isJson : true
    }
}

export const FIELD_TYPE_CONFIG = {
    'string' : {
        type : 'text',
        value : ''
    },
    'number' : {
        type : 'text',
        value : ''
    },
    'boolean' : {
        type : 'select',
        value : '',
        options : [
            {
                value : '',
                displayValue : 'Select Value'
            },
            {
                value : true,
                displayValue : 'true'
            },
            {
                value : false,
                displayValue : 'false'
            },
            {
                value : null,
                displayValue : 'null'
            }
        ]
    }
}