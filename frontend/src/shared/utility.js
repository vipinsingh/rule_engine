import { VALIDATIONS } from "./constants";

export const updateObject = (oldObject, updatedProps) => {
    return {
        ...oldObject,
        ...updatedProps
    };
};

export const checkValidity = (value, rules) => {
    let isValid = true;

    if (!rules) {
        return true;
    }

    if (rules.required) {
        isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
        isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid;
    }

    if (rules.isEmail) {
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isAlphaNum) {
        const pattern = /^[\d\sa-zA-Z]+$/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isNumeric) {
        const pattern = /^[\d-.]+$/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isNumericPositive) {
        const pattern = /^[\d.]+$/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isNumericComma) {
        const pattern = /^[\d,-.]+$/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isJson) {
        isValid = isValidJson(value) && isValid
    }

    if (rules.isApiKey) {
        const pattern = /^[a-z_A-Z]+$/;
        isValid = pattern.test(value) && isValid
    }

    return isValid;
}

const isValidJson = (str) => {
    try {
        JSON.parse(str);

        if (str.indexOf('{') === -1 || str.indexOf('"') === -1) {
            return false;
        }

        return true;
    } catch (error) {
        return false;
    }
}

export const getDateTimeFromISO = (ISOstring) => {
    const date = new Date(ISOstring);
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();
    const time = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) +
        ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) +
        ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());

    if (dt < 10) {
        dt = '0' + dt;
    }
    if (month < 10) {
        month = '0' + month;
    }

    return (month + '-' + dt + '-' + year + ' ' + time);
}

/* eslint-disable */

export const validateExpression = (expression) => {
    if (expression.length < 2) {
        return false;
    }
    
    const { expString, valueObj } = getExpString(expression);

    // console.log('expstr:::', expString);

    // console.log('valueObj:::', valueObj);

    try {
        if(expString) {
            eval(expString);
            return true;
        }

        return false;
    } catch (error) {
        console.log('error:::', error);
        return false;
    }
}

/* eslint-enable */

const getExpString = (expression, valueObj = {}) => {
    let expString = '';

    const valueForFieldType = {
        'number': 10,
        'string': 'Teststring',
        'boolean': true
    }

    expression.forEach((exp, index) => {
        let str = null;

        switch (exp.control_type) {
            case 'attribute':
                valueObj[exp.displayValue] = valueForFieldType[exp.attribute_type];
                if(expression[index + 1] && expression[index + 1].control_type === 'mathOperators' && ['in', 'not in', 'between'].includes(expression[index + 1].displayValue)) {
                    break;
                }
                expString = expString + `valueObj["${exp.displayValue}"] `;
                break;

            case 'rule' :
                valueObj[exp.displayValue] = 'dummyValue';
                if(expression[index + 1] && expression[index + 1].control_type === 'mathOperators' && ['in', 'not in', 'between'].includes(expression[index + 1].displayValue)) {
                    break;
                }
                expString = expString + `valueObj["${exp.displayValue}"] `;
                break;

            case 'expression':
                expString = expString + getExpString(exp.value).expString
                valueObj = {
                    ...valueObj,
                    ...getExpString(exp.value).valueObj
                }
                break;

            case 'mathOperators':
                
                switch(exp.displayValue) {
                    case '^' :
                        str = '** ';
                        break;

                    case '=' :
                        str = '=== ';
                        break;

                    case '!=' :
                        str = '!== ';
                        break;
                    
                    case 'in' :
                        str = getExpStringForInNotInBtw(expression, index, exp.displayValue);
                        break;

                    case 'not in' :
                        str = getExpStringForInNotInBtw(expression, index, exp.displayValue);
                        break;

                    case 'is set' :
                        str = '!== undefined ';
                        break;

                    case 'is null' :
                        str = '=== null ';
                        break;

                    case 'between' :
                        str = getExpStringForInNotInBtw(expression, index, exp.displayValue);
                        break;
                        
                    case '= true' :
                        str = '=== true ';
                        break;

                    case '= false' :
                        str = '=== false ';
                        break;

                    default :
                        str = exp.displayValue + ' ';
                        break;
                }

                expString = expString + str;
                break;

            case 'constant' :
                if(expression[index - 1] && expression[index - 1].control_type === 'mathOperators' && ['in', 'not in', 'between'].includes(expression[index - 1].displayValue)) {
                    break;
                }
                if(findTypeOfConstant(exp.value) === 'string') {
                    str = `"${exp.value}" `
                } else {
                    str = exp.value + ' ';
                }
                expString = expString +  str;
                break;

            case 'operator' :
                
                switch(exp.displayValue) {
                    case 'AND' :
                        str = '&& ';
                        break;
                    
                    case 'OR' :
                        str = '|| ';
                        break;

                    case 'NOT' :
                        str = '!';
                        break;

                    default :
                        str = exp.displayValue + ' ';
                        break;
                }

                expString = expString + str;
                break;

            default: break;
        }
    })

    return {
        expString : expString,
        valueObj : valueObj
    };
}

const findTypeOfConstant = (constant) => {
    if(isNaN(constant)) {
        return 'string';
    } else {
        return 'number';
    }
}

const getExpStringForInNotInBtw = (expression, index, operator) => {
    let expString = null;
    let attributeName = null;
    let constantArr = null;

    if(expression[index + 1] && expression[index + 1].control_type === 'constant') {
        constantArr = expression[index + 1].value.split(',');
        constantArr = constantArr.map(constant => {
            constant = `"${constant}"`;
            return constant;
        })
    }

    if(expression[index - 1] && expression[index - 1].control_type === 'attribute') {
        attributeName = `valueObj["${expression[index - 1].displayValue}"]`;
    }

    if(expression[index - 1] && expression[index - 1].control_type === 'rule') {
        attributeName = `valueObj["${expression[index - 1].displayValue}"]`;
    }

    if(constantArr && attributeName) {
        switch(operator) {
            case 'in' :
                expString = `[${constantArr}].includes(${attributeName}) `;
                break;
    
            case 'not in' :
                expString = `![${constantArr}].includes(${attributeName}) `;
                break;
    
            case 'between' :
                expString = `${attributeName} >= ${constantArr[0]} && ${attributeName} <= ${constantArr[1] ? constantArr[1] : ''} `;
                break;
    
            default : break;
        }
    }

    return expString ? expString : '';
}

export const validateDecisionFieldAccordingToDecType = (controls, type, controlName) => {
    switch(type) {
        case 'string' : 
            controls = setDecisionValidation(controls, VALIDATIONS['string'], controlName);
            break;
        case 'number' : 
            controls = setDecisionValidation(controls, VALIDATIONS['number'], controlName);
            break;
        case 'alpha_num' :
            controls = setDecisionValidation(controls, VALIDATIONS['alpha_num'], controlName);
            break;
        case 'json' :
            controls = setDecisionValidation(controls, VALIDATIONS['json'], controlName);
            break;
        case 'compute' : 
            controls = setDecisionValidation(controls, false, controlName);
            break;
        default : break;
    }

    return controls;
}

const setDecisionValidation = (controls, validationObj, controlName) => {
    controls = updateObject(controls, {
        [controlName] : updateObject(controls[controlName], {
            validation  : validationObj,
            valid : checkValidity(controls[controlName].value, validationObj),
            touched : controls[controlName].value ? true : false
        })
    });

    return controls;
}