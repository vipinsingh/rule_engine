import React, { Component } from 'react';
import './App.css';
import { Route, Switch, Redirect } from 'react-router-dom';
import SignIn from './containers/Auth/SignIn/SignIn';
import asyncComponent from './hoc/asyncComponent/asyncComponent';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';
import * as actions from './store/actions/index';
import Logout from './containers/Auth/Logout/Logout';

toast.configure({
  draggable : false,
  autoClose : 4000,
  position : toast.POSITION.TOP_RIGHT,
  hideProgressBar : true,
  pauseOnHover : false
});

const asyncSignUp = asyncComponent(() =>  {
  return import('./containers/Auth/SignUp/SignUp');
});

const asyncHome = asyncComponent(() =>  {
  return import('./containers/Home/Home');
});

class App extends Component {

  constructor(props) {
    super(props);
    if(props.redirectPath === null) {
      props.onSetAuthRedirectPath(window.location.pathname);
    }
  }

  componentDidMount() {
    this.props.checkAuthState();
  }

  render() {
    let routes = (
      <Switch>
        <Route path="/" exact component={SignIn} />
        <Route path="/signup" component={asyncSignUp} />
        <Redirect to="/" />
      </Switch>
    );

    if(this.props.isAuth) {
      routes = (
        <Switch>
          <Route path="/logout" component={Logout} />
          <Route path="/projects" component={asyncHome} />
          { this.props.redirectPath && this.props.redirectPath !== '/' ?
            <Redirect to={this.props.redirectPath} /> :
            <Redirect to="/projects" />
          }
        </Switch>
      );
    }

    return (routes);
  }
}

const mapStateToProps = state => {
  return {
    isAuth : state.auth.token !== null,
    redirectPath : state.auth.authRedirectPath
  }
}

const mapDispatchToProps = dispatch => {
  return {
    checkAuthState : () => dispatch(actions.authCheckState()),
    onSetAuthRedirectPath : (path) => dispatch(actions.setAuthRedirectPath(path))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
