import * as actionTypes from './actionTypes';
import axios from '../../axiosInstance';

export const fetchProjectsStart = () => {
    return {
        type : actionTypes.FETCH_PROJECTS_START
    };
};

export const fetchProjectsSuccess = (projects) => {
    return {
        type : actionTypes.FETCH_PROJECTS_SUCCESS,
        projects : projects
    };
};

export const fetchProjectsFail = (error) => {
    return {
        type : actionTypes.FETCH_PROJECTS_FAIL,
        error : error
    }
}

export const resetError = () => {
    return {
        type : actionTypes.RESET_ERROR
    }
}

export const fetchProjects = (user_id, pageNo = 1, perPage = 15) => {
    return dispatch => {
        dispatch(fetchProjectsStart());

        axios.get('/api/v1/projects', {
            params : { 
                user_id : user_id,
                page_no : pageNo,
                per_page : perPage
            }
        })
            .then(res => {
                let projects = null;
                if(res.data && res.data.data) {
                    projects = res.data.data;
                }
                
                dispatch(fetchProjectsSuccess(projects));
                if(localStorage.getItem('currentProj')) {
                    const curr_proj = JSON.parse(localStorage.getItem('currentProj'));
                    dispatch(setCurrentProject(curr_proj.id));
                }
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(fetchProjectsFail(error));
            });
    }
}

export const createProjectStart = () => {
    return {
        type : actionTypes.CREATE_PROJECT_START
    };
};

export const createProjectSuccess = (project) => {
    return {
        type : actionTypes.CREATE_PROJECT_SUCCESS,
        project : project
    };
};

export const createProjectFail = (error) => {
    return {
        type : actionTypes.CREATE_PROJECT_FAIL,
        error : error
    }
}

export const createProject = (projectData) => {
    return dispatch => {
        dispatch(createProjectStart());

        axios.post('/api/v1/projects', projectData)
            .then(res => {
                let project = null;
                if(res.data && res.data.data) {
                    project = res.data.data;
                }
                if(!project) {
                    dispatch(createProjectFail('Could not create project'));
                }
                dispatch(createProjectSuccess(project));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(createProjectFail(error));
            })
    }
}

export const setCurrentProject = (projectId, projectData = null) => {
    return {
        type : actionTypes.SET_CURRENT_PROJECT,
        projectId : projectId,
        data : projectData
    };
};

export const unsetCurrentProject = () => {
    return {
        type : actionTypes.UNSET_CURRENT_PROJECT
    };
};