import * as actionTypes from './actionTypes';
import axios from '../../axiosInstance';
import md5 from 'md5';

export const authStart = () => {
    return {
        type : actionTypes.AUTH_START
    };
};

export const authSuccess = (token, userData) => {
    return {
        type : actionTypes.AUTH_SUCCESS,
        token : token,
        userData : userData
    };
};

export const authFail = (error) => {
    return {
        type : actionTypes.AUTH_FAIL,
        error : error
    };
};

export const logoutSuccess = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('userData');
    localStorage.removeItem('currentProj');
    localStorage.removeItem('userId');
    localStorage.removeItem('expirationDate');
    return {
        type : actionTypes.AUTH_LOGOUT_SUCCESS
    };
};

export const logoutFail = (error) => {
    return {
        type : actionTypes.AUTH_LOGOUT_FAIL,
        error : error
    };
};

export const logoutStart = () => {
    return {
        type : actionTypes.AUTH_LOGOUT_START
    };
};

export const authLogout = () => {
    return dispatch => {
        dispatch(logoutStart());
        const userData = JSON.parse(localStorage.getItem('userData'));
        const logoutData = {
            user_id : userData.user_id,
            token : localStorage.getItem('token')
        };
        axios.post('/api/v1/user/logout', logoutData)
            .then(res => {
                dispatch(logoutSuccess());
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(logoutFail(error));
            });
    }
}

export const auth = (email, password) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email : email,
            password : md5(password)
        };

        axios.post('/api/v1/user/signin', authData)
            .then(res => {
                const data = res.data.data;
                localStorage.setItem('token', data.session_token);
                const userData = {
                    user_id : data.user_id,
                    userName : data.user_name,
                    email : data.email
                };
                localStorage.setItem('userData', JSON.stringify(userData));
                dispatch(authSuccess(data.session_token, userData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(authFail(error));
            });
    }
}

export const setAuthRedirectPath = (path) => {
    return {
        type : actionTypes.SET_AUTH_REDIRECT_PATH,
        path : path
    }
}

export const resetAuth = () => {
    return {
        type : actionTypes.RESET_AUTH
    }
}

export const authCheckState = () => {
    return dispatch => {
        dispatch(authStart());
        const token = localStorage.getItem('token');
        const userData = JSON.parse(localStorage.getItem('userData'));
        if(!token || Object.keys(userData).length === 0) {
            dispatch(logoutSuccess());
        } else {
            axios.post('/api/v1/user/auth_status', {
                user_id : userData.user_id,
                token : token
            })
            .then(res => {
                if(!res.data.data) dispatch(logoutSuccess());
                dispatch(authSuccess(token, userData));
            })
            .catch(err => {
                console.log('errr:::', err);
                dispatch(logoutSuccess());
            })
        }
    }
}