import * as actionTypes from './actionTypes';
import axios from '../../axiosInstance';

export const addField = (fieldData) => {
    return {
        type : actionTypes.ADD_FIELD,
        fieldData : fieldData
    };
};

export const addRule = () => {
    return {
        type : actionTypes.ADD_RULE
    }
}

export const addCondition = (ruleId, condition) => {
    return {
        type : actionTypes.ADD_CONDITION,
        ruleId : ruleId,
        conditionData : condition
    }
}

export const editRule = (ruleId, editedData) => {
    return {
        type : actionTypes.EDIT_RULE,
        ruleId : ruleId,
        data : editedData
    }
}

export const deleteRule = (ruleId) => {
    return {
        type : actionTypes.DELETE_RULE,
        ruleId : ruleId
    }
}

export const copyRule = (ruleId) => {
    return {
        type : actionTypes.COPY_RULE,
        ruleId : ruleId
    }
}

export const editField = (fieldId, editedData) => {
    return {
        type : actionTypes.EDIT_FIELD,
        fieldId : fieldId,
        data : editedData
    }
}

export const deleteField = (fieldId) => {
    return {
        type : actionTypes.DELETE_FIELD,
        fieldId : fieldId
    }
}

export const editCondition = (ruleId, editedData) => {
    return {
        type : actionTypes.EDIT_CONDITION,
        ruleId : ruleId,
        data : editedData
    }
}

export const createTableStart = () => {
    return {
        type : actionTypes.CREATE_TABLE_START
    }
}

export const createTableFail = (error) => {
    return {
        type : actionTypes.CREATE_TABLE_FAIL,
        error : error
    }
}

export const createTableSuccess = (tableData) => {
    return {
        type : actionTypes.CREATE_TABLE_SUCCESS,
        tableData : tableData
    }
}

export const createTable = (tableInfo, user_id, project_id) => {
    return dispatch => {
        dispatch(createTableStart());

        const data = {
            user_id,
            project_id,
            ...tableInfo
        }

        axios.post('/api/v1/tables', data)
            .then(res => {
                let tableData = null;
                if(res.data && res.data.data) {
                    tableData = res.data.data;
                }
                if(!tableData) {
                    dispatch(createTableFail('Could not create table!'));
                }
                dispatch(createTableSuccess(tableData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(createTableFail(error));
            })
    }
}

export const errorReset = () => {
    return {
        type : actionTypes.RESET_ERROR
    }
}

export const setViewMode = (mode) => {
    return {
        type : actionTypes.SET_VIEW_MODE,
        mode : mode
    }
}

export const fetchTableStart = () => {
    return {
        type : actionTypes.FETCH_TABLE_START
    }
}

export const fetchTableFail = (error) => {
    return {
        type :  actionTypes.FETCH_TABLE_FAIL,
        error : error
    }
}

export const fetchTableSuccess = (tableData) => {
    return {
        type : actionTypes.FETCH_TABLE_SUCCESS,
        tableData : tableData
    }
}

export const fetchTableDetails = (tableId, user_id, project_id) => {
    return dispatch => {
        dispatch(fetchTableStart());

        axios.get(`api/v1/tables/${tableId}`, { params : { user_id, project_id }})
            .then(res => {
                let tableData = null;
                if(res.data && res.data.data) {
                    tableData = res.data.data;
                }

                if(!tableData || tableData.length === 0) {
                    dispatch(fetchTableFail('Could not fetch table!'));
                }
                dispatch(fetchTableSuccess(tableData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(fetchTableFail(error));
            })
    }
}

export const listTablesStart = () => {
    return {
        type : actionTypes.LIST_TABLES_START
    }
}

export const listTablesFail = (error) => {
    return {
        type : actionTypes.LIST_TABLES_FAIL,
        error : error
    }
}

export const listTablesSuccess = (tables) => {
    return {
        type : actionTypes.LIST_TABLES_SUCCESS,
        tables : tables
    }
}

export const listTables = (user_id, project_id, page_no = 1, per_page = 15) => {
    return dispatch => {
        dispatch(listTablesStart());

        axios.get('/api/v1/tables', { params : { user_id, project_id, page_no, per_page }})
            .then(res => {
                let tables = null;
                if(res.data && res.data.data) {
                    tables = res.data.data;
                }

                dispatch(listTablesSuccess(tables));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(listTablesFail(error));
            })
    }
}

export const clearTableState = () => {
    return {
        type : actionTypes.CLEAR_TABLE_STATE
    }
}

export const getDecisionStart = () => {
    return {
        type : actionTypes.GET_DECISION_START
    }
}

export const getDecisionFail = (error) => {
    return {
        type : actionTypes.GET_DECISION_FAIL,
        error : error
    }
}

export const getDecisionSuccess = (decision) => {
    return {
        type : actionTypes.GET_DECISION_SUCCESS,
        decision : decision
    }
}

export const getDecision = (params) => {
    return dispatch => {
        dispatch(getDecisionStart());

        axios.post('/api/v1/decisions', params)
            .then(res => {
                let decision = null;
                if(res.data && res.data.data) {
                    decision = res.data.data;
                }

                dispatch(getDecisionSuccess(decision));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(getDecisionFail(error));
            })
    }
}

export const listDecisionsStart = () => {
    return {
        type : actionTypes.LIST_DECISIONS_START
    }
}

export const listDecisionsFail = (error) => {
    return {
        type : actionTypes.LIST_DECISIONS_FAIL,
        error : error
    }
}

export const listDecisionsSuccess = (decisions) => {
    return {
        type : actionTypes.LIST_DECISIONS_SUCCESS,
        decisions : decisions
    }
}

export const listDecisions = (params, page_no = 1, per_page = 15) => {
    return dispatch => {
        dispatch(listDecisionsStart());

        axios.get('/api/v1/decisions', { params : {...params, page_no, per_page} })
            .then(res => {
                let decision = null;
                if(res.data && res.data.data) {
                    decision = res.data.data;
                }

                dispatch(listDecisionsSuccess(decision));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(listDecisionsFail(error));
            })
    }
}

export const getDecisionDetailsStart = () => {
    return {
        type : actionTypes.GET_DECISION_DETAILS_START
    }
}

export const getDecisionDetailsFail = (error) => {
    return {
        type : actionTypes.GET_DECISION_DETAILS_FAIL,
        error : error
    }
}

export const getDecisionDetailsSuccess = (decision) => {
    return {
        type : actionTypes.GET_DECISION_DETAILS_SUCCESS,
        decision : decision
    }
}

export const getDecisionDetails = (decId, params) => {
    return dispatch => {
        dispatch(getDecisionDetailsStart());

        axios.get('/api/v1/decisions/' + decId , { params : params })
            .then(res => {
                let decision = null;
                if(res.data && res.data.data) {
                    decision = res.data.data;
                }

                dispatch(getDecisionDetailsSuccess(decision));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(getDecisionDetailsFail(error));
            })
    }
}

export const updateTableStart = () => {
    return {
        type : actionTypes.UPDATE_TABLE_START
    }
}

export const updateTableFail = (error) => {
    return {
        type : actionTypes.UPDATE_TABLE_FAIL,
        error : error
    }
}

export const updateTableSuccess = (tableData) => {
    return {
        type : actionTypes.UPDATE_TABLE_SUCCESS,
        tableData : tableData
    }
}

export const updateTable = (tableInfo, user_id, project_id, table_id) => {
    return dispatch => {
        dispatch(updateTableStart());

        const data = {
            user_id,
            project_id,
            ...tableInfo
        }

        axios.put('/api/v1/tables/' + table_id, data)
            .then(res => {
                let tableData = null;
                if(res.data && res.data.data) {
                    tableData = res.data.data;
                }
                if(!tableData) {
                    dispatch(updateTableFail('Could not update table!'));
                }
                dispatch(updateTableSuccess(tableData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(updateTableFail(error));
            })
    }
}