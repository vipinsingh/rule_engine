import * as actionTypes from './actionTypes';
import axios from '../../axiosInstance';

export const addToRuleExpression = (ruleExpression, control) => {
    return {
        type : actionTypes.ADD_TO_RULE_EXPRESSION,
        control : control,
        ruleExpression : ruleExpression
    }
}

export const editRuleExpression = (ruleExpression, controlId, controlObj) => {
    return {
        type : actionTypes.EDIT_RULE_EXPRESSION,
        controlId : controlId,
        controlObj : controlObj,
        ruleExpression : ruleExpression
    }
}

export const deleteRuleExpression = (ruleExpression, controlId) => {
    return {
        type : actionTypes.DELETE_RULE_EXPRESSION,
        controlId : controlId,
        ruleExpression : ruleExpression
    }
}

export const reorderRuleExpression = (ruleExpression, startIndex, endIndex) => {
    return {
        type : actionTypes.REORDER_RULE_EXPRESSION,
        start :  startIndex,
        end : endIndex,
        ruleExpression : ruleExpression
    }
}

const saveExpressionStart = () => {
    return {
        type : actionTypes.SAVE_EXPRESSION_START
    }
};

const saveExpressionFail = (error) => {
    return {
        type : actionTypes.SAVE_EXPRESSION_FAIL,
        error : error
    }
}

const saveExpressionSuccess = (expData) => {
    return {
        type : actionTypes.SAVE_EXPRESSION_SUCCESS,
        data : expData
    }
}

export const saveExpression = (expData) => {
    return dispatch => {
        dispatch(saveExpressionStart());

        axios.post('/api/v1/rules/expressions', expData)
            .then(res => {
                let expData = null;
                if(res.data && res.data.data) {
                    expData = res.data.data;
                }
                if(!expData) {
                    dispatch(saveExpressionFail('Could not save expression!'));
                }
                dispatch(saveExpressionSuccess(expData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(saveExpressionFail(error));
            });
    }
}

const getExpressionsStart = () => {
    return {
        type : actionTypes.GET_EXPRESSIONS_START
    }
};

const getExpressionsFail = (error) => {
    return {
        type : actionTypes.GET_EXPRESSIONS_FAIL,
        error : error
    }
}

const getExpressionsSuccess = (expData, expId, ruleExpression) => {
    return {
        type : actionTypes.GET_EXPRESSIONS_SUCCESS,
        data : expData,
        controlId : expId,
        ruleExpression : ruleExpression
    }
}

export const getExpressions = (params, expId, ruleExpression) => {
    return dispatch => {
        dispatch(getExpressionsStart());

        axios.get('/api/v1/rules/expressions', {params : params})
            .then(res => {
                let expData = null;
                if(res.data && res.data.data) {
                    expData = res.data.data;
                }
                if(!expData) {
                    dispatch(getExpressionsFail('Could not get expression!'));
                }

                dispatch(getExpressionsSuccess(expData, expId, ruleExpression));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(getExpressionsFail(error));
            });
    }
}

export const resetRuleError = () => {
    return {
        type : actionTypes.RESET_RULE_ERROR
    }
}

export const resetExpressionSaved = () => {
    return {
        type : actionTypes.RESET_EXPRESSION_SAVED
    }
}

export const setRuleViewMode = (mode) => {
    return {
        type : actionTypes.SET_VIEW_MODE,
        mode : mode
    }
}

const listRulesStart = () => {
    return {
        type : actionTypes.LIST_RULES_START
    }
}

const listRulesFail = (error) => {
    return {
        type : actionTypes.LIST_RULES_FAIL,
        error : error
    }
}

const listRulesSuccess = (data) => {
    return {
        type : actionTypes.LIST_RULES_SUCCESS,
        data : data
    }
}

export const listRules = (params) => {
    return dispatch => {
        dispatch(listRulesStart());

        axios.get('/api/v1/rules', {params : params})
            .then(res => {
                let ruleList = null;
                if(res.data && res.data.data) {
                    ruleList = res.data.data;
                }

                dispatch(listRulesSuccess(ruleList));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(listRulesFail(error));
            });
    }
}

const getRuleStart = () => {
    return {
        type : actionTypes.GET_RULE_START
    }
}

const getRuleFail = (error) => {
    return {
        type : actionTypes.GET_RULE_FAIL,
        error : error
    }
}

const getRuleSuccess = (data) => {
    return {
        type : actionTypes.GET_RULE_SUCCESS,
        data : data
    }
}

export const getRule = (ruleId, params) => {
    return dispatch => {
        dispatch(getRuleStart());

        axios.get('/api/v1/rules/' + ruleId, {params : params})
            .then(res => {
                let ruleData = null;
                if(res.data && res.data.data) {
                    ruleData = res.data.data;
                }

                if(!ruleData || ruleData.length === 0) {
                    dispatch(getRuleFail('Could not fetch rule!'));
                }

                dispatch(getRuleSuccess(ruleData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(getRuleFail(error));
            });
    }
}

const saveRuleStart = () => {
    return {
        type : actionTypes.SAVE_RULE_START
    }
}

const saveRuleFail = (error) => {
    return {
        type : actionTypes.SAVE_RULE_FAIL,
        error : error
    }
}

const saveRuleSuccess = (data) => {
    return {
        type : actionTypes.SAVE_RULE_SUCCESS,
        data : data
    }
}

export const saveRule = (data) => {
    return dispatch => {
        dispatch(saveRuleStart());

        axios.post('/api/v1/rules', data)
            .then(res => {
                let ruleData = null;
                if(res.data && res.data.data) {
                    ruleData = res.data.data;
                }
                if(!ruleData) {
                    dispatch(saveRuleFail('Could not save rule!'));
                }
                dispatch(saveRuleSuccess(ruleData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(saveRuleFail(error));
            });
    }
}

const editRuleStart = () => {
    return {
        type : actionTypes.EDIT_RULE_START
    }
}

const editRuleFail = (error) => {
    return {
        type : actionTypes.EDIT_RULE_FAIL,
        error : error
    }
}

const editRuleSuccess = (data) => {
    return {
        type : actionTypes.EDIT_RULE_SUCCESS,
        data : data
    }
}

export const editDecisionRule = (ruleId, data) => {
    return dispatch => {
        dispatch(editRuleStart());

        axios.put('/api/v1/rules/' + ruleId, data)
            .then(res => {
                let ruleData = null;
                if(res.data && res.data.data) {
                    ruleData = res.data.data;
                }
                if(!ruleData) {
                    dispatch(editRuleFail('Could not edit rule!'));
                }
                dispatch(editRuleSuccess(ruleData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(editRuleFail(error));
            });
    }
}

export const clearRuleState = () => {
    return {
        type : actionTypes.CLEAR_RULE_STATE
    }
}

const getRuleDecisionStart = () => {
    return {
        type : actionTypes.GET_RULE_DECISION_START
    }
}

const getRuleDecisionFail = (error) => {
    return {
        type : actionTypes.GET_RULE_DECISION_FAIL,
        error : error
    }
}

const getRuleDecisionSuccess = (data) => {
    return {
        type : actionTypes.GET_RULE_DECISION_SUCCESS,
        data : data
    }
}

export const getRuleDecision = (ruleId, data) => {
    return dispatch => {
        dispatch(getRuleDecisionStart());

        axios.post('/api/v1/rules/'+ruleId+'/decision', data)
            .then(res => {
                let ruleData = null;
                if(res.data && res.data.data) {
                    ruleData = res.data.data;
                }
                if(!ruleData) {
                    dispatch(getRuleDecisionFail('Could not get decision!'));
                }
                dispatch(getRuleDecisionSuccess(ruleData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(getRuleDecisionFail(error));
            });
    }
}