import * as actionTypes from './actionTypes';
import axios from '../../axiosInstance';

export const addOpToRuleSet = (operator) => {
    return {
        type : actionTypes.ADD_OP_TO_RULE_SET,
        operator : operator
    }
}

export const deleteOpRuleSet = (opId) => {
    return {
        type : actionTypes.DELETE_OP_RULE_SET,
        opId : opId
    }
}

export const editOpRuleSet = (opId, data) => {
    return {
        type : actionTypes.EDIT_OP_RULE_SET,
        opId : opId,
        data : data
    }
}

export const addExpToRuleSetOp = (opId, exp_name, expression) => {
    return {
        type : actionTypes.ADD_EXP_TO_RULE_SET_OP,
        opId : opId,
        expName : exp_name,
        expression : expression
    }
}

export const editExpRuleSetOp = (opId, expId, exp_name, expression) => {
    return {
        type : actionTypes.EDIT_EXP_RULE_SET_OP,
        opId : opId,
        expId : expId,
        expName : exp_name,
        expression : expression
    }
}

export const deleteExpRuleSetOp = (opId, expId, exp_name) => {
    return {
        type : actionTypes.DELETE_EXP_RULE_SET_OP,
        opId : opId,
        expId : expId,
        expName : exp_name
    }
}

export const reorderExpRuleSetOp = (opId, expName, startIndex, endIndex) => {
    return {
        type : actionTypes.REORDER_EXP_RULE_SET_OP,
        opId : opId,
        start :  startIndex,
        end : endIndex,
        expName : expName
    }
}

const getRuleSuggestionsStart = () => {
    return {
        type : actionTypes.GET_RULE_SUGGESTIONS_START
    }
}

const getRuleSuggestionsFail = (error) => {
    return {
        type : actionTypes.GET_RULE_SUGGESTIONS_FAIL,
        error : error
    }
}

const getRuleSuggestionsSuccess = (data, opId, expId) => {
    return {
        type : actionTypes.GET_RULE_SUGGESTIONS_SUCCESS,
        data : data,
        opId : opId,
        expId : expId
    }
}

export const getRuleSuggestions = (params, opId, expId, ) => {
    return dispatch => {
        dispatch(getRuleSuggestionsStart());

        axios.get('/api/v1/rules/rule_suggestions', { params : params })
            .then(res => {
                let ruleData = null;
                if(res.data && res.data.data) {
                    ruleData = res.data.data;
                }
                if(!ruleData) {
                    dispatch(getRuleSuggestionsFail('Could not get expression!'));
                }

                dispatch(getRuleSuggestionsSuccess(ruleData, opId, expId));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(getRuleSuggestionsFail(error));
            });
    }
}

export const resetRuleSetError = () => {
    return {
        type : actionTypes.RESET_RULE_SET_ERROR
    }
}

export const setRuleSetViewMode = (mode) => {
    return {
        type : actionTypes.SET_VIEW_MODE,
        mode : mode
    }
}

export const clearRuleSetState = () => {
    return {
        type : actionTypes.CLEAR_RULE_SET_STATE
    }
}

const saveRuleSetStart = () => {
    return {
        type : actionTypes.SAVE_RULE_SET_START
    }
}

const saveRuleSetFail = (error) => {
    return {
        type : actionTypes.SAVE_RULE_SET_FAIL,
        error : error
    }
}

const saveRuleSetSuccess = (data) => {
    return {
        type : actionTypes.SAVE_RULE_SET_SUCCESS,
        data : data
    }
}

export const saveRuleSet = (data) => {
    return dispatch => {
        dispatch(saveRuleSetStart());

        axios.post('/api/v1/rule_set', data)
            .then(res => {
                let ruleSetData = null;
                if(res.data && res.data.data) {
                    ruleSetData = res.data.data;
                }
                if(!ruleSetData) {
                    dispatch(saveRuleSetFail('Could not save rule!'));
                }
                dispatch(saveRuleSetSuccess(ruleSetData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(saveRuleSetFail(error));
            });
    }
}

const getRuleSetStart = () => {
    return {
        type : actionTypes.GET_RULE_SET_START
    }
}

const getRuleSetFail = (error) => {
    return {
        type : actionTypes.GET_RULE_SET_FAIL,
        error : error
    }
}

const getRuleSetSuccess = (data) => {
    return {
        type : actionTypes.GET_RULE_SET_SUCCESS,
        data : data
    }
}

export const getRuleSet = (ruleSetId, params) => {
    return dispatch => {
        dispatch(getRuleSetStart());

        axios.get('/api/v1/rule_set/' + ruleSetId, { params : params })
            .then(res => {
                let ruleSetData = null;
                if(res.data && res.data.data) {
                    ruleSetData = res.data.data;
                }
                if(!ruleSetData) {
                    dispatch(getRuleSetFail('Could not save rule!'));
                }
                dispatch(getRuleSetSuccess(ruleSetData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(getRuleSetFail(error));
            });
    }
}

const listRuleSetStart = () => {
    return {
        type : actionTypes.LIST_RULE_SET_START
    }
}

const listRuleSetFail = (error) => {
    return {
        type : actionTypes.LIST_RULE_SET_FAIL,
        error : error
    }
}

const listRuleSetSuccess = (data) => {
    return {
        type : actionTypes.LIST_RULE_SET_SUCCESS,
        data : data
    }
}

export const listRuleSet = (params) => {
    return dispatch => {
        dispatch(listRuleSetStart());

        axios.get('/api/v1/rule_set', { params : params })
            .then(res => {
                let ruleSetData = null;
                if(res.data && res.data.data) {
                    ruleSetData = res.data.data;
                }
                if(!ruleSetData) {
                    dispatch(listRuleSetFail('Could not save rule!'));
                }
                dispatch(listRuleSetSuccess(ruleSetData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(listRuleSetFail(error));
            });
    }
}

const updateRuleSetStart = () => {
    return {
        type : actionTypes.UPDATE_RULE_SET_START
    }
}

const updateRuleSetFail = (error) => {
    return {
        type : actionTypes.UPDATE_RULE_SET_FAIL,
        error : error
    }
}

const updateRuleSetSuccess = (data) => {
    return {
        type : actionTypes.UPDATE_RULE_SET_SUCCESS,
        data : data
    }
}

export const updateRuleSet = (ruleSetId, data) => {
    return dispatch => {
        dispatch(updateRuleSetStart());

        axios.put('/api/v1/rule_set/' + ruleSetId, data)
            .then(res => {
                let ruleSetData = null;
                if(res.data && res.data.data) {
                    ruleSetData = res.data.data;
                }
                if(!ruleSetData) {
                    dispatch(updateRuleSetFail('Could not save rule!'));
                }
                dispatch(updateRuleSetSuccess(ruleSetData));
            })
            .catch(err => {
                console.log('errr:::', err);
                let error = (err.response && err.response.data) || 'An error occurred!';
                dispatch(updateRuleSetFail(error));
            });
    }
}