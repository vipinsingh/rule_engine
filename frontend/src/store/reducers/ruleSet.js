import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    rule_set_list : null,
    rule_set_info : null,
    rule_set_exp : [{
        id : 1,
        operator : '$if',
        displayValue : 'IF',
        rule_expression : [],
        then_decision : null,
        decision_type : null,
        decision_exp : []
    }],
    rule_suggestions : {},
    loading : false,
    error : null,
    viewMode : 'create',
    isRuleSetCreated : false,
};

const addOpToRuleSet = (state, action) => {
    const updatedRuleSetExp = state.rule_set_exp.concat({
        id : state.rule_set_exp.length + 1,
        ...action.operator
    })

    return updateObject(state, {
        rule_set_exp : updatedRuleSetExp
    })
}

const editOpRuleSet = (state, action) => {
    const updatedRuleSetExp = state.rule_set_exp.map(exp => {
        if(exp.id === action.opId) {
            exp = updateObject(exp, {
                ...action.data
            })
        }
        return exp;
    });

    return updateObject(state, {
        rule_set_exp : updatedRuleSetExp
    })
}

const deleteOpRuleSet = (state, action) => {
    let updatedRuleSetExp = state.rule_set_exp.filter(exp => {
        if(exp.id === action.opId) {
            return false;
        }
        return true;
    });

    updatedRuleSetExp = updatedRuleSetExp.map((exp,index) => {
        exp.id = index + 1;

        return exp;
    })

    return updateObject(state, {
        rule_set_exp : updatedRuleSetExp
    })
}

const addExpToRuleSetOp = (state, action) => {
    const updatedRuleSetExp = state.rule_set_exp.map(exp => {
        let updatedExp = exp;
        let updatedRuleExp = exp[action.expName];
        if(exp.id === action.opId) {
            updatedRuleExp = updatedRuleExp.concat({
                id : updatedRuleExp.length + 1,
                ...action.expression
            });

            updatedExp = updateObject(updatedExp, {
                [action.expName] : updatedRuleExp
            })
        }
        return updatedExp;
    });

    return updateObject(state, {
        rule_set_exp : updatedRuleSetExp
    })
}

const editExpToRuleSetOp = (state, action) => {
    const updatedRuleSetExp = state.rule_set_exp.map(exp => {
        let updatedExp = exp;
        let updatedRuleExp = exp[action.expName];
        if(exp.id === action.opId) {
            updatedRuleExp = updatedRuleExp.map(ruleExp => {
                if(ruleExp.id === action.expId) {
                    ruleExp = updateObject(ruleExp, {
                        ...action.expression
                    })
                }
                return ruleExp;
            })

            updatedExp = updateObject(updatedExp, {
                [action.expName] : updatedRuleExp
            })
        }
        return updatedExp;
    });

    return updateObject(state, {
        rule_set_exp : updatedRuleSetExp
    })
}

const deleteExpToRuleSetOp = (state, action) => {
    const updatedRuleSetExp = state.rule_set_exp.map(exp => {
        let updatedExp = exp;
        let updatedRuleExp = exp[action.expName];
        if(exp.id === action.opId) {
            updatedRuleExp = updatedRuleExp.filter(ruleExp => {
                if(ruleExp.id === action.expId) {
                    return false
                }
                return true;
            })

            updatedRuleExp = updatedRuleExp.map((ruleExp, index) => {
                ruleExp = updateObject(ruleExp, {
                    id : index + 1
                })
                return ruleExp;
            })

            updatedExp = updateObject(updatedExp, {
                [action.expName] : updatedRuleExp
            })
        }
        return updatedExp;
    });

    return updateObject(state, {
        rule_set_exp : updatedRuleSetExp
    })
}

const reorderExpToRuleSetOp = (state, action) => {

    const updatedRuleSetExp = state.rule_set_exp.map(exp => {
        let updatedExp = exp;
        let updatedRuleExp = exp[action.expName];
        if(exp.id === action.opId) {
            const selectedExp = {...updatedRuleExp[action.start]};
            updatedRuleExp = [
                ...updatedRuleExp.slice(0, action.start),
                ...updatedRuleExp.slice(action.start + 1)
            ];
        
            updatedRuleExp.splice(action.end, 0, selectedExp);
        
            updatedRuleExp = updatedRuleExp.map((exp,index) => {
                const updatedId = index + 1;
                const updatedExpRule = updateObject(exp, {
                    id : updatedId
                });
                return updatedExpRule;
            });
        
            updatedExp =  updateObject(updatedExp, {
                [action.expName] : updatedRuleExp
            });
        }
        return updatedExp;
    });

    return updateObject(state, {
        rule_set_exp : updatedRuleSetExp
    })
}

const getRuleSuggestionsStart = (state, action) => {
    return updateObject(state, {
        error : null
    })
}

const getRuleSuggestionsFail = (state, action) => {
    return updateObject(state, {
        error : action.error
    })
}

const getRuleSuggestionsSuccess = (state, action) => {
    const updatedRuleSuggestions = updateObject(state.rule_suggestions, {
        [action.opId] : updateObject(state.rule_suggestions[action.opId], {
            [action.expId] : action.data
        })
    });

    return updateObject(state, {
        error : null,
        rule_suggestions : updatedRuleSuggestions
    })
}

const saveRuleSetStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    })
}

const saveRuleSetFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    })
}

const saveRuleSetSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        rule_set_info : {
            id : action.data.id,
            name : action.data.name,
            description : action.data.description,
        },
        rule_set_exp : action.data.rule_set_exp,
        viewMode : 'view',
        isRuleSetCreated : true
    })    
}

const listRuleSetStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    })
}

const listRuleSetFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    })
}

const listRuleSetSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        rule_set_list : action.data
    })
}

const getRuleSetStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    })
}

const getRuleSetFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    })
}

const getRuleSetSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        rule_set_info : {
            id : action.data.id,
            name : action.data.name,
            description : action.data.description,
        },
        rule_set_exp : action.data.rule_set_exp,
        viewMode : 'view'
    })
}

const updateRuleSetStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    })
}

const updateRuleSetFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    })
}

const updateRuleSetSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        rule_set_info : {
            id : action.data.id,
            name : action.data.name,
            description : action.data.description,
        },
        rule_set_exp : action.data.rule_set_exp,
        viewMode : 'view',
        isRuleSetCreated : true
    })
}

const resetRuleSetError = (state, action) => {
    return updateObject(state, {
        error : null
    })
}

const clearRuleSetState = (state, action) => {
    return updateObject(state, {
        rule_set_list : null,
        rule_set_info : null,
        rule_set_exp : [{
            id : 1,
            operator : '$if',
            displayValue : 'IF',
            rule_expression : [],
            then_decision : null,
            decision_type : null,
            decision_exp : []
        }],
        rule_suggestions : {},
        loading : false,
        error : null,
        viewMode : 'create',
        isRuleSetCreated : false,
    })
}

const setRuleSetViewMode = (state, action) => {
    const updatedState = {
        viewMode : action.mode
    };

    if(action.mode === 'edit') {
        updatedState['isRuleSetCreated'] = false;
    }

    return updateObject(state, updatedState);
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.ADD_OP_TO_RULE_SET :
            return addOpToRuleSet(state, action);
        case actionTypes.EDIT_OP_RULE_SET :
            return editOpRuleSet(state, action);
        case actionTypes.DELETE_OP_RULE_SET :
            return deleteOpRuleSet(state, action);
        case actionTypes.ADD_EXP_TO_RULE_SET_OP :
            return addExpToRuleSetOp(state, action);
        case actionTypes.EDIT_EXP_RULE_SET_OP :
            return editExpToRuleSetOp(state, action);
        case actionTypes.DELETE_EXP_RULE_SET_OP :
            return deleteExpToRuleSetOp(state, action);
        case actionTypes.REORDER_EXP_RULE_SET_OP :
            return reorderExpToRuleSetOp(state, action);
        case actionTypes.GET_RULE_SUGGESTIONS_START :
            return getRuleSuggestionsStart(state, action);
        case actionTypes.GET_RULE_SUGGESTIONS_FAIL :
            return getRuleSuggestionsFail(state, action);
        case actionTypes.GET_RULE_SUGGESTIONS_SUCCESS :
            return getRuleSuggestionsSuccess(state, action);
        case actionTypes.SAVE_RULE_SET_START :
            return saveRuleSetStart(state, action);
        case actionTypes.SAVE_RULE_SET_FAIL :
            return saveRuleSetFail(state, action);
        case actionTypes.SAVE_RULE_SET_SUCCESS :
            return saveRuleSetSuccess(state, action);
        case actionTypes.LIST_RULE_SET_START :
            return listRuleSetStart(state, action);
        case actionTypes.LIST_RULE_SET_FAIL :
            return listRuleSetFail(state, action);
        case actionTypes.LIST_RULE_SET_SUCCESS :
            return listRuleSetSuccess(state, action);
        case actionTypes.GET_RULE_SET_START :
            return getRuleSetStart(state, action);
        case actionTypes.GET_RULE_SET_FAIL :
            return getRuleSetFail(state, action);
        case actionTypes.GET_RULE_SET_SUCCESS :
            return getRuleSetSuccess(state, action);
        case actionTypes.UPDATE_RULE_SET_START :
            return updateRuleSetStart(state, action);
        case actionTypes.UPDATE_RULE_SET_FAIL :
            return updateRuleSetFail(state, action);
        case actionTypes.UPDATE_RULE_SET_SUCCESS :
            return updateRuleSetSuccess(state, action);
        case actionTypes.RESET_RULE_SET_ERROR :
            return resetRuleSetError(state, action);
        case actionTypes.CLEAR_RULE_SET_STATE :
            return clearRuleSetState(state, action);
        case actionTypes.SET_VIEW_MODE :
            return setRuleSetViewMode(state, action);        
        default : return state
    }
};

export default reducer;