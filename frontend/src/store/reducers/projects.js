import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    projects : {
        projects : [],
        count  : 0
    },
    currentProj : null,
    loading : false,
    error : null
};

const fetchProjectsStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    });
};

const fetchProjectsSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        projects : action.projects
    });
};

const fetchProjectsFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    });
};

const resetError = (state, action) => {
    return updateObject(state, {
        error : null
    });
};

const createProjectStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    });
};

const createProjectSuccess = (state, action) => {
    let updatedProjects = [];
    if(state.projects && state.projects.projects) {
        updatedProjects = state.projects.projects.concat(action.project);
    } else {
        updatedProjects = [action.project];
    }

    return updateObject(state, {
        loading :  false,
        projects : updateObject(state.projects, {
            projects :  updatedProjects,
            count : state.projects.count + 1
        })
    })
};

const createProjectFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    });
};

const setCurrentProject = (state, action) => {
    const currentProject = state.projects.projects.filter(project => project.id === action.projectId);

    localStorage.setItem('currentProj', JSON.stringify(currentProject[0]));

    return updateObject(state, {
        currentProj : currentProject[0]
    });
};

const unsetCurrentProject = (state, action) => {
    localStorage.removeItem('currentProj');

    return updateObject(state, {
        currentProj : null
    })
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.FETCH_PROJECTS_START :
            return fetchProjectsStart(state, action);
        case actionTypes.FETCH_PROJECTS_SUCCESS :
            return fetchProjectsSuccess(state, action);
        case actionTypes.FETCH_PROJECTS_FAIL :
            return fetchProjectsFail(state, action);
        case actionTypes.RESET_ERROR :
            return resetError(state, action);
        case actionTypes.CREATE_PROJECT_START :
            return createProjectStart(state, action);
        case actionTypes.CREATE_PROJECT_SUCCESS :
            return createProjectSuccess(state, action);
        case actionTypes.CREATE_PROJECT_FAIL :
            return createProjectFail(state, action);
        case actionTypes.SET_CURRENT_PROJECT :
            return setCurrentProject(state, action);
        case actionTypes.UNSET_CURRENT_PROJECT :
            return unsetCurrentProject(state, action);
        default : return state
    }
};

export default reducer;