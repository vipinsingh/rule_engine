import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    token : null,
    userData : null,
    error :  null,
    loading : false,
    authRedirectPath : null
};

const authStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    });
};

const authSuccess = (state, action) => {
    return updateObject(state, {
        token : action.token,
        userData : action.userData,
        error : null,
        loading : false
    });
};

const authFail = (state, action) => {
    return updateObject(state, {
        error : action.error,
        loading : false
    });
};

const authLogoutSuccess = (state, action) => {
    return updateObject(state, {
        token : null,
        userData  : null,
        loading : false,
        error : null,
        authRedirectPath : null
    });
};

const authLogoutStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    });
};

const authLogoutFail = (state, action) => {
    return updateObject(state, {
        loading  : false,
        error : action.error
    });
};

const setAuthRedirectPath = (state, action) => {
    return updateObject(state, {
        authRedirectPath : action.path
    });
}

const resetAuth = (state, action) => {
    return updateObject(state, {
        error : null,
        loading :  false
    });
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.AUTH_START :
            return authStart(state, action);
        case actionTypes.AUTH_SUCCESS :
            return authSuccess(state,action);
        case actionTypes.AUTH_FAIL :
            return authFail(state, action);
        case actionTypes.AUTH_LOGOUT_START :
            return authLogoutStart(state, action);
        case actionTypes.AUTH_LOGOUT_SUCCESS :
            return authLogoutSuccess(state, action);
        case actionTypes.AUTH_LOGOUT_FAIL :
            return authLogoutFail(state, action);
        case actionTypes.SET_AUTH_REDIRECT_PATH :
            return setAuthRedirectPath(state, action);
        case actionTypes.RESET_AUTH :
            return resetAuth(state, action);
        default : return state;
    }
};

export default reducer;