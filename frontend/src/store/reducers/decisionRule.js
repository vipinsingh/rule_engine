import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    ruleList : null,
    ruleInfo : null,
    rule_expression : [],
    decision_expression : [],
    default_decision_expression : [],
    filtered_suggestions : {},
    loading : null,
    error : null,
    expression_saved : false,
    viewMode : 'create',
    isRuleCreated : false,
    ruleDecision : null
};

const addToRuleExpression = (state, action) => {
    const controlObj = updateObject(action.control, {
        id : state[action.ruleExpression].length + 1
    })

    const updatedRuleExpression = state[action.ruleExpression].concat(controlObj);

    return updateObject(state, {
        [action.ruleExpression] : updatedRuleExpression
    });
}

const editRuleExpression = (state, action) => {
    let updatedRuleExpression = state[action.ruleExpression].map(exp => {
        let updatedExp = exp;

        if(exp.id === action.controlId) {
            updatedExp = updateObject(updatedExp, {
                ...action.controlObj
            });
        }
        return updatedExp;
    });

    return updateObject(state, {
        [action.ruleExpression] : updatedRuleExpression
    });
}

const deleteRuleExpression = (state, action) => {
    const deletedRuleExpression = state[action.ruleExpression].filter(exp => {
        if(exp.id === action.controlId) {
            return false;
        }
        return exp;
    })

    const updatedRuleExpression = deletedRuleExpression.map(exp => {
        if(exp.id > action.controlId) {
            const updatedExp = updateObject(exp, {
                id : exp.id - 1
            })
            return updatedExp;
        }
        return exp;
    })

    return updateObject(state, {
        [action.ruleExpression] : updatedRuleExpression
    }); 
}

const reorderRuleExpression = (state, action) => {

    const selectedExp = {...state[action.ruleExpression][action.start]};

    let updatedRuleExpression = [
        ...state[action.ruleExpression].slice(0, action.start),
        ...state[action.ruleExpression].slice(action.start + 1)
    ];

    updatedRuleExpression.splice(action.end, 0, selectedExp);

    updatedRuleExpression = updatedRuleExpression.map((exp,index) => {
        const updatedId = index + 1;
        const updatedExp = updateObject(exp, {
            id : updatedId
        });
        return updatedExp;
    });

    return updateObject(state, {
        [action.ruleExpression] : updatedRuleExpression
    });
}

const saveExpressionStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null,
        expression_saved : false
    })
}

const saveExpressionFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    })
}

const saveExpressionSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        expression_saved : 'exp'
    })
}

const getExpressionsStart = (state, action) => {
    return updateObject(state, {
        error : null
    })
}

const getExpressionsFail = (state, action) => {
    return updateObject(state, {
        error : action.error
    })
}

const getExpressionsSuccess = (state, action) => {
    const updatedFilteredSuggestions = updateObject(state.filtered_suggestions, {
        [action.ruleExpression] : updateObject(state.filtered_suggestions[action.ruleExpression], {
            [action.controlId] : action.data
        })
    })

    return updateObject(state, {
        error : null,
        filtered_suggestions : updatedFilteredSuggestions
    })
}

const resetRuleError = (state, action) => {
    return updateObject(state, {
        error : null
    })
}

const resetExpSaved = (state, action) => {
    return updateObject(state, {
        expression_saved : false
    })
}

const saveRuleStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null,
        expression_saved  : false
    })
}

const saveRuleFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    })
}

const saveRuleSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        viewMode : 'view',
        isRuleCreated : true,
        ruleInfo : action.data,
        expression_saved : 'rule'
    })
}

const listRulesStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    })
}

const listRulesFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    })
}

const listRulesSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        ruleList : action.data
    })
}

const clearRuleState = (state, action) => {
    return updateObject(state, {
        ruleInfo : null,
        rule_expression : [],
        decision_expression : [],
        default_decision_expression : [],
        filtered_suggestions : {},
        loading : null,
        error : null,
        expression_saved : false,
        viewMode : 'create',
        isRuleCreated : false,
    })
}

const getRuleStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    })
}

const getRuleFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    })
}

const getRuleSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        ruleInfo : {...action.data},
        viewMode : 'view',
        rule_expression : action.data.rule_expression ? action.data.rule_expression : [], 
        decision_expression : Array.isArray(action.data.decision) ? action.data.decision : [], 
        default_decision_expression : Array.isArray(action.data.default_decision) ? action.data.default_decision : [], 
    })
}

const editRuleStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null,
        expression_saved  : false
    })
}

const editRuleFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    })
}

const editRuleSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        viewMode : 'view',
        ruleInfo : action.data,
        expression_saved : 'rule'
    })
}

const setViewMode = (state, action) => {
    return updateObject(state, {
        viewMode : action.mode
    })
}

const getRuleDecisionStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    })
}

const getRuleDecisionFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    })
}

const getRuleDecisionSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        ruleDecision : action.data
    })
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.ADD_TO_RULE_EXPRESSION :
            return addToRuleExpression(state, action);
        case actionTypes.EDIT_RULE_EXPRESSION :
            return editRuleExpression(state, action);
        case actionTypes.DELETE_RULE_EXPRESSION :
            return deleteRuleExpression(state, action);
        case actionTypes.REORDER_RULE_EXPRESSION :
            return reorderRuleExpression(state, action);
        case actionTypes.SAVE_EXPRESSION_START :
            return saveExpressionStart(state, action);
        case actionTypes.SAVE_EXPRESSION_FAIL :
            return saveExpressionFail(state, action);
        case actionTypes.SAVE_EXPRESSION_SUCCESS :
            return saveExpressionSuccess(state, action);
        case actionTypes.GET_EXPRESSIONS_START :
            return getExpressionsStart(state, action);
        case actionTypes.GET_EXPRESSIONS_FAIL :
            return getExpressionsFail(state, action);
        case actionTypes.GET_EXPRESSIONS_SUCCESS :
            return getExpressionsSuccess(state, action);
        case actionTypes.RESET_RULE_ERROR :
            return resetRuleError(state, action);
        case actionTypes.RESET_EXPRESSION_SAVED :
            return resetExpSaved(state, action);
        case actionTypes.SAVE_RULE_START :
            return saveRuleStart(state, action);
        case actionTypes.SAVE_RULE_FAIL :
            return saveRuleFail(state, action);
        case actionTypes.SAVE_RULE_SUCCESS :
            return saveRuleSuccess(state, action);
        case actionTypes.LIST_RULES_START :
            return listRulesStart(state, action);
        case actionTypes.LIST_RULES_FAIL :
            return listRulesFail(state, action);
        case actionTypes.LIST_RULES_SUCCESS :
            return listRulesSuccess(state, action);
        case actionTypes.CLEAR_RULE_STATE :
            return clearRuleState(state, action);
        case actionTypes.GET_RULE_START :
            return getRuleStart(state, action);
        case actionTypes.GET_RULE_FAIL :
            return getRuleFail(state, action);
        case actionTypes.GET_RULE_SUCCESS :
            return getRuleSuccess(state, action);
        case actionTypes.EDIT_RULE_START :
            return editRuleStart(state, action);
        case actionTypes.EDIT_RULE_FAIL :
            return editRuleFail(state, action);
        case actionTypes.EDIT_RULE_SUCCESS :
            return editRuleSuccess(state, action);
        case actionTypes.SET_VIEW_MODE :
            return setViewMode(state, action);
        case actionTypes.GET_RULE_DECISION_START :
            return getRuleDecisionStart(state, action);
        case actionTypes.GET_RULE_DECISION_FAIL :
            return getRuleDecisionFail(state, action);
        case actionTypes.GET_RULE_DECISION_SUCCESS :
            return getRuleDecisionSuccess(state, action);
        default : return state; 
    }
};

export default reducer;