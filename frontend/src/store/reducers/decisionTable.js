import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    tables : null,
    tableInfo : null,
    fields : [],
    rules : [],
    loading : null,
    error : null,
    isTableCreated : false,
    viewMode : 'create',
    decisionData : null,
    decisionsList : null,
    decisionDetails : null,
};

const onAddField = (state, action) => {
    const field = {
        id : state.fields.length + 1,
        title : action.fieldData.title,
        api_key : action.fieldData.api_key,
        fieldType : action.fieldData.fieldType
    }

    const updatedFields = state.fields.concat(field);

    return updateObject(state, {
        fields : updatedFields
    });
}

const onAddRule = (state, action) => {
    const updatedRules = state.rules.concat({
        id : state.rules.length + 1,
        title : '',
        description : '',
        conditions : [],
        decision : ''            
    });

    return updateObject(state, {
        rules : updatedRules
    });
}

const onAddCondition = (state, action) => {
    const updatedRules = state.rules.map(rule => {
        if(rule.id === action.ruleId) {
            const updatedConditions = rule.conditions.concat(action.conditionData);
            rule.conditions = updatedConditions;
        }
        return rule;
    });

    return updateObject(state, {
        rules : updatedRules
    });
}

const onEditRule = (state, action) => {
    const updatedRules = state.rules.map(rule => {
        if(rule.id === action.ruleId) {
            rule = updateObject(rule, {
                ...action.data
            })
        }
        return rule;
    });

    return updateObject(state, {
        rules : updatedRules
    });
}

const onDeleteRule = (state, action) => {
    const updatedRules = state.rules.filter(rule => {
        if(rule.id !== action.ruleId) {
            if(rule.id > action.ruleId) {
                const id = rule.id - 1;
                rule.conditions.forEach(condition => {
                    condition.rule_id = id;
                });
                rule.id = id;
            }
            return rule;
        }
        return false;
    });
    return updateObject(state, {
        rules : updatedRules
    });
}

const onCopyRule = (state, action) => {
    const index = action.ruleId;
    const copiedRule = JSON.parse(JSON.stringify(state.rules.find(rule => rule.id === action.ruleId)));
    state.rules.splice(index, 0, copiedRule);
    const updatedRules = state.rules.filter((rule, index) => {
        if(index >= action.ruleId) {
            const id = rule.id + 1;
            rule.conditions.forEach(condition => {
                condition.rule_id = id;
            });
            rule.id = id;
        }
        return rule;
    })
    return updateObject(state, {
        rules : updatedRules
    });
}

const onEditField = (state, action) => {
    const updatedFields = state.fields.map(field => {
        if(field.id === action.fieldId) {
            field = updateObject(field, {
                ...action.data
            })
        }
        return field;
    });

    return updateObject(state, {
        fields : updatedFields
    });
}

const onDeleteField = (state, action) => {

    const updatedFields = state.fields.filter(field => {
        if(field.id !== action.fieldId) {
            if(field.id > action.fieldId) {
                field.id = field.id - 1;
            }
            return field;
        }
        return false;
    });

    const updatedRules = state.rules.map(rule => {
        const updatedConditions = rule.conditions.filter(condition => {
            if(condition.field_id !== action.fieldId) {
                if(condition.field_id > action.fieldId) {
                    condition.field_id = condition.field_id - 1;
                }
                return condition;
            }
            return false;
        });
        rule.conditions = updatedConditions;
        return rule;
    })

    return updateObject(state,{
        fields : updatedFields,
        rules : updatedRules
    });
}

const onEditCondition = (state, action) => {
    const updatedRules = state.rules.map(rule => {
        if(rule.id === action.ruleId) {
            const updatedConditions = rule.conditions.map(condition => {
                if(condition.field_name === action.data.field_name) {
                    condition = updateObject(condition, {
                        ...action.data
                    })
                }
                return condition;
            })
            rule.conditions = updatedConditions
        }
        return rule;
    })

    return updateObject(state, {
        rules : updatedRules
    });
}

const createTableStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null,
        isTableCreated : false
    })
}

const createTableFail = (state, action) => {
    return updateObject(state,{
        loading : false,
        error : action.error,
        isTableCreated : false
    })
}

const createTableSuccess = (state, action) => {

    return updateObject(state, {
        loading : false,
        error : null,
        fields : action.tableData.fields,
        rules :  action.tableData.rules,
        tableInfo : {
            ...action.tableData
        },
        isTableCreated : true,
        viewMode : 'view'
    });
}

const updateTableStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null,
        isTableCreated : false
    })
}

const updateTableFail = (state, action) => {
    return updateObject(state,{
        loading : false,
        error : action.error,
        isTableCreated : false
    })
}

const updateTableSuccess = (state, action) => {

    return updateObject(state, {
        loading : false,
        error : null,
        fields : action.tableData.fields,
        rules :  action.tableData.rules,
        tableInfo : {
            ...action.tableData
        },
        isTableCreated : true,
        viewMode : 'view'
    });
}

const resetError = (state, action) => {
    return updateObject(state, {
        error : null
    });
}

const setViewMode = (state, action) => {
    return updateObject(state, {
        viewMode : action.mode
    })
}

const fetchTableStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    });
}

const fetchTableFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    });
}

const fetchTableSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        fields : action.tableData.fields ? action.tableData.fields : [],
        rules :  action.tableData.rules ? action.tableData.rules : [],
        tableInfo : {
            ...action.tableData
        },
        viewMode : 'view'
    });
}

const listTablesStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    });
}

const listTablesFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    });
}

const listTablesSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        tables : action.tables
    });
}

const clearTableState = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        tableInfo : null,
        rules : [],
        fields : [],
        isTableCreated : false,
        viewMode : 'create',
        decisionData : null
    });
}

const getDecisionStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    });
}

const getDecisionFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    });
}

const getDecisionSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        decisionData : action.decision
    });
}

const listDecisionsStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    });
}

const listDecisionsFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    });
}

const listDecisionsSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        decisionsList : action.decisions
    });
}

const getDecisionDetailsStart = (state, action) => {
    return updateObject(state, {
        loading : true,
        error : null
    });
}

const getDecisionDetailsFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : action.error
    });
}

const getDecisionDetailsSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        error : null,
        decisionDetails : action.decision
    });
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.ADD_FIELD :
            return onAddField(state, action);
        case actionTypes.EDIT_FIELD :
            return onEditField(state, action);
        case actionTypes.DELETE_FIELD :
            return onDeleteField(state, action);
        case  actionTypes.ADD_RULE :
            return onAddRule(state, action);
        case  actionTypes.EDIT_RULE :
            return onEditRule(state, action);
        case  actionTypes.DELETE_RULE :
            return onDeleteRule(state, action);
        case  actionTypes.COPY_RULE :
            return onCopyRule(state, action);
        case actionTypes.ADD_CONDITION :
            return onAddCondition(state, action);
        case actionTypes.EDIT_CONDITION :
            return onEditCondition(state, action);
        case actionTypes.CREATE_TABLE_START :
            return createTableStart(state, action);
        case actionTypes.CREATE_TABLE_FAIL :
            return createTableFail(state, action);
        case actionTypes.CREATE_TABLE_SUCCESS :
            return createTableSuccess(state, action);
        case actionTypes.UPDATE_TABLE_START :
            return updateTableStart(state, action);
        case actionTypes.UPDATE_TABLE_FAIL :
            return updateTableFail(state, action);
        case actionTypes.UPDATE_TABLE_SUCCESS :
            return updateTableSuccess(state, action);
        case actionTypes.RESET_ERROR :
            return resetError(state, action);
        case actionTypes.SET_VIEW_MODE :
            return setViewMode(state, action);
        case actionTypes.FETCH_TABLE_START :
            return fetchTableStart(state, action);
        case actionTypes.FETCH_TABLE_FAIL :
            return fetchTableFail(state, action);
        case actionTypes.FETCH_TABLE_SUCCESS :
            return fetchTableSuccess(state, action);
        case actionTypes.LIST_TABLES_START :
            return listTablesStart(state, action);
        case actionTypes.LIST_TABLES_FAIL :
            return listTablesFail(state, action);
        case actionTypes.LIST_TABLES_SUCCESS :
            return listTablesSuccess(state, action);
        case actionTypes.CLEAR_TABLE_STATE :
            return clearTableState(state, action);
        case actionTypes.GET_DECISION_START :
            return getDecisionStart(state, action);
        case actionTypes.GET_DECISION_FAIL :
            return getDecisionFail(state, action);
        case actionTypes.GET_DECISION_SUCCESS :
            return getDecisionSuccess(state, action);
        case actionTypes.LIST_DECISIONS_START :
            return listDecisionsStart(state, action);
        case actionTypes.LIST_DECISIONS_FAIL :
            return listDecisionsFail(state, action);
        case actionTypes.LIST_DECISIONS_SUCCESS :
            return listDecisionsSuccess(state, action);
        case actionTypes.GET_DECISION_DETAILS_START :
            return getDecisionDetailsStart(state, action);
        case actionTypes.GET_DECISION_DETAILS_FAIL :
            return getDecisionDetailsFail(state, action);
        case actionTypes.GET_DECISION_DETAILS_SUCCESS :
            return getDecisionDetailsSuccess(state, action);    
        default : return state;
    }
}

export default reducer;